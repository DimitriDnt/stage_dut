**GIT commun aux projets GETS et Inclino**

**Travail commun :** Programmation Arduino, Programmation Python, Développement Web

**Projet GETS :** Frédéric LAURET, Dimitri DENNEMONT et François Philippe HOAREAU

**Projet Inclino :** Frédéric LAURET, Jérémy ROULT

**Auteurs :** Frédéric LAURET, Jérémy ROULT, Dimitri DENNEMONT et François Philippe HOAREAU

**Propriété de l'Observatoire Volcanologique du Piton de la Fournaise (OVPF)**