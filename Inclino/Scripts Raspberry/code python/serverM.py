import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.gen
from tornado.options import define, options
import os
import sys
import time
import multiprocessing
import serialworker
import json
from collections import namedtuple 
import serial
import logging
import signal

log = logging.getLogger('serial2tcp')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
log.addHandler(ch)






define("port", default=8080, help="run on the given port", type=int)
 
clients = [] 


class IndexHandler(tornado.web.RequestHandler):
	
	

	def get(self):
		self.render('index.html')

class StaticFileHandler(tornado.web.RequestHandler):
	def get(self):
		self.render('main.js')
 
class WebSocketHandler(tornado.websocket.WebSocketHandler):
	def urls(cls):
		return [
            (r'/ws', cls, {}),  # Route/Handler/kwargs
        ]
    
    
    
	def check_origin(self, origin):
		"""
        Override the origin check if needed
		"""
		return True

	def open(self):
		print 'new connection'
		clients.append(self)
		self.write_message("connected")
 
	def on_message(self, message):
		for c in clients:
				c.write_message(message)
		
        #self.write_message('ack')
		for ser in PORTreduit:
			ser.queue_input.put(message)
 
	def on_close(self):
		print 'connection closed'
		clients.remove(self)


def checkQueue():
	for ser in PORTreduit:
		if not ser.queue_output.empty():
			message = ser.queue_output.get()
			
	



	if 'message' in locals():
		for c in clients:
				c.write_message(message)


if __name__ == '__main__':



	MyStructSerial = namedtuple("MyStructSerial", "port bauderate")
	MyStructSerialreduit = namedtuple("MyStructSerialreduit", "port bauderate queue_input queue_output")
	SERIAL_PORT=[]

	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB0",bauderate= "115200"))    	 #arduino reglette lineaire 1
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB1",bauderate= "115200"))		 #arduino Moteur Assenceur 1
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB2",bauderate= "115200"))		 #arduino Moteur Assenceur 2
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB3",bauderate= "115200"))		 #arduino Moteur principale
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB4",bauderate= "115200"))		 #arduino Moteur Assenceur auxilliaire
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB5",bauderate= "115200"))	     #arduino regulation thermique
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB6",bauderate= "115200"))	     #arduino palpeur 1
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB7",bauderate= "115200"))		 #arduino palpeur 2
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB8",bauderate= "115200"))		 #arduino mesure capteur
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB9",bauderate= "115200"))		 #arduino mesure capteur
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB10",bauderate= "115200"))		 #arduino mesure capteur
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB11",bauderate= "115200"))	 	 #arduino mesure capteur
	SERIAL_PORT.append(MyStructSerial( port="/dev/ttyUSB12",bauderate= "115200"))	 	 #arduino mesure capteur
	 
	PORTreduit=[]




	for ser in SERIAL_PORT:
		print(" port "+ser.port)
		ps=serial.Serial()
		ps.port=ser.port
		ps.bauderate=ser.bauderate
		try:
			print("ouverture du port :"+ps.portstr)
			ps.open()	
			print("ouverture du port"+ps.portstr+" reussi")
			PORTreduit.append(MyStructSerialreduit(port =ser.port,bauderate =ser.bauderate, queue_input= multiprocessing.Queue(), queue_output= multiprocessing.Queue()
))																									
			ps.close()
		except serial.SerialException as e:
				
				log.fatal("port serie %s non disponible  " % (ps.portstr))
				
		if len(PORTreduit)<1: 
			log.fatal("aucun port serie disponible, ce serveur ne peut se lancer  ")
			sys.exit(1)
	   







	sp=[]
	for ser in PORTreduit:
		print (ser)
		sp.append(serialworker.SerialProcess(ser.queue_input,ser.queue_output,ser.port,ser.bauderate))
		sp[-1].daemon = True
		sp[-1].start()
	
	
	tornado.options.parse_command_line()
	app = tornado.web.Application(
	    handlers=[
	        (r"/", IndexHandler),
	        (r"/static/(.*)", tornado.web.StaticFileHandler, {'path':  './'}),
	        (r"/ws", WebSocketHandler)
	    ]
	)
	httpServer = tornado.httpserver.HTTPServer(app)
	httpServer.listen(options.port)
	print "Listening on port:", options.port

	mainLoop = tornado.ioloop.IOLoop.instance()
	## adjust the scheduler_interval according to the frames sent by the serial port
	scheduler_interval = 100
	scheduler = tornado.ioloop.PeriodicCallback(checkQueue, scheduler_interval, io_loop = mainLoop)
	scheduler.start()
	mainLoop.start()
