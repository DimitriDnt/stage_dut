# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 09:02:16 2019

@author: lauret
"""

#!/usr/bin/python

from fsm import FiniteStateMachine, get_graph, State

STATES = [ 'POSITION11', 'POSITION2', 'POSITION3', 
          'ERREUR']

tcpip = FiniteStateMachine('Moteur')

init = State('INIT', initial=True)
position1, position2, position3, erreur = [State(s) for s in STATES]
init['(position1)']=position1     #pour faire une transition
init.update({r'debut': position1,
               'erreur': erreur})

position1.update({r'coucou2': position1,
                r'coucou3': position2,
                r'coucou4': position3})





graph = get_graph(tcpip)
graph.draw('tcp.png', prog='dot')


position1.update({r'coucou20': position1,
                r'coucou3': position2,
                r'coucou4': position3})
graph = get_graph(tcpip)
graph.draw('tcp4.png', prog='dot')