import websocket
import json
from time import sleep
from collections import namedtuple
global json_string

try:
    import thread
except ImportError:
    import _thread as thread
import time


def on_message(ws, message):                                #ce qu'on recois quand il y a un message
    global json_string
    try:
        json_string=json.loads(message.replace("'",'"'), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        #print(json_string)
    except:
        pass
        #print(message)
def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    def run(*args):
        global json_string
        ws.send('{"Moteur1":{"Fonction":"avance","vitesse":75}}')
        a=True
        ws.send('{"Moteur1":{"Mesure":"MesureDiscrete"}}')
        """
        while(a):
            try:
                print(json_string.position)
                if (float(json_string.position)>120):
                    ws.send('{"Moteur1":{"Fonction":"immobile","vitesse":75}}')
                    a=False
                else:
                    ws.send('{"Moteur1":{"Mesure":"MesureDiscrete"}}')
                    sleep(1)
            except NameError:
                pass
"""
            
        
        while():
            #print("Received '%s'" % reponse)
            sleep(0.1)
            while(1):
                ws.send('{"Moteur1":{"Fonction":"avance","vitesse":75}}')
                print('{"Moteur1":{"Fonction":"avance","vitesse":75}}')
                sleep(5)
                ws.send('{"Moteur1":{"Fonction":"recule","vitesse":75}}')
                print('{"Moteur1":{"Fonction":"recule","vitesse":75}}')
                sleep(5)

    thread.start_new_thread(run, ())


if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://192.168.1.88:8080/ws",
                               on_message = on_message,
                               on_error = on_error,
                               on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
            