
from transitions import Machine
#from fsm import FiniteStateMachine, get_graph, State
import websocket
import json
from time import sleep
from collections import namedtuple
global json_string

try:
    import thread
except ImportError:
    import _thread as thread
import time



i=1

def on_message(ws, message):                                #ce qu'on recois quand il y a un message
    global json_string
    try:
        json_string=json.loads(message.replace("'",'"'), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        #print(json_string)
    except:
        pass
        #print(message)
def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")


#tcpip = FiniteStateMachine('Moteur')

class Moteur(object):
    states = ['start','repos','monte','descend','erreur']
    
    def __init__(self, name):
        self.name=name
        self.machine= Machine(model=self, states = Moteur.states, initial= 'start')
        
        self.machine.add_transition(
            trigger='debut',
            source='start',
            dest='repos',
            conditions=condition1())

        self.machine.add_transition(
            trigger='conditionmonte',
            source='repos',
            dest='moteurmonte',
            after=print_test(),
            conditions=condition1())

        self.machine.add_transition(
            trigger='monte',
            source='moteurmonte',
            dest='moteurmonte',
            after=print_test(),
            #after=avance(),print_test(),
            conditions=condition11())

        self.machine.add_transition(
            trigger='monteefinie',
            source='moteurmonte',
            dest='repos',
            after=print_test(),
            #after=immobile(),print_test(),
            conditions=condition2())

        self.machine.add_transition(
            trigger='conditiondescend',
            source='repos',
            dest='moteurdescend',
            after=print_test(),
            conditions=condition2())
        
        self.machine.add_transition(
            trigger='descend',
            source='moteurdescend',
            dest='moteurdescend',
            after=print_test(),
            #after=recule(),print_test(),
            conditions=condition22())

        self.machine.add_transition(
            trigger='descentefinie',
            source='moteurdescend',
            dest='repos',
            after=print_test(),
            #ater=immobile(),print_test(),
            conditions=condition1())
        
"""        
def avance():
        def run(*args):
            ws.send('{"Moteur1":{"Fonction":"avance",vitesse":75}}')
    
def recule():
        def run(*args):
            ws.send('{"Moteur1":{"Fonction":"recule",vitesse":75}}')

def immobile():
        def run(*args):
            ws.send('{"Moteur1":{"Fonction":"immobile",vitesse":75}}')
"""

"""
def condition1():
        if (json_string.position>90):
            return True

def condition11():
        if (json_string.position>10):
            return True

def condition2():
        if (json_string.position<10):
            return True

def condition22():
        if (json_string.position<90):
            return True
"""

chose=Machine()
sleep(2)
print(chose.state)
chose._process
print(chose.state)


def condition1():
        if (i==1):
            return True

def condition11():
        if (i==0):
            return True

def condition2():
        if (i==0):
            return True

def condition22():
        if (i==0):
            return True



def print_test():
    print("test")
    print(chose.state)



"""
def repos(self):
    print("test1")
    return 0

def moteurmonte(self):
    print("test1")
    return 0

def moteurdescend(self):
    print("test1")
    return 0
"""

"""
def on_open(ws):
    def run(*args):
        ws.send('{"Moteur1":{"Fonction":"recule",vitesse":75}}')

"""

"""

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://192.168.1.88:8080/ws",
                               on_message = on_message,
                               on_error = on_error,
                               on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()

"""

"""
graph = get_graph(tcpip)
graph.draw('tcp.png', prog='dot')
"""