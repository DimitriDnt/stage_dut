import websocket
import json
from time import sleep
from collections import namedtuple
global json_string

try:
    import thread
except ImportError:
    import _thread as thread
import time

from fsmN import FiniteStateMachine, get_graph, State, Transducer

STATES = [ 'REPOS', 'MOTEURMONTE', 'MOTEURDESCEND', 
          'ERREUR']

tcpip = FiniteStateMachine('Moteur')

init = State('INIT', initial=True)
repos, moteurmonte, moteurdescend, erreur = [State(s) for s in STATES]
#init['(repos)']=repos     #pour faire une transition
init.update({r'debut': repos,
               'erreur': erreur})

repos.update({r'attente' : repos,
                r'conditionmonte': moteurmonte,
                r'conditiondescend': moteurdescend,
                r'disfonctionnement': erreur})

moteurmonte.update({r'monte':moteurmonte,
                        r'monteefinie':repos,
                        r'disfonctionnement1':erreur})

moteurdescend.update({r'descend':moteurdescend,
                        r'descentefinie':repos,
                        r'disfonctionnement2':erreur})

erreur.update({r'reset':repos})


def on_message(ws, message):                                #ce qu'on recois quand il y a un message
    global json_string
    try:
        json_string=json.loads(message.replace("'",'"'), object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        #print(json_string)
    except:
        pass
        #print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def avance():                                                         #fonctions moteur
    ws.send('{"Moteur1":{"Fonction":"avance","vitesse":35}}')

def recule():                                                         
    ws.send('{"Moteur1":{"Fonction":"recule","vitesse":35}}')

def immobile():                                                         
    ws.send('{"Moteur1":{"Fonction":"immobile","vitesse":35}}')

def Sinit():                                                #DEFINITION FONCTIONS ETATS
    tcpip.transition( 'REPOS')
    print ("patate")              


def Srepos():
    global json_string
    immobile()
    if (json_string.DHT11_1.Temperature <25):
        tcpip.transition( 'REPOS' )
    if (json_string.DHT11_1.Temperature >=26):
        tcpip.transition('ERREUR')
    if (json_string.DHT11_1.Temperature >=25 and json_string.DHT11_1.Temperature <26):
        tcpip.transition( 'MOTEURMONTE')

def Smoteurmonte():
    global json_string
    avance()
    if (json_string.DHT11_1.Temperature <25):
        tcpip.transition( 'REPOS')
    if (json_string.DHT11_1.Temperature >=26):
        tcpip.transition('ERREUR')
    if (json_string.DHT11_1.Temperature >=25 and json_string.DHT11_1.Temperature <26):
        tcpip.transition( 'MOTEURMONTE' )

    

def transit():                                                      #SCAN DE L'ETAT ACTUEL
    print("patate2")
    print(tcpip.current_state)
    if (tcpip.current_state == "INIT"):
        Sinit()
    if (tcpip.current_state == "REPOS"):
        Srepos()
    if (tcpip.current_state == "MOTEURMONTE"):
        Smoteurmonte()   


tcpip.current_state="INIT"
def on_open(ws):
    def run(*args):

        global json_string
        ws.send('{"Moteur1":{"Fonction":"immobile"}}')
        a=True
        ws.send('{"Moteur1":{"Mesure":"MesureDiscrete"}}')
        sleep(2)

        while(a):                                                                                   #loop
            try:
                sleep(2)
                #print(json_string.Moteur1.Position)
                #print(json_string.DHT11_1.Temperature) 
                #print(tcpip.current_state)
                transit()

            except NameError:
                pass

    thread.start_new_thread(run, ())


if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://localhost:8080/ws",
                               on_message = on_message,
                               on_error = on_error,
                               on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()