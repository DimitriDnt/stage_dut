# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 09:02:16 2019

@author: lauret
"""
import websocket
import json
from time import sleep
from collections import namedtuple
global json_string

try:
    import thread
except ImportError:
    import _thread as thread
import time

i=True

#!/usr/bin/python
#/usr/local/lib/python2.7/dist-packages/            localisation des librairies !

#from fsm import FiniteStateMachine, get_graph, State
from fsmM import FiniteStateMachine, get_graph, State, Transducer

STATES = [ 'REPOS', 'MOTEURMONTE', 'MOTEURDESCEND', 
          'ERREUR']

tcpip = FiniteStateMachine('Moteur')

init = State('INIT', initial=True)
repos, moteurmonte, moteurdescend, erreur = [State(s) for s in STATES]
#init['(repos)']=repos     #pour faire une transition
init.update({r'debut': repos,
               'erreur': erreur})

repos.update({r'conditionmonte': moteurmonte,
                r'conditiondescend': moteurdescend,
                r'disfonctionnement': erreur})

moteurmonte.update({r'monte':moteurmonte,
                        r'monteefinie':repos,
                        r'disfonctionnement1':erreur})

moteurdescend.update({r'descend':moteurdescend,
                        r'descentefinie':repos,
                        r'disfonctionnement2':erreur})

erreur.update({r'reset':repos})
                                                                                           

def on_message(ws, message):                                #RECEPTION MESURE JSON
    global json_string
    try:
        json_string=json.loads( object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        #print(json_string)
    except:
        pass
        #print(message)
def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")



def avance():                                                         #fonctions moteur
    def on_open(ws):
        def run(*args):
            #ordre = ""
            ws.send('{"Moteur1":{"Fonction":"avance","vitesse":75}}')

def recule():                                                         
    def on_open(ws):
        def run(*args):
            ws.send('{"Moteur1":{"Fonction":"recule","vitesse":75}}')



def Sinit():                                                #DEFINITION FONCTIONS ETATS
    avance()
    tcpip.transition( 'debut')              


def Srepos():
    global json_string
    if (json_string.DHT11_1.Temperature >25):
        tcpip.transition( 'conditionmonte')
    if (json_string.DHT11_1.Temperature >30):
        tcpip.transition('disfonctionnement')

def Smoteurmonte():
    global json_string
    if (json_string.DHT11_1.Temperature <25):
        tcpip.transition( 'monteefinie')
    if (json_string.DHT11_1.Temperature >30):
        tcpip.transition('disfonctionnement1')

    

def transit():                                                      #SCAN DE L'ETAT ACTUEL

    if (tcpip.current_state.name == "INIT"):
        Sinit()
    if (tcpip.current_state.name == "REPOS"):
        Srepos()
    if (tcpip.current_state.name == "MOTEURMONTE"):
        Smoteurmonte()

a=0
tcpip.current_state=init
sleep(10)
while i==True:                                                              #LOOP
    print(tcpip.current_state.name)
    transit()
    sleep(1)


"""
mycase={
    "INIT":debut,
    "REPOS":conditionmonte,
    "MONTE":monteefinie,
}
myfunction=mycase[tcpip.current_state.name]
"""
"""
tcpip.current_state=repos
print(tcpip.current_state.name)
"""
"""
mycase={
    #"INIT":tcpip.transition( 'conditionmonte'),
    "REPOS":tcpip.transition( 'conditionmonte'),
    "MOTEURMONTE":tcpip.transition( 'monteefinie'),
    "REPOS":tcpip.transition( 'conditionmonte'),
}
"""
"""
#myfunction=mycase[tcpip.current_state.name]

def function(etat):
    mycase={
        #"INIT":tcpip.transition( 'conditionmonte'),
        "REPOS":tcpip.transition( 'conditionmonte'),
        "MOTEURMONTE":tcpip.transition( 'monteefinie'),
        "REPOS":tcpip.transition( 'conditionmonte'),
        
        
    }
    mycase[tcpip.current_state.name]



while i==True:
    name = tcpip.current_state.name
    function(name)
    print(tcpip.current_state.name)
    sleep(1)


"""
"""
graph = get_graph(tcpip)
graph.draw('tcp.png', prog='dot')

tcpip.current_state=repos
print(tcpip.current_state.name)
tcpip.current_state=moteurmonte
print(tcpip.current_state.name)
"""

"""
def first_case():
    print "first"

def second_case():
    print "second"

def third_case():
    print "third"

mycase = {
'first': first_case, #do not use ()
'second': second_case, #do not use ()
'third': third_case #do not use ()
}
myfunc = mycase['first']
myfunc()
"""