

from fsmN import FiniteStateMachine, get_graph, State, Transducer
from time import sleep

STATES = [ 'PREMIER', 'DEUXIEME', 
          'ERROR']

tcpip = FiniteStateMachine('Moteur')

init = State('INIT', initial=True)
premier, deuxieme, error = [State(s) for s in STATES]
#init['(repos)']=repos     #pour faire une transition
init=({r'debut': premier,
                'toerror': error})

premier=({r'to2' : deuxieme,
                r'toerror1': error})

deuxieme=({r'to1':premier,
                    r'toerror2':error})

error=({r'reset':init})



def function(etat):
    mycase={
        #"INIT":tcpip.transition( 'conditionmonte'),
        "PREMIER":tcpip.transition( 'PREMIER'),

        "DEUXIEME":tcpip.transition( 'DEUXIEME'),

        
        
    }
    mycase[tcpip.current_state]

"""
tcpip.current_state="INIT"
print(tcpip.current_state)
tcpip.transition( 'debut')
print(tcpip.current_state)
tcpip.transition('to2')
print(tcpip.current_state)
"""

i=True
tcpip.current_state="PREMIER"
print(tcpip.current_state)
while i==True:
    name = tcpip.current_state
    function(name)
    print(tcpip.current_state)
    sleep(1)
