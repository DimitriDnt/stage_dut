# -*- coding: utf-8 -*-
"""
Created on Thu May  2 19:48:30 2019

@author: lauret
"""

_version__ = '0.01'

from apng import APNG      #librarie pour faire des images png animées
import json                #librarie pour utiliser les objet json
import websocket           #librarie pour faire unclient websocket
try:
    import thread          #librarie pour lancer le client websocket dans un tread
except ImportError:
    import _thread as thread
import copy             
#decodage des trames json arrivant du Websocket

#il faut une classe pour chaque type object communiquant

class MoteurAscenseur(object):
    def __init__(self, position, vitesse, error ):
        self.position =float( position)
        self.vitesse = float(vitesse)
        self.error = error
class palpeur(object):
    def __init__(self, mesure, error ):
        self.mesure =float(mesure)
        self.error = error
        MesureMoteur()
        
def MesureMoteur():
    ws.send({"Moteur1":{"Mesure":"MesureDiscrete"}})

#il faut un bloc if pour chaque object communiquant        
def object_decoder(obj):
   
    print("peu"+str(obj))
        
    if 'Moteur1' in obj:
              
        return MoteurAscenseur(obj['Moteur1']['position'], obj['Moteur1']['vitesse'], obj['Moteur1']['error'])

    elif 'Moteur2' in obj:
              
        return MoteurAscenseur(obj['Moteur2']['position'], obj['Moteur2']['vitesse'], obj['Moteur2']['error'])

    elif 'palpeur1' in obj:
            
        return palpeur(obj['palpeur1']['mesure'], obj['palpeur1']['error'])


    return obj


 
MACHINES = dict()

NOOP = lambda: None
NOOP_ARG = lambda arg: None
# librairie pout utilisation du troiéme état X dontcare

class _DontCare(object):
    def __lt__(self, other):
        return True
    
    def __gt__(self, other):
        return True
    
    def __eq__(self, other):
        return True
    
    def __add__(self, other):
        return other
    
    def __sub__(self, other):
        return 0 if self is other else -other
    
    def __mul__(self, other):
        return other

    def __pow__(self, p):
        return self
    
    def __floordiv__(self, other):
        return 1//other
    
    def __div__(self, other):
        return 1/other
    
    def __mod__(self, other):
        return 0
    
    def __and__(self, other):
        return True
    
    def __or__(self, other):
        return True
    
    def __xor__(self, other):
        return True
    
    
    def __rlt__(self, other):
        return True
    
    def __rgt__(self, other):
        return True
    
    def __req__(self, other):
        return True
    
    def __radd__(self, other):
        return other
    
    def __rsub__(self, other):
        return other
    
    def __rmul__(self, other):
        return other

    def __rpow__(self, p):
        return p
    
    def __rfloordiv__(self, other):
        return other
    
    def __rdiv__(self, other):
        return other
    
    def __rmod__(self, other):
        return 0
    
    def __rand__(self, other):
        return True
    
    def __ror__(self, other):
        return True
    
    def __rxor__(self, other):
        return True
    
    def __str__(self):
        return "dontcare"
    
    def __repr__(self):
        return "dontcare"

dontcare = _DontCare()
# librairie pour les machines à etat

class FSMError(Exception):
    """Base FSM exception."""
    pass

class TransitionError(FSMError):
    """Transition exception."""
    pass

class StateError(FSMError):
    """State manipulation error."""


class FiniteStateMachine(object):
    
    """Machine à etat generique."""

    DOT_ATTRS = {
        'directed': True,
        'strict': False,
        'rankdir': 'LR',
        'ratio': '0.3'
    }

    def __init__(self, name, default=True):
        """Constructeur de la FSM."""
        self.name = name
        FiniteStateMachine._setup(self)
        self._setup()
        self.current_state = None
        MACHINES[name] = self
        if default:
            MACHINES['default'] = MACHINES[name]

    def _setup(self):
        """Setup de la FSM."""
        # All finite state machines share the following attributes.
        self.inputs = list()
        self.states = list()
        self.init_state = None

    @property
    def all_transitions(self):
        """donne l'ensemble des transition d'un etat.
        
        Returns: une list de 3 elements tuples (source state, input, destination state)
        """
        transitions = list()
        for src_state in self.states:
            for input_value, dst_state in src_state.items():
                transitions.append((src_state, input_value[1], dst_state))
        return transitions

    def conforme(self,a,b):  #verification bit a bit de l' egalite des tuples en input
                for k in zip(a,b):
			if ((k[0]!=k[1]) and ((k[0]!=dontcare) and (k[1]!=dontcare))):
				return False

		return True    
    def transition(self, input_value):
        
        """evalue les transition vers l'etat suivant depuis l'etat courent."""
        
        current = self.current_state
        if current is None:
            raise TransitionError('etat courent non definie.')
        Newinput_value=list()
        #print("liste des transitions")
        for i in current:
            j=i[0][1]
            #print ('input value entree de transition'+str(input_value))
            #print ("j"+ str(j))
            #print ("input_value"+str(self.conforme(j,input_value)))
            if (self.conforme(j,input_value)):
                Newinput_value.append(i[0][0])
                Newinput_value.append(i[0][1])
                Newinput_valueA=tuple(Newinput_value)
                Newinput_value=list()
                Newinput_value.append(Newinput_valueA)
                Newinput_value.append(i[1])
                break
        #print ('input value sortie de transition'+str(Newinput_value))
        destination_state = current.get(tuple(Newinput_value), current.default_transition)
        if destination_state is None: 
            raise TransitionError('ne peut faire de transition depuis l etat %r'
                                  ' avec cette entree %r.' % (current.name, (dontcare,input_value)))
        else:
            self.current_state = destination_state                                      #changement d'etat effectif ici
            #print("image crée")                                               #on fait un png pour le nouvelle etat
            get_graph(GPAfsm,0,"Machine a etat pour la gestion du couple moteur palpeur ").draw('GPAcurrent.png', prog='dot')
            im = APNG.open(self.name+"animation.png")       # on ajouter le nouvel etat a l'animation
            files = [ (self.name+'current.png', 100)]
            for file, delay in files:
                im.append_file(file, delay=delay)
            im.save(self.name+"animation.png")

    def reset(self):
        """reset de la machine à état."""
        self.current_state = self.init_state
                                                            #on fait un png pour cette  etat initial
        get_graph(GPAfsm,0,"Machine a etat pour la gestion du couple moteur palpeur ").draw(self.name+'current.png', prog='dot')
        im = APNG()
        files = [ (self.name+'current.png', 100)]          # on cree une nouvelle  l'animation avec le png initial
        for file, delay in files:
            im.append_file(file, delay=delay)         
        im.save(self.name+"animation.png")
        



    def process(self, input_data):                          #process une liste de vecteur d'entree en faisant d'abord un reset
        """Process input data."""
        self.reset()
        for item in input_data:
            self.transition(item)
    def processSingle(self, input_data):                    # process un vecteur d'entree depuis l'etat courant              
        """Process input data."""
        self.transition(input_data)

class Acceptor(FiniteStateMachine):

    """Acceptor machine."""

    def _setup(self):
        """Setup an acceptor."""
        self.accepting_states = list()

    def process(self, input_data):
        """Process input data."""
        self.reset()
        for item in input_data:
            self.transition(item[1])
        return id(self.current_state) in [id(s) for s in self.accepting_states]


class Transducer(FiniteStateMachine):
    
    """A semiautomaton transducer."""

    def _setup(self):
        """Setup a transducer."""
        self.outputs = list()

    def output(self, input_value):
        """Return state's name as output."""
        return self.current_state.name

    def process(self, input_data, yield_none=True):
        """Process input data."""
        self.reset()
        for item in input_data:
            if yield_none: 
                yield self.output(item)
            elif self.output(item) is not None:
                yield self.output(item)
            self.transition(item)
    def processSingle(self, input_data, yield_none=True):
        """Process input data."""
        #print('transducer processSingle'+ str(input_data))
        if yield_none: 
            yield self.output(input_data)
        elif self.output(input_data) is not None:
                yield self.output(input_data)
        self.transition(input_data)


class MooreMachine(Transducer):
    
    """Moore Machine."""

    def output(self, input_value):
        """Return output value assigned to the current state."""
        return self.current_state.output_values[0][1]


class MealyMachine(Transducer):
    
    """Mealy Machine."""

    def output(self, input_value):
        """Return output for a given state transition."""
        #print ("mealyMachine" + str(input_value))
        return dict(self.current_state.output_values).get(input_value) 


class State(dict):
    
    """State class."""

    DOT_ATTRS = {
        'shape': 'circle',
        'height': '1.2',
    }
    DOT_ACCEPTING = 'doublecircle'

    def __init__(self, name, initial=False, accepting=False, output=None,
                 on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG, 
                 on_transition=NOOP_ARG, machine=None, default=None):
        """Construct a state."""
        dict.__init__(self)
        self.name = name
        self.entry_action = on_entry
        self.exit_action = on_exit
        self.input_action = on_input
        self.transition_action = on_transition
        self.output_values = [(None, output)]
        self.default_transition = default
        if machine is None:
            try:
                machine = MACHINES['default']
            except KeyError:
                pass

        if machine:
            machine.states.append(self)
            if accepting:
                try:
                    machine.accepting_states.append(self)
                except AttributeError:
                    raise StateError('The %r %s does not support accepting '
                                     'states.' % (machine.name, 
                                     machine.__class__.__name__))
            if initial:
                machine.init_state = self

    def __getitem__(self, input_value):
        """Make a transition to the next state."""
        next_state = dict.__getitem__(self, input_value)
        self.input_action(input_value)
        self.exit_action()
        self.transition_action(next_state)
        next_state.entry_action()
        return next_state

    def __setitem__(self, input_value, next_state):
        """Set a transition to a new state."""
        if not isinstance(next_state, State):
            raise StateError('A state must transition to another stateut_value')
            self.output_values.append((input_value, output_value))
        dict.__setitem__(self, input_value, next_state)

    def __repr__(self):
        """Represent the object in a string."""
        return '<%r %s @ 0x%x>' % (self.name, self.__class__.__name__, id(self))


def get_graph(fsm,Tlabel=-1,title=None):   #fsm la machine à dessiner, Tlabel=0 affichage de nom des transitions Tlabel=1 affichage de nom des transitions et du vecteur d'entree
    """generation du graphique."""
    try:
        import pygraphviz as pgv           #librarie pour faire le dessin
    except ImportError:
        pgv = None    

    if title is None:                      #si None affiche le nom de la machine à etat
        title = fsm.name
    elif title is False:                   #si False affiche rien
        
        title = ''                          
    else:                                  #si non  affiche le nom de la machine à etat fournie  suivie de l' etat courant
        title +=r'\n current state: '+ str(fsm.current_state.name) 
        
    
    fsm_graph = pgv.AGraph(label=title,labelloc = "t" , **fsm.DOT_ATTRS)
    fsm_graph.node_attr.update(State.DOT_ATTRS)

    for state in [fsm.init_state] + fsm.states:
        shape = State.DOT_ATTRS['shape']
        if hasattr(fsm, 'accepting_states'):
            if id(state) in [id(s) for s in fsm.accepting_states]:
                shape = state.DOT_ACCEPTING
                if state.name==fsm.current_state:
                    fsm_graph.add_node(n=state.name, shape=shape,color='red')	#dessin l'etat courant en rouge
                else:
		    fsm_graph.add_node(n=state.name, shape=shape,color='black')
     
    fsm_graph.add_node('null', shape='plaintext', label=' ')
    fsm_graph.add_edge('null', fsm.init_state.name)

    for src, input_value, dst in fsm.all_transitions:
        if Tlabel==-1:
            label = str(input_value)
        else:
            label = str(input_value[Tlabel])
        if isinstance(fsm, MealyMachine):
            
            label += ' / %s' % dict(src.output_values).get(input_value)
        
            fsm_graph.add_edge(src.name, dst.name, label=label)
        
    for state in fsm.states:
        if state.default_transition is not None:
            fsm_graph.add_edge(state.name, state.default_transition.name, 
                               label='else')
    return fsm_graph





def on_message(ws, message):  # a chaque message recu je cree l'objet image
	global Llimitetoleree
	global Objetdecode
    
	
	try:
            global Objetdecode
            print(message)
            Objetdecode=json.loads(message,object_hook=object_decoder) 
            
        except:
            pass
        #try:
        print(Objetdecode)
        print ('GPAfsm current state : '+ str(GPAfsm.current_state))
          
        if str(type(Objetdecode))=="<class '__main__.palpeur'>":
            Palpeur1=copy.deepcopy(Objetdecode)
            print("jambon")
            #print(Palpeur1.mesure)
            #print ('GPAfsm current state apres decodage: '+ str(GPAfsm.current_state))
            #print ('mesure palpeur :'+ str(int(Palpeur1.mesure)))
            #print ('hors gamme :'+str(Llimitetoleree(int(Palpeur1.mesure))))
            
            
                #try:
            #print(list(GPAfsm.processSingle((False,evaluation(int(Palpeur1.mesure))))))
            list(GPAfsm.processSingle((False,evaluationE1(int(Palpeur1.mesure)))))                #commande pour exectuter l'elevateur1
            list(Generalfsm.processSingle((False,evaluationG1)))
            #GPAfsm.__getitem__(False,evaluation(int(Palpeur1.mesure)))
            #print((False,evaluation(int(Palpeur1.mesure))))
            print ('GPAfsm current state : '+ str(GPAfsm.current_state))
            #print ('Generalfsm current state : '+ str(Generalfsm.current_state))
                                            #si la reception est une trame json palpeur
                                            #je lance le process de la machine GPA.
                #del Palpeur1                    #le message à été traité je l' efface
                #print ('palpeur recu et traité')
                #except:
                    #pass
        #except:
            #pass

def on_error(ws, error):
    print(error)

def on_close(ws):
    global cursor
    global db
    print("### closed ###")
 
def display():
    print("patate")


def on_open(ws):
    def run(*args):
        thread.start_new_thread(run, ())
commandeStart = lambda : "on demarre"
if __name__ == "__main__":
    global Palpeur1
    global GPAfsm
    global Generalfsm
    Objetdecode=0
    websocket.enableTrace(True)
    GPAfsm = MealyMachine('gestion palpeur_Ascenseur')        
    Generalfsm = MealyMachine('gestion moteur d angle')  
    
#GPAfsm
    init = State('INIT',initial=True, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPAfsm, default=None) 
    repos = State('REPOS',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPAfsm, default=None) 
    moteurmonte = State('MOTEURMONTE',initial=False, accepting=False, output=commandeStart(),on_entry=display(), on_exit=display(), on_input=NOOP_ARG,on_transition=display(), machine=GPAfsm, default=None)
    moteurdescend = State('MOTEURDESCEND',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPAfsm, default=None) 
    erreur = State('ERREUR',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPAfsm, default=None) 
#Generalfsm
    generalinit = State('INIT',initial=True, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=Generalfsm, default=None)
    elevateur1 = State('ELEVATEUR1',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=Generalfsm, default=None)
    elevateur2 = State('ELEVATEUR2',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=Generalfsm, default=None)
    mesure = State('MESURE',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=Generalfsm, default=None)
    erreur = State('ERREUR',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=Generalfsm, default=None)
    bouge = State('BOUGE',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=Generalfsm, default=None)


    #ecriture des fonctions lambda de sortie à l'affichage
    Linit=  lambda : "inialisation GPAfsm"                                           
    LmoteurMonte=lambda : "j'envoie l'ordre de monter"
    LmoteurDescend=lambda : "j'envoie l'ordre de descendre"
    LmoteurArret=lambda : "j'envoie l'ordre d'arreter"
    Lnefairerien=lambda : "on est dans la gamme"
    Lerreur=lambda : "erreur"
    #ecriture des fonctions lambda d'entrée
                                                 
    
    def evaluationG1(x):                                                             #fonction qui gère toutes les conditions des transitions de la machine a etat générale
        if (GPAfsm.current_state==erreur):
            return "erreur"
        sens = "anglecroissant"
        if sens=="anglecroissant":                                                            
            if (Generalfsm.current_state==bouge):
                if (GPAfsm.current_state==repos):                # ==> reste dans bouge
                    return "inactif"                               
                if ((GPAfsm.current_state==moteurmonte)or(GPAfsm.current_state==moteurdescend)):                # ==> va dans E1
                    return "compensation1"
            if ((Generalfsm.current_state==elevateur1)or(Generalfsm.current_state==elevateur2)):
                if (GPAfsm.current_state==repos):
                    return "retourbouge"
                if ((GPAfsm.current_state!=repos)and(GPAfsm.current_state!=erreur)):
                    return "compensation1"
            



    def evaluationE1(x):                                                                      #fonction qui gère toutes les conditions des transitions de l'elevateur 1
        sens = "anglecroissant"
        print("poireaux")
        if sens=="anglecroissant":                                                            #test sur le sens du moteur pas a pas, determine les tests sur les GPA
            if (GPAfsm.current_state==repos):
                if ((x>5)&(x<95)):
                    return "inactif"
                if ((x>=95)&(x<98)):
                    return "monte"
                if ((x<=2)|(x>=98)):
                    return "erreur"

            if (GPAfsm.current_state==moteurmonte):
                if ((x>5)&(x<98)):
                    return "monte"
                if ((x<=5)&(x>2)):
                    return "inactif"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                
        if sens=="angledecroissant":
            if (GPAfsm.current_state==repos):
                if ((x>5)&(x<95)):
                    return "inactif"
                if ((x<=5)&(x>2)):
                    return "descend"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                
            if (GPAfsm.current_state==moteurdescend):
                if ((x<95)&(x>2)):
                    return "descend"
                if ((x>=95)&(x<98)):
                    return "inactif"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                 


    
    LstatusOK=lambda x: x=="erreur Moteur"                                             
    Loperation=lambda x : x
    # ecriture des vecteurs d'entrée

    #etatdedépart[('nom de la transion',(Lstatus,Llimitetoleree,operation)),output]=etatdarrive

    init[(r'debut',(False,dontcare)),Linit()]=repos
    repos[(r'disfonctionnement',(True,dontcare)),Lerreur()]=erreur                           # si erreur moteur quelquesoit valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    repos[('gammeOK',(False,"inactif")),Lnefairerien()]=repos                                # on est dans la gamme 5% et 95% pas de changement necessaire
    repos[(r'conditionmonte',(False,"monte")),LmoteurMonte()]=moteurmonte                    # on est hors gamme,on est dans la phase montante, un ratrappage est nécessaire on passe à l' etat moteur monte
    repos[( r'conditiondescend',(False,"descend")),LmoteurDescend()]=moteurdescend           # on est hors gamme,on est dans la phase descendante, un ratrappage est nécessaire on passe à l' etat moteur descent
    repos[(r'disfonctionnement',(False,"erreur")),Lerreur()]=erreur
    moteurmonte[( r'monteefinie',(False,"inactif")),LmoteurArret()]=repos                    # le moteur est en train de monter,on a fini de le ratapage on est en fin de plage descendantete
    moteurmonte[(r'monte',(False,"monte"))]=moteurmonte                                      # le moteur est en train de monter,on a pas fini le ratapage on continue en restant dans cette etat
    moteurmonte[(r'disfonctionnement1',(True,dontcare)),Lerreur()]=erreur                    # si erreur moteur quelques soit la valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    moteurmonte[(r'disfonctionnement1',(False,"erreur")),Lerreur()]=erreur
    moteurdescend[(r'descentefinie',(False,"inactif")),LmoteurArret()]=repos                 # le moteur est en train de descendre,on a fini de le ratapage on est en fin de plage descendante, on passe dans l'etat repos
    moteurdescend[(r'descend',(False,"descend")),LmoteurDescend()]=moteurdescend             # le moteur est en train de descendre,on a pas fini le ratapage on continue en restant dans cette etat
    moteurdescend[(r'disfonctionnement2',(True,dontcare)),Lerreur()]=erreur                  # si erreur moteur quelques soit la valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    moteurdescend[(r'disfonctionnement2',(False,"erreur")),Lerreur()]=erreur 


    generalinit[r'debut',(False,dontcare)]=bouge
    bouge[r'actionE1',(False,"compensation1")]=elevateur1
    bouge[r'actionE2',(False,"compensation2")]=elevateur2
    elevateur1[r'elevation1finie',(False,"retourbouge")]=bouge
    elevateur2[r'elevation2finie',(False,"retourbouge")]=bouge


    #print ("avant reset" + str( GPAfsm.current_state))
    GPAfsm. reset()
    Generalfsm. reset()
    #print ("apres reset" + str( GPAfsm.current_state))
    print(list(GPAfsm.processSingle((False,dontcare))))
    #print ("apres process" +str(GPAfsm.current_state))
    #print(GPAfsm.all_transitions)
    ws = websocket.WebSocketApp("ws://localhost:8080/ws",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
    

#{"palpeur1":{"mesure":"94","error":"none"}} 
