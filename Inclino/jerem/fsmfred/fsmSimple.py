# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 09:02:16 2019

@author: lauret
"""

#!/usr/bin/python
NOOP = lambda: None
NOOP_ARG = lambda arg: None
from fsmM import FiniteStateMachine, get_graph, State
import time
def commandeStart():
	print("on commence")

def commandeON():
	print("j'allume la lumi�re")
def commandeOFF():
	print("j'eteins la lumi�re")
def on():
	global i
	if i==1:
		return True
	else:
		return False 
global i
i=0
maFSM = FiniteStateMachine('Clignotant')
STATES = ['START', 'ALLUMER', 'ETEINT']
start,allumee, eteint= [State(s) for s in STATES]
#objet contenant les differents etats
#etat initial
start = State('START',initial=True, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=maFSM, default=None) 
#etat suivant

allumee=State('ALLUMER',initial=False, accepting=False, output=commandeON(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=maFSM, default=None)
eteint = State('ETEINT',initial=False, accepting=False, output=commandeOFF(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=maFSM, default=None) 
print (start)

# start.update({r'(mise en route)':start})
# start.update({r'(mise en route)':allumee})
# allumee.update({r'(eteindre)':eteint})
# eteint.update({r'(allumer)':allumee})

start.__setitem__(lambda x :x==1,start)
start.__setitem__(lambda x :x==2, allumee)
allumee.__setitem__(lambda x :x==1, eteint)
eteint.__setitem__(lambda x :x==1, allumee)




graph = get_graph(maFSM)
graph.draw('tcp1.png', prog='dot')
maFSM.reset()
print(maFSM.current_state)
while (True):
	i=True
	maFSM.processSingle(1)
	sleep(1)

