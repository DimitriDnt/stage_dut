# -*- coding: utf-8 -*-
"""
Created on Thu May  2 19:48:30 2019

@author: lauret
"""

_version__ = '0.01'

from apng import APNG      #librarie pour faire des images png animées
import json                #librarie pour utiliser les objet json
import websocket           #librarie pour faire unclient websocket
try:
    import thread          #librarie pour lancer le client websocket dans un tread
except ImportError:
    import _thread as thread
import copy             
#decodage des trames json arrivant du Websocket

#il faut une classe pour chaque type object communiquant

class MoteurAscenseur(object):
    def __init__(self, position, vitesse, error ):
        self.position =float( position)
        self.vitesse = float(vitesse)
        self.error = error
class palpeur(object):
    def __init__(self, mesure, error ):
        self.mesure =float(mesure)
        self.error = error
        
#il faut un bloc if pour chaque object communiquant        
def object_decoder(obj):
    #print("suricate")
    #print(obj)
        
    if 'Moteur1' in obj:
              
        return MoteurAscenseur(obj['Moteur1']['position'], obj['Moteur1']['vitesse'], obj['Moteur1']['error'])

    elif 'Moteur2' in obj:
              
        return MoteurAscenseur(obj['Moteur2']['position'], obj['Moteur2']['vitesse'], obj['Moteur2']['error'])

    elif 'palpeur1' in obj:
            
        return palpeur(obj['palpeur1']['mesure'], obj['palpeur1']['error'])

    elif 'palpeur2' in obj:
            
        return palpeur(obj['palpeur2']['mesure'], obj['palpeur2']['error'])


    return obj


 
MACHINES = dict()

NOOP = lambda: None
NOOP_ARG = lambda arg: None
# librairie pout utilisation du troiéme état X dontcare

class _DontCare(object):
    def __lt__(self, other):
        return True
    
    def __gt__(self, other):
        return True
    
    def __eq__(self, other):
        return True
    
    def __add__(self, other):
        return other
    
    def __sub__(self, other):
        return 0 if self is other else -other
    
    def __mul__(self, other):
        return other

    def __pow__(self, p):
        return self
    
    def __floordiv__(self, other):
        return 1//other
    
    def __div__(self, other):
        return 1/other
    
    def __mod__(self, other):
        return 0
    
    def __and__(self, other):
        return True
    
    def __or__(self, other):
        return True
    
    def __xor__(self, other):
        return True
    
    
    def __rlt__(self, other):
        return True
    
    def __rgt__(self, other):
        return True
    
    def __req__(self, other):
        return True
    
    def __radd__(self, other):
        return other
    
    def __rsub__(self, other):
        return other
    
    def __rmul__(self, other):
        return other

    def __rpow__(self, p):
        return p
    
    def __rfloordiv__(self, other):
        return other
    
    def __rdiv__(self, other):
        return other
    
    def __rmod__(self, other):
        return 0
    
    def __rand__(self, other):
        return True
    
    def __ror__(self, other):
        return True
    
    def __rxor__(self, other):
        return True
    
    def __str__(self):
        return "dontcare"
    
    def __repr__(self):
        return "dontcare"

dontcare = _DontCare()
# librairie pour les machines à etat

class FSMError(Exception):
    """Base FSM exception."""
    pass

class TransitionError(FSMError):
    """Transition exception."""
    pass

class StateError(FSMError):
    """State manipulation error."""


class FiniteStateMachine(object):
    
    """Machine à etat generique."""

    DOT_ATTRS = {
        'directed': True,
        'strict': False,
        'rankdir': 'LR',
        'ratio': '0.3'
    }

    def __init__(self, name, default=True):
        """Constructeur de la FSM."""
        self.name = name
        FiniteStateMachine._setup(self)
        self._setup()
        self.current_state = None
        MACHINES[name] = self
        if default:
            MACHINES['default'] = MACHINES[name]

    def _setup(self):
        """Setup de la FSM."""
        # All finite state machines share the following attributes.
        self.inputs = list()
        self.states = list()
        self.init_state = None

    @property
    def all_transitions(self):
        """donne l'ensemble des transition d'un etat.
        
        Returns: une list de 3 elements tuples (source state, input, destination state)
        """
        transitions = list()
        for src_state in self.states:
            for input_value, dst_state in src_state.items():
                transitions.append((src_state, input_value[1], dst_state))
        return transitions

    def conforme(self,a,b):  #verification bit a bit de l' egalite des tuples en input
                for k in zip(a,b):
			if ((k[0]!=k[1]) and ((k[0]!=dontcare) and (k[1]!=dontcare))):
				return False

		return True    
    def transition(self, input_value):
        
        """evalue les transition vers l'etat suivant depuis l'etat courent."""
        
        current = self.current_state
        if current is None:
            raise TransitionError('etat courent non definie.')
        Newinput_value=list()
        #print("liste des transitions")
        for i in current:
            j=i[0][1]
            #print ('input value entree de transition'+str(input_value))
            #print ("j"+ str(j))
            #print ("input_value"+str(self.conforme(j,input_value)))
            if (self.conforme(j,input_value)):
                Newinput_value.append(i[0][0])
                Newinput_value.append(i[0][1])
                Newinput_valueA=tuple(Newinput_value)
                Newinput_value=list()
                Newinput_value.append(Newinput_valueA)
                Newinput_value.append(i[1])
                break
        #print ('input value sortie de transition'+str(Newinput_value))
        destination_state = current.get(tuple(Newinput_value), current.default_transition)
        if destination_state is None: 
            raise TransitionError('ne peut faire de transition depuis l etat %r'
                                  ' avec cette entree %r.' % (current.name, (dontcare,input_value)))
        else:
            self.current_state = destination_state                                      #changement d'etat effectif ici
            #print("image crée")                                               #on fait un png pour le nouvelle etat
            get_graph(GPA1fsm,0,"Machine a etat pour la gestion du couple moteur palpeur ").draw('GPA1current.png', prog='dot')
            im = APNG.open(self.name+"animation.png")       # on ajouter le nouvel etat a l'animation
            files = [ (self.name+'current.png', 100)]
            for file, delay in files:
                im.append_file(file, delay=delay)
            im.save(self.name+"animation.png")

    def reset(self):
        """reset de la machine à état."""
        self.current_state = self.init_state
                                                            #on fait un png pour cette  etat initial
        get_graph(GPA1fsm,0,"Machine a etat pour la gestion du couple moteur palpeur ").draw(self.name+'current.png', prog='dot')
        im = APNG()
        files = [ (self.name+'current.png', 100)]          # on cree une nouvelle  l'animation avec le png initial
        for file, delay in files:
            im.append_file(file, delay=delay)         
        im.save(self.name+"animation.png")
        



    def process(self, input_data):                          #process une liste de vecteur d'entree en faisant d'abord un reset
        """Process input data."""
        self.reset()
        for item in input_data:
            self.transition(item)
    def processSingle(self, input_data):                    # process un vecteur d'entree depuis l'etat courant              
        """Process input data."""
        self.transition(input_data)

class Acceptor(FiniteStateMachine):

    """Acceptor machine."""

    def _setup(self):
        """Setup an acceptor."""
        self.accepting_states = list()

    def process(self, input_data):
        """Process input data."""
        self.reset()
        for item in input_data:
            self.transition(item[1])
        return id(self.current_state) in [id(s) for s in self.accepting_states]


class Transducer(FiniteStateMachine):
    
    """A semiautomaton transducer."""

    def _setup(self):
        """Setup a transducer."""
        self.outputs = list()

    def output(self, input_value):
        """Return state's name as output."""
        return self.current_state.name

    def process(self, input_data, yield_none=True):
        """Process input data."""
        self.reset()
        for item in input_data:
            if yield_none: 
                yield self.output(item)
            elif self.output(item) is not None:
                yield self.output(item)
            self.transition(item)
    def processSingle(self, input_data, yield_none=True):
        """Process input data."""
        #print('transducer processSingle'+ str(input_data))
        if yield_none: 
            yield self.output(input_data)
        elif self.output(input_data) is not None:
                yield self.output(input_data)
        self.transition(input_data)


class MooreMachine(Transducer):
    
    """Moore Machine."""

    def output(self, input_value):
        """Return output value assigned to the current state."""
        return self.current_state.output_values[0][1]


class MealyMachine(Transducer):
    
    """Mealy Machine."""

    def output(self, input_value):
        """Return output for a given state transition."""
        #print ("mealyMachine" + str(input_value))
        return dict(self.current_state.output_values).get(input_value) 


class State(dict):
    
    """State class."""

    DOT_ATTRS = {
        'shape': 'circle',
        'height': '1.2',
    }
    DOT_ACCEPTING = 'doublecircle'

    def __init__(self, name, initial=False, accepting=False, output=None,
                 on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG, 
                 on_transition=NOOP_ARG, machine=None, default=None):
        """Construct a state."""
        dict.__init__(self)
        self.name = name
        self.entry_action = on_entry
        self.exit_action = on_exit
        self.input_action = on_input
        self.transition_action = on_transition
        self.output_values = [(None, output)]
        self.default_transition = default
        if machine is None:
            try:
                machine = MACHINES['default']
            except KeyError:
                pass

        if machine:
            machine.states.append(self)
            if accepting:
                try:
                    machine.accepting_states.append(self)
                except AttributeError:
                    raise StateError('The %r %s does not support accepting '
                                     'states.' % (machine.name, 
                                     machine.__class__.__name__))
            if initial:
                machine.init_state = self

    def __getitem__(self, input_value):
        """Make a transition to the next state."""
        next_state = dict.__getitem__(self, input_value)
        self.input_action(input_value)
        self.exit_action()
        self.transition_action(next_state)
        next_state.entry_action()
        return next_state

    def __setitem__(self, input_value, next_state):
        """Set a transition to a new state."""
        if not isinstance(next_state, State):
            raise StateError('A state must transition to another stateut_value')
            self.output_values.append((input_value, output_value))
        dict.__setitem__(self, input_value, next_state)

    def __repr__(self):
        """Represent the object in a string."""
        return '<%r %s @ 0x%x>' % (self.name, self.__class__.__name__, id(self))


def get_graph(fsm,Tlabel=-1,title=None):   #fsm la machine à dessiner, Tlabel=0 affichage de nom des transitions Tlabel=1 affichage de nom des transitions et du vecteur d'entree
    """generation du graphique."""
    try:
        import pygraphviz as pgv           #librarie pour faire le dessin
    except ImportError:
        pgv = None    

    if title is None:                      #si None affiche le nom de la machine à etat
        title = fsm.name
    elif title is False:                   #si False affiche rien
        
        title = ''                          
    else:                                  #si non  affiche le nom de la machine à etat fournie  suivie de l' etat courant
        title +=r'\n current state: '+ str(fsm.current_state.name) 
        
    
    fsm_graph = pgv.AGraph(label=title,labelloc = "t" , **fsm.DOT_ATTRS)
    fsm_graph.node_attr.update(State.DOT_ATTRS)

    for state in [fsm.init_state] + fsm.states:
        shape = State.DOT_ATTRS['shape']
        if hasattr(fsm, 'accepting_states'):
            if id(state) in [id(s) for s in fsm.accepting_states]:
                shape = state.DOT_ACCEPTING
                if state.name==fsm.current_state:
                    fsm_graph.add_node(n=state.name, shape=shape,color='red')	#dessin l'etat courant en rouge
                else:
		    fsm_graph.add_node(n=state.name, shape=shape,color='black')
     
    fsm_graph.add_node('null', shape='plaintext', label=' ')
    fsm_graph.add_edge('null', fsm.init_state.name)

    for src, input_value, dst in fsm.all_transitions:
        if Tlabel==-1:
            label = str(input_value)
        else:
            label = str(input_value[Tlabel])
        if isinstance(fsm, MealyMachine):
            
            label += ' / %s' % dict(src.output_values).get(input_value)
        
            fsm_graph.add_edge(src.name, dst.name, label=label)
        
    for state in fsm.states:
        if state.default_transition is not None:
            fsm_graph.add_edge(state.name, state.default_transition.name, 
                               label='else')
    return fsm_graph




sens="anglecroissant"
def on_message(ws, message):  # a chaque message recu je cree l'objet image
	global Llimitetoleree
	global Objetdecode
        
	
	try:
            print("sortie de json load"+str(message))
            Objetdecode=json.loads(message,object_hook=object_decoder) 
            #print("sortie de json load"+str(message))
            #print("message"+str (message))
            #print("objetdecode"+str (Objetdecode))
        except:
            pass
        #try:
        #print ('GPA1fsm current state : '+ str(GPA1fsm.current_state))
        #print ('MOTEURfsm current state : '+ str(MOTEURfsm.current_state))

            
        if str(type(Objetdecode))=="<class '__main__.palpeur'>":
            #print("jambon")
            if 'palpeur1' in message:                                               #check fontion "item"
                Palpeur1=copy.deepcopy(Objetdecode)
                list(GPA1fsm.processSingle((False,evaluationE1(int(Palpeur1.mesure)))))
            if 'palpeur2' in message:
                Palpeur2=copy.deepcopy(Objetdecode)
                list(GPA2fsm.processSingle((False,evaluationE2(int(Palpeur2.mesure)))))
                    
            #print(Objetdecode)  
            #print(Palpeur1)
            #print(Palpeur1.mesure)
            #print ('GPA1fsm current state apres decodage: '+ str(GPA1fsm.current_state))
            #print ('mesure palpeur :'+ str(int(Palpeur1.mesure)))
            #print ('hors gamme :'+str(Llimitetoleree(int(Palpeur1.mesure))))
            
            
                #try:
            #print(list(GPA1fsm.processSingle((False,evaluation(int(Palpeur1.mesure))))))
            
            
            

            etat1=GPA1fsm.current_state
            etat2=GPA2fsm.current_state
            #print (etat1)
            #print (etat2)
            #print ('MOTEURfsm current state : '+ str(MOTEURfsm.current_state))
            list(MOTEURfsm.processSingle((False,evaluationM1(etat1,etat2))))
            #print((False,evaluation(int(Palpeur1.mesure))))
            print ('GPA1fsm current state : '+ str(GPA1fsm.current_state))
            print ('GPA2fsm current state : '+ str(GPA2fsm.current_state))
            print ('MOTEURfsm current state : '+ str(MOTEURfsm.current_state))
                                            #si la reception est une trame json palpeur
                                            #je lance le process de la machine GPA1.
            Objetdecode = None                   #le message à été traité je l' efface
                #print ('palpeur recu et traité')
                #except:
                    #pass
        #except:
            #pass

def on_error(ws, error):
    print(error)

def on_close(ws):
    global cursor
    global db
    print("### closed ###")
 
def display():
    print("patate")

def on_open(ws):
    def run(*args):
        thread.start_new_thread(run, ())
commandeStart = lambda : "on demarre"
if __name__ == "__main__":
    global Llimitetoleree
    global Palpeur1
    global GPA1fsm
    global GPA2fsm
    global BANCSTARTfsm
    global MOTEURfsm
    Objetdecode=0
    e=0
    websocket.enableTrace(True)
    GPA1fsm = MealyMachine('gestion palpeur_Ascenseur1')
    GPA2fsm = MealyMachine('gestion palpeur_Ascenseur2')
    BANCSTARTfsm = MealyMachine('gestion initialisation du banc')
    MOTEURfsm = MealyMachine('gestion moteur d angle')  

    
#GPA1fsm
    init1 = State('INIT',initial=True, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA1fsm, default=None) 
    repos1 = State('REPOS',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA1fsm, default=None) 
    moteurmonte1 = State('MOTEURMONTE',initial=False, accepting=False, output=commandeStart(),on_entry=display(), on_exit=display(), on_input=NOOP_ARG,on_transition=display(), machine=GPA1fsm, default=None)
    moteurdescend1 = State('MOTEURDESCEND',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA1fsm, default=None) 
    erreur1 = State('ERREUR',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA1fsm, default=None) 


#GPA2fsm
    init2 = State('INIT',initial=True, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA2fsm, default=None) 
    repos2 = State('REPOS',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA2fsm, default=None) 
    moteurmonte2 = State('MOTEURMONTE',initial=False, accepting=False, output=commandeStart(),on_entry=display(), on_exit=display(), on_input=NOOP_ARG,on_transition=display(), machine=GPA2fsm, default=None)
    moteurdescend2 = State('MOTEURDESCEND',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA2fsm, default=None) 
    erreur2 = State('ERREUR',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=GPA2fsm, default=None) 


#MOTEURfsm
    MOTEURinit = State('INIT',initial=True, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=MOTEURfsm, default=None)
    elevateur1 = State('ELEVATEUR1',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=MOTEURfsm, default=None)
    elevateur2 = State('ELEVATEUR2',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=MOTEURfsm, default=None)
    mesure = State('MESURE',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=MOTEURfsm, default=None)
    erreur = State('ERREUR',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=MOTEURfsm, default=None)
    bouge = State('BOUGE',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=MOTEURfsm, default=None)
    moteurimmobile = State('DODO',initial=False, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=MOTEURfsm, default=None)
    

#BANCSTARTfsm
    BANCSTARTfsminit = State('INIT',initial=True, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=BANCSTARTfsmfsm, default=None)
    moteur0 = State('InitialisationMoteurPalpeurs',initial=false, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=BANCSTARTfsmfsm, default=None)
    angle0 = State('InitialisationMoteurAngle',initial=false, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=BANCSTARTfsmfsm, default=None)
    contactpalpeurs = State('contactpalpeur',initial=false, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=BANCSTARTfsmfsm, default=None)
    anglemini = State('InitialisationAngleMinimum',initial=false, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=BANCSTARTfsmfsm, default=None)
    demaragefini = State('fini',initial=false, accepting=False, output=commandeStart(),on_entry=NOOP, on_exit=NOOP, on_input=NOOP_ARG,on_transition=NOOP_ARG, machine=BANCSTARTfsmfsm, default=None)
    erreurinit    


    
    #ecriture des fonctions lambda de sortie à l'affichage
    Linit=  lambda : "inialisation GPA1fsm"                                           
    LmoteurMonte=lambda : "j'envoie l'ordre de monter"
    LmoteurDescend=lambda : "j'envoie l'ordre de descendre"
    LmoteurArret=lambda : "j'envoie l'ordre d'arreter"
    Lnefairerien=lambda : "on est dans la gamme"
    Lerreur=lambda : "erreur"
    #ecriture des fonctions lambda d'entrée
                                                 

    def evaluationM1(x,y):                                                             #fonction qui gère toutes les conditions des transitions de la machine a etat générale
        #if (MOTEURfsm.current_state==MOTEURinit):
        #    return "start"
        global e
        if e==1:
            return "erreur"
        if (x==erreur):
            return "erreur"
        global sens 
        if sens=="anglecroissant":                                                            
            if (MOTEURfsm.current_state==bouge):
                if((x!=repos1)and(x!=erreur1)and(y!=repos2)and(y!=erreur2)):
                    e=1                                            #variable pour la généralisation de l'état erreur dans toutes les machines
                    return "erreur"
                if ((x==repos1)and(y==repos2)):                # ==> reste dans bouge
                    return "inactif"                               
                if ((x==moteurmonte1)or(x==moteurdescend1)):                # ==> va dans E1
                    return "compensation1"
                if ((y==moteurmonte2)or(y==moteurdescend2)):                # ==> va dans E1
                    return "compensation2"                
            if ((MOTEURfsm.current_state==elevateur1)or(MOTEURfsm.current_state==elevateur2)):
                #print("legume")
                #print(x)
                #print(y)
                if ((x==repos1))and(y==repos2):
                    #print("ninja")
                    return "retourbouge"
            if (MOTEURfsm.current_state==elevateur1):
                if (y!=repos2):
                    e=1
                    return "erreur"
                if ((x!=repos1)and(x!=erreur1)and(y==repos2)):
                    #print("pawa")
                    return "compensation1"
            if (MOTEURfsm.current_state==elevateur2):
                if (x!=repos1):
                    e=1
                    return "erreur"
                if ((y!=repos2)and(y!=erreur2)and(x==repos1)):
                    return "compensation2"
            
        

    #Llimitetoleree=lambda x: ((x>5) & (x<95))
    def evaluationE1(x):                                                                          #fonction qui gère toutes les conditions des transitions de GPA1fsm
        global sens
        global e
        if e==1:
            return "erreur"
        if sens=="anglecroissant":                                                            
            if (GPA1fsm.current_state==repos1):
                if ((x>5)&(x<95)):
                    return "inactif"
                if ((x>=95)&(x<98)):
                    return "monte"
                if ((x<=2)|(x>=98)):
                    return "erreur"

            if (GPA1fsm.current_state==moteurmonte1):
                #print("jambon")
                if ((x>5)&(x<98)):
                    return "monte"
                if ((x<=5)&(x>2)):
                    #print("lapin")
                    return "inactif"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                
        if sens=="angledecroissant":
            if (GPA1fsm.current_state==repos1):
                if ((x>5)&(x<95)):
                    return "inactif"
                if ((x<=5)&(x>2)):
                    return "descend"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                
            if (GPA1fsm.current_state==moteurdescend1):
                if ((x<95)&(x>2)):
                    return "descend"
                if ((x>=95)&(x<98)):
                    return "inactif"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                 
    def evaluationE2(x):                                                                          #fonction qui gère toutes les conditions des transitions de GPA2fsm
        global sens
        global e
        if e==1:
            return "erreur"
        if sens=="angledecroissant":                                                           
            if (GPA2fsm.current_state==repos2):
                if ((x>5)&(x<95)):
                    return "inactif"
                if ((x>=95)&(x<98)):
                    return "monte"
                if ((x<=2)|(x>=98)):
                    return "erreur"

            if (GPA2fsm.current_state==moteurmonte2):
                if ((x>5)&(x<98)):
                    return "monte"
                if ((x<=5)&(x>2)):
                    return "inactif"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                
        if sens=="anglecroissant":
            if (GPA2fsm.current_state==repos2):
                if ((x>5)&(x<95)):
                    return "inactif"
                if ((x<=5)&(x>2)):
                    return "descend"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                
            if (GPA2fsm.current_state==moteurdescend2):
                if ((x<95)&(x>2)):
                    return "descend"
                if ((x>=95)&(x<98)):
                    return "inactif"
                if ((x<=2)|(x>=98)):
                    return "erreur"
                 
    def evaluationSTART(x,y,a):
        sens = angledecroissant
        global e
        if (e==1):
            return"erreur"
        if (BANCSTARTfsm.current_state==moteur0):

        if (BANCSTARTfsm.current_state==angle0):

        if (BANCSTARTfsm.current_state==contactpalpeurs):
            if(x>30):
                moteur1avance(100)
            if((x<=30)and(x>5)):
                moteur1avance(50)
            if(x<=5):
                moteur1immobile()
            if(x>30):
                moteur2avance(100)
            if((x<=85)and(x>50)):
                moteur2avance(50)
            if(x<=50):
                moteur2immobile()

        if (BANCSTARTfsm.current_state==anglemini):



    
    LstatusOK=lambda x: x=="erreur Moteur"                                             
    Loperation=lambda x : x
    # ecriture des vecteurs d'entrée

    #etatdedépart[('nom de la transion',(Lstatus,Llimitetoleree,operation)),output]=etatdarrive
#GPA1fsm
    init1[(r'debut',(False,dontcare)),Linit()]=repos1
    repos1[(r'disfonctionnement',(True,dontcare)),Lerreur()]=erreur1                          # si erreur moteur quelquesoit valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    repos1[('gammeOK',(False,"inactif")),Lnefairerien()]=repos1                                # on est dans la gamme 5% et 95% pas de changement necessaire
    repos1[(r'conditionmonte',(False,"monte")),LmoteurMonte()]=moteurmonte1                    # on est hors gamme,on est dans la phase montante, un ratrappage est nécessaire on passe à l' etat moteur monte
    repos1[( r'conditiondescend',(False,"descend")),LmoteurDescend()]=moteurdescend1           # on est hors gamme,on est dans la phase descendante, un ratrappage est nécessaire on passe à l' etat moteur descent
    repos1[(r'disfonctionnement',(False,"erreur")),Lerreur()]=erreur1
    moteurmonte1[( r'monteefinie',(False,"inactif")),LmoteurArret()]=repos1                    # le moteur est en train de monter,on a fini de le ratapage on est en fin de plage descendantete
    moteurmonte1[(r'monte',(False,"monte")),LmoteurMonte()]=moteurmonte1                                      # le moteur est en train de monter,on a pas fini le ratapage on continue en restant dans cette etat
    moteurmonte1[(r'disfonctionnement1',(True,dontcare)),Lerreur()]=erreur1                    # si erreur moteur quelques soit la valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    moteurmonte1[(r'disfonctionnement1',(False,"erreur")),Lerreur()]=erreur1
    moteurdescend1[(r'descentefinie',(False,"inactif")),LmoteurArret()]=repos1                 # le moteur est en train de descendre,on a fini de le ratapage on est en fin de plage descendante, on passe dans l'etat repos
    moteurdescend1[(r'descend',(False,"descend")),LmoteurDescend()]=moteurdescend1             # le moteur est en train de descendre,on a pas fini le ratapage on continue en restant dans cette etat
    moteurdescend1[(r'disfonctionnement2',(True,dontcare)),Lerreur()]=erreur1                  # si erreur moteur quelques soit la valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    moteurdescend1[(r'disfonctionnement2',(False,"erreur")),Lerreur()]=erreur1 
    erreur1[(r'erreur',(False,"erreur")),Lerreur()]=erreur1

#GPA2fsm
    init2[(r'debut',(False,dontcare)),Linit()]=repos2
    repos2[(r'disfonctionnement',(True,dontcare)),Lerreur()]=erreur2                          # si erreur moteur quelquesoit valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    repos2[('gammeOK',(False,"inactif")),Lnefairerien()]=repos2                                # on est dans la gamme 5% et 95% pas de changement necessaire
    repos2[(r'conditionmonte',(False,"monte")),LmoteurMonte()]=moteurmonte2                    # on est hors gamme,on est dans la phase montante, un ratrappage est nécessaire on passe à l' etat moteur monte
    repos2[( r'conditiondescend',(False,"descend")),LmoteurDescend()]=moteurdescend2           # on est hors gamme,on est dans la phase descendante, un ratrappage est nécessaire on passe à l' etat moteur descent
    repos2[(r'disfonctionnement',(False,"erreur")),Lerreur()]=erreur2
    moteurmonte2[( r'monteefinie',(False,"inactif")),LmoteurArret()]=repos2                    # le moteur est en train de monter,on a fini de le ratapage on est en fin de plage descendantete
    moteurmonte2[(r'monte',(False,"monte")),LmoteurMonte()]=moteurmonte2                                      # le moteur est en train de monter,on a pas fini le ratapage on continue en restant dans cette etat
    moteurmonte2[(r'disfonctionnement1',(True,dontcare)),Lerreur()]=erreur2                    # si erreur moteur quelques soit la valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    moteurmonte2[(r'disfonctionnement1',(False,"erreur")),Lerreur()]=erreur2
    moteurdescend2[(r'descentefinie',(False,"inactif")),LmoteurArret()]=repos2                 # le moteur est en train de descendre,on a fini de le ratapage on est en fin de plage descendante, on passe dans l'etat repos
    moteurdescend2[(r'descend',(False,"descend")),LmoteurDescend()]=moteurdescend2             # le moteur est en train de descendre,on a pas fini le ratapage on continue en restant dans cette etat
    moteurdescend2[(r'disfonctionnement2',(True,dontcare)),Lerreur()]=erreur2                  # si erreur moteur quelques soit la valeur de Llimitetoleree et de operation on va dans erreur fsmMoteur-palpeur
    moteurdescend2[(r'disfonctionnement2',(False,"erreur")),Lerreur()]=erreur2 
    erreur2[(r'erreur',(False,"erreur")),Lerreur()]=erreur2

#MOTEURfsm
    moteurinit[(r'debut1',(False,dontcare)),Linit()]=bouge
    bouge[(r'continue',(False,"inactif")),Linit()]=bouge
    bouge[(r'actionE1',(False,"compensation1")),Linit()]=elevateur1
    bouge[(r'actionE2',(False,"compensation2")),Linit()]=elevateur2
    bouge[(r'erreur',(False,"erreur")),Linit()]=erreur
    bouge[(r'immobile',(False,"moteurstop"))Linit()]=moteurimmobile
    moteurimmobile[(r'deplace'(False,"moteurbouge"))Linit()]=bouge
    elevateur1[(r'elevation1finie',(False,"retourbouge")),Linit()]=bouge
    elevateur1[(r'continue',(False,"compensation1")),Linit()]=elevateur1
    elevateur1[(r'erreur',(False,"erreur")),Linit()]=erreur
    elevateur2[(r'elevation2finie',(False,"retourbouge")),Linit()]=bouge
    elevateur2[(r'elevation2finie',(False,"compensation2")),Linit()]=elevateur2
    elevateur2[(r'erreur',(False,"erreur")),Linit()]=erreur
    erreur[(r'erreur',(False,"erreur")),Lerreur()]=erreur

#BANCSTARTfsm
    startinit[(r'debutinitialisation',(False,dontcare)),Linit()]=moteur0
    moteur0[(r'chercheposition0',(False,"Chercheposition0"),Linit())]=moteur0
    moteur0[(r'erreur',(False,"erreur"),Linit())]=erreurinit
    moteur0[(r'initialisationmoteurpalpeur',(False,"moteur0atteint")),Linit()]=angle0
    angle0[(r'chercheangle0',(False,"chercheangle0")Linit())]=angle0
    angle0[(r'erreur',(False,"erreur")Linit())]=erreurinit
    angle0[(r'initialisationmoteurangle',(False,"angle0atteint")),Linit()]=contactpalpeurs
    contactpalpeurs[(r'monteepalpeurs',(False,"cherchecontact"),Linit())]=contactpalpeurs
    contactpalpeurs[(r'erreur',(False,"erreur"),Linit())]=erreurinit
    contactpalpeurs[(r'miseencontactdespalpeurs',(False,"contactfait")),Linit()]=anglemini
    anglemini[(r'anglemini',(False,"chercheanglemini")),Linit()]=anglemini
    anglemini[(r'erreur',(False,"erreur")),Linit()]=erreurinit
    anglemini[(r'initialisationfinie',(False,"initialisationfinie")),Linit()]=demaragefini
    
    GPA1fsm. reset()
    GPA2fsm. reset()
    BANCSTARTfsm. reset()
    MOTEURfsm. reset()
    
    print(list(GPA1fsm.processSingle((False,dontcare))))
    print(list(GPA2fsm.processSingle((False,dontcare))))
    print(list(MOTEURfsm.processSingle((False,dontcare))))
    
    
    ws = websocket.WebSocketApp("ws://localhost:8080/ws",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
    

#{"palpeur1":{"mesure":"94","error":"none"}} 


#blabla