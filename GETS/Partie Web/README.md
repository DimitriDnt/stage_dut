# Projet Geochemical Easily Transported System (GETS)

Travail réalisé durant ma participation au projet GETS.

## Répertoires de la partie Web

Il y a deux répertoires pour la partie Web du projet : **Configuration Apache** et **gets**.

#### Configuration Apache

```
Répertoire contenant les fichiers de configuration du serveur web installé sur le Raspberry Pi.
```

#### gets

```
Répertoire contenant tout le code réalisé lors du travail sur la partie Web du projet GETS.
```

## Dans le répertoire gets

#### bdd  

```
Contient les fichiers PHP pour se connecter à la base de données 
```

#### css 

```
Contient toutes les feuilles de style CSS pour la présentation et la forme des pages
```

#### html

```
Contient tous les fichiers HTML
```

#### images

```
Contient toutes les images utilisées et le fond de carte hors ligne
```

#### js 

```
Contient tous les fichiers et codes JavaScript
```

#### librairies 

```
Contient les librairies JavaScript utilisées (Bootstrap, Bootstrap Treeview, Highcharts...)
```

#### menus 

```
Contient les fichiers des barres de menus pour l'Administrateur et l'Utilisateur
```

#### php 
```
Contient tous les fichiers PHP
```
