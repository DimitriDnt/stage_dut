$(document).ready(function() {

    // Initialisation des variables
    json = null;
    data = null;
    delai = 300;
    var socket = new WebSocket("ws://192.168.1.25:81/ws");
    //var socket = new WebSocket("ws://192.168.1.121:81/ws");

    test = JSON.stringify({
        "GETS": {
            "ID": "GETS",
            "ordre": "getcfg"
        }
    });

    /************* CONNEXION AU SERVEUR WEB SOCKET ************/

    socket.onopen = function() {
        console.log("Connexion !");
    };

    socket.onmessage = function(message) {
        console.log("Receiving : " + message.data);
        data = message.data;
    };

    socket.onclose = function() {
        console.log("Deconnexion");
    };

    var sendMessage = function(message) {
        console.log("Envoi : " + message.data);
        socket.send(message.data);
    };

    // LSELECTION DE LA STATION GETS DANS LA LISTE
    $("#input").on('input', function() {

        // FONCTION POUR AFFICHER LE POPUP BDD OU WS
        function functionConfirm(msg, bdd, ws) {
            var confirmBox = $("#confirm");
            confirmBox.find(".message").text(msg);
            confirmBox.find(".bdd,.ws").unbind().click(function() {
                confirmBox.hide();
            });
            confirmBox.find(".bdd").click(bdd);
            confirmBox.find(".ws").click(ws);
            confirmBox.show();
        }

        functionConfirm("Choisissez où récupérer la configuration.",

            // SI ON CHOISIT LA LECTURE BDD
            function bdd() {

                alert("Lecture de la base de données effectuée !");
                window.location.replace("../php/conf_capteurs_bdd.php")
            },

            // SI ON CHOISIT LA LECTURE WS
            function ws() {

                var station = $("#input").val();

                switch (station) {

                    case "GETS 1":
                        sendMessage({
                            'data': test
                        });
                        break;

                    case "GETS 2":
                        sendMessage({
                            'data': test
                        });
                        break;

                    case "GETS 3":
                        sendMessage({
                            'data': test
                        });
                        break;

                    case "GETS 4":
                        sendMessage({
                            'data': "GETS4"
                        });
                        break;

                    case "GETS 5":
                        sendMessage({
                            'data': "GETS5"
                        });
                        break;

                    case "GETS 6":
                        sendMessage({
                            'data': "GETS6"
                        });
                        break;

                    case "GETS 7":
                        sendMessage({
                            'data': "GETS7"
                        });
                        break;

                    case "GETS 8":
                        sendMessage({
                            'data': "GETS8"
                        });
                        break;

                    case "GETS 9":
                        sendMessage({
                            'data': "GETS9"
                        });
                        break;

                    case "GETS 10":
                        sendMessage({
                            'data': "GETS10"
                        });
                        break;

                    default:

                        window.location.replace("../php/conf_capteurs.php");
                }

                setTimeout(function() {

                    if (data != null) {

                        json = JSON.parse(data);
                        afficherJSON();
                        alert("Configuration chargée avec succès !");
                        data = null;
                    } else {

                        alert("Erreur de communication avec la station choisie !");
                    }

                }, delai);
            });

    });

    // BOUTON POUR FAIRE REBOOT LE HELTEC
    $("#reset").click(function(ev) {

        var reset = JSON.stringify({
            "GETS": {
                "ID": "GETS",
                "ordre": "reboot"
            }
        });

        console.log(sendMessage({
            'data': reset
        }));
    });


    // BOUTON POUR ENVOYER UN JSON DE CONFIGURATION AU HELTEC
    $("#apply").click(function(ev) {

        var newJson = {};
        var newJson1 = {};

        /*************************************** CONVERTISSEUR 1 ***************************************/

        // VALEURS DU CAPTEUR 1
        var name = document.getElementById("name1").value;
        var gain = document.getElementById("gain1").value;
        var A0 = document.getElementById("A01").value;
        var A1 = document.getElementById("A11").value;
        var enable = document.getElementById("checkbox1").value;

        // VALEURS DU CAPTEUR 2
        var name1 = document.getElementById("name2").value;
        var gain1 = document.getElementById("gain2").value;
        var A01 = document.getElementById("A02").value;
        var A11 = document.getElementById("A12").value;
        var enable1 = document.getElementById("checkbox2").value;

        // VALEURS DU CAPTEUR 3
        var name2 = document.getElementById("name3").value;
        var gain2 = document.getElementById("gain3").value;
        var A02 = document.getElementById("A03").value;
        var A12 = document.getElementById("A13").value;
        var enable2 = document.getElementById("checkbox3").value;

        // VALEURS DU CAPTEUR 4
        var name3 = document.getElementById("name4").value;
        var gain3 = document.getElementById("gain4").value;
        var A03 = document.getElementById("A04").value;
        var A13 = document.getElementById("A14").value;
        var enable3 = document.getElementById("checkbox4").value;

        /*************************************** CONVERTISSEUR 2 ***************************************/

        // VALEURS DU CAPTEUR 1
        var name4 = document.getElementById("name5").value;
        var gain4 = document.getElementById("gain5").value;
        var A04 = document.getElementById("A05").value;
        var A14 = document.getElementById("A15").value;
        var enable4 = document.getElementById("checkbox5").value;

        // VALEURS DU CAPTEUR 2
        var name5 = document.getElementById("name6").value;
        var gain5 = document.getElementById("gain6").value;
        var A05 = document.getElementById("A06").value;
        var A15 = document.getElementById("A16").value;
        var enable5 = document.getElementById("checkbox6").value;

        // VALEURS DU CAPTEUR 3
        var name6 = document.getElementById("name7").value;
        var gain6 = document.getElementById("gain7").value;
        var A06 = document.getElementById("A07").value;
        var A16 = document.getElementById("A17").value;
        var enable6 = document.getElementById("checkbox7").value;

        // VALEURS DU CAPTEUR 4
        var name7 = document.getElementById("name8").value;
        var gain7 = document.getElementById("gain8").value;
        var A07 = document.getElementById("A08").value;
        var A17 = document.getElementById("A18").value;
        var enable7 = document.getElementById("checkbox8").value;

        // CREATION DU NOUVEAU JSON AVEC LES NOUVELLES VALEURS
        //newJson.GPS = document.getElementById("gps").value;

        // PARTIE ADS1 DU JSON
        newJson = {
            ...newJson,

            "GETS": {

                "ID": "GETS",

                "ADS1": {
                    "cpt1": {
                        "nom": name,
                        "G": gain,
                        "A0": A0,
                        "A1": A1,
                        "enable": enable
                    },

                    "cpt2": {
                        "nom": name1,
                        "G": gain1,
                        "A0": A01,
                        "A1": A11,
                        "enable": enable1
                    },

                    "cpt3": {
                        "nom": name2,
                        "G": gain2,
                        "A0": A02,
                        "A1": A12,
                        "enable": enable2
                    },

                    "cpt4": {
                        "nom": name3,
                        "G": gain3,
                        "A0": A03,
                        "A1": A13,
                        "enable": enable3
                    }
                }

            }
        };

        // PARTIE ADS2 DU JSON
        newJson1 = {
            ...newJson1,

            "GETS": {

                "ID": "GETS",

                "ADS2": {

                    "cpt1": {
                        "nom": name4,
                        "G": gain4,
                        "A0": A04,
                        "A1": A14,
                        "enable": enable4
                    },

                    "cpt2": {
                        "nom": name5,
                        "G": gain5,
                        "A0": A05,
                        "A1": A15,
                        "enable": enable5
                    },

                    "cpt3": {
                        "nom": name6,
                        "G": gain6,
                        "A0": A06,
                        "A1": A16,
                        "enable": enable6
                    },

                    "cpt4": {
                        "nom": name7,
                        "G": gain7,
                        "A0": A07,
                        "A1": A17,
                        "enable": enable7
                    }
                }

            }
        };

        /*newJson.CO2_A = document.getElementById("CO2_1").value;
        newJson.CO2_B = document.getElementById("CO2_2").value;
        newJson.Energie = document.getElementById("MesureUI").value;
        newJson.Qair_1 = document.getElementById("Qair_1").value;
        newJson.Qair_2 = document.getElementById("Qair_2").value;
        newJson.Temp = document.getElementById("Temp").value;
        newJson.Baro = document.getElementById("Baro").value;
        newJson.C_Particules_1 = document.getElementById("C_Particules_1").value;
        newJson.C_Particules_2 = document.getElementById("C_Particules_1").value;*/
        var stringJSON = JSON.stringify(newJson);
        var stringJSON1 = JSON.stringify(newJson1);


        $(function sendJSON() {

            // ENVOI DU PREMIER JSON
            sendMessage({
                'data': stringJSON
            });

            // ENVOI DU SECOND JSON
            sendMessage({
                'data': stringJSON1
            });

            alert("Configuration envoyée avec succès !")
            return true;

        });
    });

    /************************************ FONCTION CHANGER LE STATUT DES CHECKBOX ************************************/
    $(function checkbox() {

        $("#checkbox1").click(function() {
            if ($(this).is(":checked")) {

                $('#checkbox1').val('True');

            } else {

                $('#checkbox1').val('False');
            }
        });

        $("#checkbox2").click(function() {
            if ($(this).is(":checked")) {

                $('#checkbox2').val('True');

            } else {

                $('#checkbox2').val('False');
            }
        });

        $("#checkbox3").click(function() {

            if ($(this).is(":checked")) {

                $('#checkbox3').val('True');

            } else {

                $('#checkbox3').val('False');
            }
        });

        $("#checkbox4").click(function() {

            if ($(this).is(":checked")) {

                $('#checkbox4').val('True');

            } else {
                $('#checkbox4').val('False');
            }
        });

        $("#checkbox5").click(function() {

            if ($(this).is(":checked")) {

                $('#checkbox5').val('True');

            } else {

                $('#checkbox5').val('False');
            }
        });

        $("#checkbox6").click(function() {

            if ($(this).is(":checked")) {

                $('#checkbox6').val('True');

            } else {

                $('#checkbox6').val('False');
            }
        });

        $("#checkbox7").click(function() {
            if ($(this).is(":checked")) {

                $('#checkbox7').val('True');

            } else {

                $('#checkbox7').val('False');
            }
        });

        $("#checkbox8").click(function() {
            if ($(this).is(":checked")) {

                $('#checkbox8').val('True');

            } else {

                $('#checkbox8').val('False');
            }
        });

        // ACTIVER TOUTES LES CHECKBOXES DU BLOC CONVERTISSEUR 1
        $("#all1").click(function() {

            if ($(this).is(":checked")) {

                $('.checkbox').prop('checked', true);
                $('.checkbox').val('True');

            } else {

                $('.checkbox').prop('checked', false);
                $('.checkbox').val('False');

            }
        });

        // ACTIVER TOUTES LES CHECKBOX DU CONVERTISSEUR 2
        $("#all2").click(function() {

            if ($(this).is(":checked")) {

                $('.checkboxes').prop('checked', true);
                $('.checkboxes').val('True');

            } else {
                $('.checkboxes').prop('checked', false);
                $('.checkboxes').val('False');

            }
        });

        // GPS
        $('.gps').change(function() {
            if (this.checked)
                $('.gps').val('True');
            else
                $('.gps').val('False');
        });

        // CO2_1
        $('.co2a').change(function() {
            if (this.checked)
                $('.co2a').val('True');
            else
                $('.co2a').val('False');
        });

        // CO2_2
        $('.co2b').change(function() {
            if (this.checked)
                $('.co2b').val('True');
            else
                $('.co2b').val('False');
        });

        // ENERGIE
        $('.energie').change(function() {
            if (this.checked)
                $('.energie').val('True');
            else
                $('.energie').val('False');
        });

        // QUALITE DE L'AIR 1
        $('.air1').change(function() {
            if (this.checked)
                $('.air1').val('True');
            else
                $('.air1').val('False');
        });

        // QUALITE DE L'AIR 2
        $('.air2').change(function() {
            if (this.checked)
                $('.air2').val('True');
            else
                $('.air2').val('False');
        });

        // TEMPERATURE
        $('.temp').change(function() {
            if (this.checked)
                $('.temp').val('True');
            else
                $('.temp').val('False');
        });

        // BAROMETRE
        $('.baro').change(function() {
            if (this.checked)
                $('.baro').val('True');
            else
                $('.baro').val('False');
        });

        // PARTICULES 1
        $('.particules1').change(function() {
            if (this.checked)
                $('.particules1').val('True');
            else
                $('.particules1').val('False');
        });

        // PARTICULES 2
        $('.particules2').change(function() {
            if (this.checked)
                $('.particules2').val('True');
            else
                $('.particules2').val('False');
        });
    });

    /************************************ FONCTION POUR PLACER LE JSON DANS LES FORMULAIRES ************************************/
    function afficherJSON() {

        /********** CONVERTISSEUR 1 **********/

        // CAPTEUR 1
        var gain = json.GETS.ADS1.cpt1.G;
        var A0 = json.GETS.ADS1.cpt1.A0;
        var A1 = json.GETS.ADS1.cpt1.A1;
        var enable = json.GETS.ADS1.cpt1.enable;

        $("#gain1").val(gain);
        $("#A01").val(A0);
        $("#A11").val(A1);
        $("#checkbox1").val(enable);

        // CAPTEUR 2
        var gain1 = json.GETS.ADS1.cpt2.G;
        var A01 = json.GETS.ADS1.cpt2.A0;
        var A11 = json.GETS.ADS1.cpt2.A1;
        var enable1 = json.GETS.ADS1.cpt2.enable;

        $("#gain2").val(gain1);
        $("#A02").val(A01);
        $("#A12").val(A11);
        $("#checkbox2").val(enable1);

        // CAPTEUR 3
        var gain2 = json.GETS.ADS1.cpt3.G;
        var A02 = json.GETS.ADS1.cpt3.A0;
        var A12 = json.GETS.ADS1.cpt3.A1;
        var enable2 = json.GETS.ADS1.cpt3.enable;

        $("#gain3").val(gain2);
        $("#A03").val(A02);
        $("#A13").val(A12);
        $("#checkbox3").val(enable2);

        // CAPTEUR 4
        var gain3 = json.GETS.ADS1.cpt4.G;
        var A03 = json.GETS.ADS1.cpt4.A0;
        var A13 = json.GETS.ADS1.cpt4.A1;
        var enable3 = json.GETS.ADS1.cpt4.enable;

        $("#gain4").val(gain3);
        $("#A04").val(A03);
        $("#A14").val(A13);
        $("#checkbox4").val(enable3);

        /********** CONVERTISSEUR 2 **********/

        // CAPTEUR 1
        var gain4 = json.GETS.ADS2.cpt1.G;
        var A04 = json.GETS.ADS2.cpt1.A0;
        var A14 = json.GETS.ADS2.cpt1.A1;
        var enable4 = json.GETS.ADS2.cpt1.enable;

        $("#gain5").val(gain4);
        $("#A05").val(A04);
        $("#A15").val(A14);
        $("#checkbox5").val(enable4);

        // CAPTEUR 2
        var gain5 = json.GETS.ADS2.cpt2.G;
        var A05 = json.GETS.ADS2.cpt2.A0;
        var A15 = json.GETS.ADS2.cpt2.A1;
        var enable5 = json.GETS.ADS2.cpt2.enable;

        $("#gain6").val(gain5);
        $("#A06").val(A05);
        $("#A16").val(A15);
        $("#checkbox6").val(enable5);

        // CAPTEUR 3
        var gain6 = json.GETS.ADS2.cpt3.G;
        var A06 = json.GETS.ADS2.cpt3.A0;
        var A16 = json.GETS.ADS2.cpt3.A1;
        var enable6 = json.GETS.ADS2.cpt3.enable;

        $("#gain7").val(gain6);
        $("#A07").val(A06);
        $("#A17").val(A16);
        $("#checkbox7").val(enable6);

        // CAPTEUR 4
        var gain7 = json.GETS.ADS2.cpt4.G;
        var A07 = json.GETS.ADS2.cpt4.A0;
        var A17 = json.GETS.ADS2.cpt4.A1;
        var enable7 = json.GETS.ADS2.cpt4.enable;

        $("#gain8").val(gain7);
        $("#A08").val(A07);
        $("#A18").val(A17);
        $("#checkbox8").val(enable7);

        // BLOC CAPTEURS DIVERS
        for (var key in json) {

            if (json[key] === "True") {
                document.getElementById(key).checked = true;

                document.getElementById("checkbox1").checked = true;
                document.getElementById("checkbox2").checked = true;
                document.getElementById("checkbox3").checked = true;
                document.getElementById("checkbox4").checked = true;
                document.getElementById("checkbox5").checked = true;
                document.getElementById("checkbox6").checked = true;
                document.getElementById("checkbox7").checked = true;
                document.getElementById("checkbox8").checked = true;

                $('.checkbox').val('True');
                $('.checkboxes').val('True');
                $('.gps').val('True');
            }
            if (json[key] === "true") {
                document.getElementById(key).checked = true;
                $('.co2a').val('True');
                $('.co2b').val('True');
                $('.energie').val('True');
                $('.air1').val('True');
                $('.air2').val('True');
                $('.temp').val('True');
                $('.baro').val('True');
                $('.particules1').val('True');
                $('.particules2').val('True');
            }
        }
    };
});