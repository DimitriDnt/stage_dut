$(document).ready(function() {

    // Initialisation des variables
    var socket = new WebSocket("ws://192.168.1.90:81/ws");
    //var socket = new WebSocket("ws://192.168.43.12:81/ws");
    var jsonV;
    var temperature;
    var hydrometrie;

    // CONNEXION AU SERVEUR WEBSOCKET
    socket.onopen = function() {
        console.log("Connexion !");
    };

    socket.onmessage = function(message) {
        console.log("Receiving : " + message.data);
        var series = Highcharts.charts[0].series.slice();
        var data = message.data;
        console.log("Donnees : " + data);
        var jsonV = JSON.parse(data);

        // On récupère les valeurs du capteur de Température numéro 2
        var temp2 = parseFloat(jsonV.DHT11_2.Temperature);

        // On récupère les valeurs du capteur d'Hydrométrie numéro 2
        var hydro2 = parseFloat(jsonV.DHT11_2.Hydrometrie);
        temperature = temp2;
        hydrometrie = hydro2;

        var y1 = parseFloat(jsonV.DHT11_1.Temperature);
        var y2 = parseFloat(jsonV.DHT11_1.Hydrometrie);

        temperature1 = y1;
        hydrometrie1 = y2;

        var y3 = parseFloat(jsonV.DHT11_3.Temperature);
        var y4 = parseFloat(jsonV.DHT11_3.Hydrometrie);

        temperature1 = y3;
        hydrometrie1 = y4;
    }

    socket.onclose = function() {
        console.log("disconnected");
    };

    // INITIALISATION DES PARAMETRES DES JAUGES 
    var gaugeOptions = {

        chart: {
            type: 'solidgauge'
        },

        title: null,

        credits: {
            enabled: false
        },

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // AXE DES VALEURS
        yAxis: {
            stops: [
                [0.2, '#55BF3B'], // Vert
                [0.5, '#DDDF0D'], // Jaune
                [0.9, '#DF5353'] // Rouge
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    // PREMIERE JAUGE : TEMPERATURE
    var chart1 = Highcharts.chart('container1', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 40,
            title: {
                text: '<b>Température 2</b>'
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Température',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                    '<span style="font-size:12px;color:black">°C</span></div>'
            },
            tooltip: {
                valueSuffix: ' °C'
            }
        }]
    }));

    // DEUXIEME JAUGE : HYDROMETRIE
    var chart2 = Highcharts.chart('container2', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: '<b>Hygrométrie 2</b>'
            }
        },

        series: [{
            name: 'Hydrométrie',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                    '<span style="font-size:12px;color:black">%</span></div>'
            },
            tooltip: {
                valueSuffix: 'Pourcents'
            }
        }]
    }));

    // FONCTION POUR AJOUTER LES MESURES AUX JAUGES
    setInterval(function() {
        // Temperature
        var point,
            newVal;

        if (chart1) {
            point = chart1.series[0].points[0];
            newVal = temperature;
            point.update(newVal);
        }

        // Hydrometrie
        if (chart2) {
            point = chart2.series[0].points[0];
            newVal = hydrometrie;
            point.update(newVal);
        }
    }, 1000);

    // JAUGE 3 : TEMPERATURE
    Highcharts.chart('container', {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '<b>Température 1</b>'
            },
            credits: {
                enabled: false
            },
            pane: {
                startAngle: -100,
                endAngle: 100,
                background: [{
                    backgroundColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 50,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: '°C'
                },
                plotBands: [{
                    from: 0,
                    to: 20,
                    color: '#55BF3B' // green
                }, {
                    from: 20,
                    to: 35,
                    color: '#DDDF0D' // yellow
                }, {
                    from: 35,
                    to: 50,
                    color: '#DF5353' // red
                }]
            },

            series: [{
                name: 'Temperature',
                data: [0],
                tooltip: {
                    valueSuffix: ' °C'
                }
            }]

        },
        // Ajout du JSON sur les jauges
        function(chart) {
            setInterval(function() {
                var point = chart.series[0].points[0],
                    newVal = temperature1;
                point.update(newVal);

            }, 3000);
        });

    // JAUGE 4 : HYGROMETRIE
    Highcharts.chart('hygrometrie', {

            chart: {
                type: 'gauge',
                plotBackgroundColor: null,
                plotBackgroundImage: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '<b>Hygrométrie 1</b>'
            },
            credits: {
                enabled: false
            },
            pane: {
                startAngle: -100,
                endAngle: 100,
                background: [{
                    backgroundColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#FFF'],
                            [1, '#333']
                        ]
                    },
                    borderWidth: 0,
                    outerRadius: '109%'
                }, {
                    backgroundColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, '#333'],
                            [1, '#FFF']
                        ]
                    },
                    borderWidth: 1,
                    outerRadius: '107%'
                }, {
                    // default background
                }, {
                    backgroundColor: '#DDD',
                    borderWidth: 0,
                    outerRadius: '105%',
                    innerRadius: '103%'
                }]
            },

            // Axe des valeurs
            yAxis: {
                min: 0,
                max: 100,

                minorTickInterval: 'auto',
                minorTickWidth: 1,
                minorTickLength: 10,
                minorTickPosition: 'inside',
                minorTickColor: '#666',

                tickPixelInterval: 30,
                tickWidth: 2,
                tickPosition: 'inside',
                tickLength: 10,
                tickColor: '#666',
                labels: {
                    step: 2,
                    rotation: 'auto'
                },
                title: {
                    text: '%'
                },
                plotBands: [{
                    from: 0,
                    to: 20,
                    color: '#55BF3B' // Vert
                }, {
                    from: 20,
                    to: 60,
                    color: '#DDDF0D' // Jaune
                }, {
                    from: 60,
                    to: 100,
                    color: '#DF5353' // Rouge
                }]
            },

            series: [{
                name: 'Hygrometrie',
                data: [0],
                tooltip: {
                    valueSuffix: ' %'
                }
            }]

        },
        // Ajout du JSON sur les jauges
        function(chart) {
            setInterval(function() {
                var point = chart.series[0].points[0],
                    newVal = hydrometrie1;
                point.update(newVal);

            }, 3000);
        });

});