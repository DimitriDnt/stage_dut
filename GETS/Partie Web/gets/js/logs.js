$(document).ready(function() {

    // Initialisation des variables
    var message;
    var tableau = [];
    var test;
    var socket;
    var nTab = document.getElementById('tab1');
    console.log("Value = " + nTab.value);
    var received = $('#received');
    var socket = new WebSocket("ws://192.168.1.90:81/ws");
    //var socket = new WebSocket("ws://192.168.43.12:81/ws");
    var data;
    var id = 0;

    /************* CONNEXION AU SERVEUR WEB SOCKET ************/

    socket.onopen = function() {
        console.log("Connexion !");
    };

    socket.onmessage = function(message) {
        console.log("Receiving : " + message.data);
        received.append(id + " : " + "{\"Date\":\"" + new Date().toLocaleDateString() + "\",\"Heure\":\"" + new Date().toLocaleTimeString() + "\"," + message.data.slice(1));
        received.append($('<br/>'));
        id++;
        // On stocke le message re�u dans un tableau
        test = "{\"Date\":\"" + new Date().toLocaleDateString() + "\",\"Heure\":\"" + new Date().toLocaleTimeString() + "\"," + message.data.slice(1);
        tableau.push(test);
    };

    socket.onclose = function() {
        console.log("Deconnexion");
    };

    var sendMessage = function(message) {
        console.log("Sending : " + message.data);
        socket.send(message.data);
    };

    // Bouton pour envoyer des donn�es au port s�rie qui transmet les donn�es
    $("#cmd_send").click(function(ev) {
        ev.preventDefault();
        var cmd = $('#cmd_value').val();
        sendMessage({
            'data': cmd
        });
        $('#cmd_value').val("");
    });

    // Bouton pour effacer les trames re�ues
    $('#clear').click(function() {
        received.empty();
        tableau = [];
        id = 0;
    });

    // Clic sur le bouton "ARRETER" pour fermer la connexion au WebSocket
    $('#stop').click(function() {

        if (socket.readyState == WebSocket.OPEN) {
            socket.close();
        }
    });

    // Clic sur le bouton "REPRENDRE" pour se reconnecter au WebSocket
    $('#start').click(function() {
        var socket = new WebSocket("ws://192.168.1.90:81/ws");
        //var socket = new WebSocket("ws://192.168.1.88:8080/ws");
        socket.onopen = function() {
            console.log("Connexion !");
        };

        socket.onmessage = function(message) {
            console.log("Receiving : " + message.data);
            received.append(id + " : " + "{\"Date\":\"" + new Date().toLocaleDateString() + "\",\"Heure\":\"" + new Date().toLocaleTimeString() + "\"," + message.data.slice(1));
            //received.append("{\"ID\":\"" + (id++) + "\",\"Date\":\""+ new Date().toLocaleDateString() + "\",\"Heure\":\""+ new Date().toLocaleTimeString()+ "\"," + message.data.slice(1));
            received.append($('<br/>'));
            id++;
            // On stocke le message re�u dans un tableau
            test = "{\"Date\":\"" + new Date().toLocaleDateString() + "\",\"Heure\":\"" + new Date().toLocaleTimeString() + "\"," + message.data.slice(1);
            tableau.push(test);
        };

        socket.onclose = function() {
            console.log("Deconnexion");
        };

        var sendMessage = function(message) {
            console.log("sending :" + message.data);
            socket.send(message.data);
        };

        $("#cmd_send").click(function(ev) {
            ev.preventDefault();
            var cmd = $('#cmd_value').val();
            sendMessage({
                'data': cmd
            });
            $('#cmd_value').val("");
        });

        // Bouton pour arr�ter le d�filement des trames et fermer la connexion au websocket
        $('#stop').click(function() {

            if (socket.readyState == WebSocket.OPEN) {
                socket.close();
            }
        });
    });

    // Bouton pour cr�er l'arborescence � partir de la trame JSON choisie
    $('#update').click(function() {
        console.log("nTab =" + nTab.value);
        var message = tableau[nTab.value];
        console.log("Message = " + message);
        data = JSON.parse(message);
        $("#tree").treeview({
            color: "#428bca",
            expandIcon: "glyphicon glyphicon-plus-sign",
            collapseIcon: "glyphicon glyphicon-minus-sign",
            //borderColor: "#428bca",
            showTags: true,
            data: format_for_treeview(data, [])
        });
    });

    // Fonction pour mettre le JSON au format Bootstrap Treeview pour afficher l'arborescence
    function format_for_treeview(data, arr) {
        for (var key in data) {
            if (Array.isArray(data[key]) || data[key].toString() === "[object Object]") {
                // Quand les donn�es sont un tableau ou un objet Javascript
                var nodes = [];
                var completedNodes = format_for_treeview(data[key], nodes);
                arr.push({
                    text: key,
                    nodes: completedNodes
                })
            } else {
                // Quand les donn�es sont au format String
                arr.push({
                    text: key + " : " + data[key]
                })
            }
        }
        return arr;
    }
});