$(document).ready(function() {

    // Initialisation des variables
    json = 0;
    line = 0;
    lat = 0;
    lon = 0;
    alt = 0;
    map = null;
    marker = null;

    var socket = new WebSocket("ws://192.168.1.90:81/ws");
    //var socket = new WebSocket("ws://192.168.43.12:81/ws");


    /************************* CONNEXION AU SERVEUR WEBSOCKET ***********************/
    socket.onopen = function() {
        console.log("Connexion !");
    };

    socket.onmessage = function(message) {
        //console.log("Receiving : " + message.data);
        var data = message.data;
        json = JSON.parse(data);

        // Latitude
        lat = json.GPS.y;
        console.log("LATITUDE = " + lat);

        // Longitude
        lon = json.GPS.x;
        console.log("LONGITUDE = " + lon);

        alt = json.GPS.alt;
        console.log("ALTITUDE = " + alt);

    };

    socket.onclose = function() {
        console.log("disconnected");
    };

    // On definit le centre de la carte et le niveau de zoom initial

    var map = L.map('map', {
        scrollWheelZoom: true
    }).setView([-21.20893, 55.5719235], 12);

    /***************************** FOND DE CARTE EN LIGNE *****************************/

    /*L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);*/

    /*************************** FOND DE CARTE HORS CONNEXION *************************/
    var layer = L.tileLayer('../images/test/{z}/{x}/{y}.png', {
        minZoom: 15,
        maxZoom: 17
    }).addTo(map);

    // On trace une ligne pour pouvoir visualiser le trajet effectué par le marqueur
    var line = L.polyline([], {
        color: 'blue',
        weight: '6',
        opacity: 1
    }).addTo(map);

    var cercle = L.circle([lat, lon], 100, {
        opacity: 0.9,
        fillColor: 'transparent',
    }).addTo(map);

    var layerGroup = L.layerGroup().addTo(map);

    // Fonction pour repositionner le marqueur à chaque changement de Latitude / Longitude
    function actualiserPosition() {

        layerGroup.clearLayers();
        for (var i = 0; i < 10; i += 1) {
            marker = L.marker([lat, lon]).addTo(layerGroup).bindPopup("<center><b>DRONE</b></center><br/><b>Lat : </b>" + lat + "<br/><b>Lon : </b>" + lon + "<br/><b> Alt : </b>" + alt + " m");
            line.addLatLng([lat, lon]);
            cercle.setLatLng([lat, lon]);

            // Si l'altitude est en dessous de 1550
            if (alt < 1550) {

                // La couleur du cercle est vert
                cercle.setStyle({
                    color: 'green',
                    fillColor: 'green',
                    fillOpacity: .3
                })

                // Si l'altitude est au dessus de 1550
            } else {

                // La couleur du cercle est rouge
                cercle.setStyle({
                    color: 'red',
                    fillColor: 'red',
                    fillOpacity: .3
                });
            }
        }
    }

    $('#depart').click(function() {
        setInterval(actualiserPosition, 2000);
    });

});