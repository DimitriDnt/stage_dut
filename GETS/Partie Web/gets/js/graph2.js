$(document).ready(function() {

    // Initialisation des variables
    //var series = new Array(6);
    var hygro1 = new Array(2);
    var hygro2 = new Array(2);
    var hygro3 = new Array(2);
    var socket = new WebSocket("ws://192.168.1.90:81/ws");
    jsonV = null;
    //var socket = new WebSocket("ws://192.168.43.12:81/ws");


    // CONNEXION AU SERVEUR WEB SOCKET
    socket.onopen = function() {
        console.log("Connexion !");
    };

    socket.onmessage = function(message) {
        //console.log("Receiving : " + message.data);

        var hygro1 = Highcharts.charts[0].series.slice();
        var hygro2 = Highcharts.charts[1].series.slice();
        var hygro3 = Highcharts.charts[2].series.slice();
        var data = message.data;
        //console.log("Donnees : " + data);
        var jsonV = JSON.parse(data);

        var x = (new Date()).getTime();

        // VALEURS DES CAPTEURS DE hydroERATURE
        var h1 = parseFloat(jsonV.DHT11_1.Hydrometrie);
        console.log(h1);
        var h2 = parseFloat(jsonV.DHT11_2.Hydrometrie);
        console.log(h2);
        var h3 = parseFloat(jsonV.DHT11_3.Hydrometrie);
        console.log(h3);

        // AJOUT DES VALEURS SUR LES GRAPHIQUES
        hygro1[0].addPoint([x, h1], true, true);
        hygro2[0].addPoint([x, h2], true, true);
        hygro3[0].addPoint([x, h3], true, true);

    }

    socket.onclose = function() {
        console.log("disconnected");
    };

    /*************************************************** GRAPHIQUE HYGROMETRIE 1 ***************************************************/

    window.chart1 = Highcharts.chart('hygrometrie1', {
        chart: {
            type: 'spline',
            animation: Highcharts.svg,
            borderColor: "black",
            borderWidth: 2,
            marginRight: 10,

            events: {

            }
        },
        time: {
            useUTC: false
        },

        credits: {
            enabled: false
        },

        title: {
            text: '<b>Hygrométrie</b><br/><p style="color: blue;"> (Capteur 1)</p>'
        },

        plotOptions: {

            series: {
                color: 'blue'
            }

        },

        // AXE DU hydroS
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },

        // AXE DES VALEURS
        yAxis: {
            title: {
                text: 'Pourcents'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Cpt1 Hygrométrie',
            data: (function() {

                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -60; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: 0
                    });
                }
                return data;
            }())
        }]
    });

    /*************************************************** GRAPHIQUE HYGROMETRIE 2 ***************************************************/

    window.chart2 = Highcharts.chart('hygrometrie2', {
        chart: {
            type: 'spline',
            animation: Highcharts.svg,
            borderColor: "black",
            borderWidth: 2,
            marginRight: 10,

            events: {

            }
        },
        time: {
            useUTC: false
        },

        credits: {
            enabled: false
        },

        title: {
            text: '<b>Hygrométrie</b><br/><p style="color: green;"> (Capteur 2)</p>'
        },

        plotOptions: {

            series: {
                color: 'green'
            }

        },

        // AXE DU hydroS
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },

        // AXE DES VALEURS
        yAxis: {
            title: {
                text: 'Pourcents'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Cpt2 Hygrométrie',
            data: (function() {

                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -60; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: 0
                    });
                }
                return data;
            }())
        }]
    });


    /*************************************************** GRAPHIQUE HYGROMETRIE 3 ***************************************************/

    window.chart3 = Highcharts.chart('hygrometrie3', {
        chart: {
            type: 'spline',
            animation: Highcharts.svg,
            borderColor: "black",
            borderWidth: 2,
            marginRight: 10,

            events: {

            }
        },
        time: {
            useUTC: false
        },

        credits: {
            enabled: false
        },

        title: {
            text: '<b>Hygrométrie</b><br/><p style="color: red;"> (Capteur 3)</p>'
        },

        plotOptions: {

            series: {
                color: 'red'
            }

        },

        // AXE DU hydroS
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },

        // AXE DES VALEURS
        yAxis: {
            title: {
                text: 'Pourcents'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: 'red'
            }]
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Cpt3 Hygrométrie',
            data: (function() {

                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -60; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: 0
                    });
                }
                return data;
            }())
        }]
    });
});