$(document).ready(function() {

    // Initialisation des variables
    json = null;
    data = null;
    delai = 300;
    //var socket = new WebSocket("ws://192.168.1.99:81/ws");
    var socket = new WebSocket("ws://192.168.1.25:81/ws");

    test = JSON.stringify({
        "GETS": {
            "ID": "GETS",
            "ordre": "getcfg"
        }
    });

    /************* CONNEXION AU SERVEUR WEB SOCKET ************/

    socket.onopen = function() {
        console.log("Connexion !");
    };

    socket.onmessage = function(message) {
        console.log(message.data);
        data = message.data;
    };

    socket.onclose = function() {
        console.log("Deconnexion");
    };

    var sendMessage = function(message) {
        console.log("Envoi : " + message.data);
        socket.send(message.data);
    };

    // MENU DEROULANT POUR SELECTIONNER LA STATION GETS
    $("#input").on('input', function() {

        // FONCTION POUR AFFICHER LE POPUP BASE DE DONNEES OU WEBSOCKET
        function functionConfirm(msg, bdd, ws) {
            var confirmBox = $("#confirm");
            confirmBox.find(".message").text(msg);
            confirmBox.find(".bdd,.ws").unbind().click(function() {
                confirmBox.hide();
            });
            confirmBox.find(".bdd").click(bdd);
            confirmBox.find(".ws").click(ws);
            confirmBox.show();
        }

        functionConfirm("Choisissez où récupérer la configuration.",

            // SI ON CHOISIT LA LECTURE BASE DE DONNEES
            function bdd() {
                alert("Lecture de la base de données effectuée !");
                window.location.replace("../php/conf_reseau_bdd.php")
            },

            // SI ON CHOISIT LA LECTURE WEBSOCKET
            function ws() {

                var station = $("#input").val();

                switch (station) {

                    case "GETS 1":
                        sendMessage({
                            'data': test
                        });
                        break;

                    case "GETS 2":
                        sendMessage({
                            'data': test
                        });
                        break;

                    case "GETS 3":
                        sendMessage({
                            'data': test
                        });
                        break;

                    case "GETS 4":
                        sendMessage({
                            'data': test
                        });
                        break;

                    case "GETS 5":
                        sendMessage({
                            'data': "GETS5"
                        });
                        break;

                    case "GETS 6":
                        sendMessage({
                            'data': "GETS6"
                        });
                        break;

                    case "GETS 7":
                        sendMessage({
                            'data': "GETS7"
                        });
                        break;

                    case "GETS 8":
                        sendMessage({
                            'data': "GETS8"
                        });
                        break;

                    case "GETS 9":
                        sendMessage({
                            'data': "GETS9"
                        });
                        break;

                    case "GETS 10":
                        sendMessage({
                            'data': "GETS10"
                        });
                        break;
                }

                setTimeout(function() {

                    if (data != null) {

                        json = JSON.parse(data);
                        afficherJSON();
                        alert("Configuration chargée avec succès !");
                        data = null;
                    } else {

                        alert("Erreur de communication avec la station choisie !");
                    }

                }, delai);
            });

    });


    // BOUTON POUR ENVOYER LE NOUVEAU JSON AVEC LES MODIFICATIONS
    $("#save").click(function(ev) {

        // RECUPERATION DES VALEURS DANS LE JSON
        var ssid = document.getElementById("ssid").value;
        var pwd = document.getElementById("pwd").value;
        var adresse = document.getElementById("adresse").value;
        console.log(CheckIP(adresse));
        var masque = document.getElementById("masque").value;
        console.log(CheckMask(masque));
        var passerelle = document.getElementById("passerelle").value;
        console.log(CheckGateway(passerelle));
        var dns = document.getElementById("dns").value;
        console.log(CheckIP(dns));

        var wifi = {};
        var lora = {};
        var ip = {};

        // PARTIE WIFI DU JSON
        wifi = {

            "GETS": {

                "ID": "GETS",

                "wifi": {
                    "ssid": ssid,
                    "password": pwd,
                    "portWS": 0,
                    "portTelnet": 0
                }

            }
        };

        // PARTIE LORA DU JSON
        lora = {

            "GETS": {

                "ID": "GETS",

                "lora": {
                    "band": 0,
                    "localAddress": 0,
                    "interval": 0
                }
            }
        };

        // PARTIE IP DU JSON
        ip = {

            "GETS": {

                "ID": "GETS",

                "ip": {
                    "Mode": false,
                    "ip": adresse,
                    "nm": masque,
                    "gw": passerelle,
                    "dns": dns,
                    "ntp": "114.114.114.114"
                }
            }
        };

        // CREATION DU NOUVEAU JSON AVEC LES NOUVELLES VALEURS
        /*newJson.SSID = document.getElementById("ssid").value;
        newJson.PWD = document.getElementById("pwd").value;
        newJson.ADRESSE = document.getElementById("adresse").value;
        newJson.MASQUE = document.getElementById("masque").value;
        newJson.PASSERELLE = document.getElementById("passerelle").value;
        newJson.DNS = document.getElementById("dns").value;
        var stringJSON = JSON.stringify(newJson);*/

        $(function validation() {

            if (CheckIP(adresse) != true) {

                alert("Adresse IPv4 incorrecte !");
                return false;
            }

            if (CheckMask(masque) != true) {

                alert("Masque de sous-réseau incorrect !");
                return false;

            }

            if (CheckGateway(passerelle) != true) {

                alert("Passerelle par défaut incorrecte !");
                return false;

            }

            if (CheckIP(dns) != true) {

                alert("Serveur DNS incorrect !");
                return false;

            } else {

                stringJSON = JSON.stringify(wifi);
                stringJSON1 = JSON.stringify(lora);
                stringJSON2 = JSON.stringify(ip);

                // ENVOI DE LA CONF WIFI
                sendMessage({
                    'data': stringJSON
                });

                // ENVOI DE LA CONF LORA
                sendMessage({
                    'data': stringJSON1
                });

                // ENVOI DE LA CONF IP
                sendMessage({
                    'data': stringJSON2
                });

                alert("Configuration envoyée avec succès !")
                return true;
            }
        });

    });


    /************************************ FONCTION POUR PLACER LES DONNEES DU JSON ************************************/
    function afficherJSON() {

        // CONFIGURATION LORA
        var band = json.GETS.lora.band;
        var localAddress = json.GETS.lora.localAddress;
        var interval = json.GETS.lora.interval;

        $("#band").val(band);
        $("#local").val(localAddress);
        $("#interval").val(interval);

        // CONFIGURATION WIFI
        var ssid = json.GETS.wifi.ssid;
        var pwd = json.GETS.wifi.password;
        var portWS = json.GETS.wifi.portWS;
        var portTelnet = json.GETS.wifi.portTelnet;

        $("#ssid").val(ssid);
        $("#pwd").val(pwd);
        $("#ws").val(portWS);
        $("#telnet").val(portTelnet);

        // CONFIGURATION IP
        var mode = json.GETS.ip.Mode;
        var ip = json.GETS.ip.ip;
        var mask = json.GETS.ip.nm;
        var gw = json.GETS.ip.gw;
        var dns = json.GETS.ip.dns;
        var ntp = json.GETS.ip.ntp;

        $("#mode").val(mode);
        $("#ip").val(ip);
        $("#mask").val(mask);
        $("#gw").val(gw);
        $("#dns").val(dns);
        $("#ntp").val(dns);

    };

    /************************************ FONCTION POUR VERIFIER L'ADRESSE IP ************************************/

    ////////////////////////////////////////////////////////////////////
    // Adresses IPv4 invalides
    //                                                              
    //case 1:       127.(0-255).(0-255).(0-255)            //class A
    //case 2:       (224-255).(0-255).(0-255).(0-255)      //class D
    //case 3:       128.0.(0-255).(0-255)                  //class B
    //case 4:       191.255.(0-255).(0-255)                //class C
    //case 5:       (64-127).255.255.255                   //class A
    //case 6:       (128-191).(0-255).255.255              //class B
    //case 7:       (192-255).(0-255).(0-255).255          //class C
    //case 8:       (64-127).0.0.0                         //class A
    //case 9:       (128-191).(0-255).0.0                  //class B
    //case 10:  (192-255).(0-255).(0-255).0                 //class C
    //2003.10.14
    //case 11:  (255-~).(255-~).(255-~).(255-~)
    //case 12:  0.0.0.0
    //case 13:  11.0.0.0
    //case 14:  11.127.255.255
    //case 15:  11.255.255.255
    //case 16:    0.(1-254).(1-254).(1-254)
    //case 17:   (1-254).(1-254).(1-254).0
    //////////////////////////////////////////////////////////////////////
    function CheckIP(textValue) {
        re = /\W/;

        ipSplit = textValue.split(re);

        if (ipSplit.length != 4) return false;

        re = /[A-Za-z]/;

        for (var i = 0; i < ipSplit.length; i++) {
            if (textValue.search(re) != -1) {
                return false;
            }
        }

        for (i = 0; i < ipSplit.length; i++) {
            if (ipSplit[i] >= 255) return false; // case 11
        }

        if (ipSplit[0] == 0 && ipSplit[1] == 0 && ipSplit[2] == 0 && ipSplit[3] == 0) return false; // case 12
        if (ipSplit[0] == 11 && ipSplit[1] == 0 && ipSplit[2] == 0 && ipSplit[3] == 0) return false; // case 13
        if (ipSplit[0] == 11 && ipSplit[1] == 127 && ipSplit[2] == 255 && ipSplit[3] == 255) return false; // case 14
        if (ipSplit[0] == 11 && ipSplit[1] == 255 && ipSplit[2] == 255 && ipSplit[3] == 255) return false; // case 15

        if (ipSplit[0] == 127) return false; //case 1
        if (ipSplit[0] >= 224 && ipSplit[0] <= 255) return false; //case 2

        if (ipSplit[0] == 128 && ipSplit[1] == 0) return false; //case 3
        if (ipSplit[0] == 191 && ipSplit[1] == 255) return false; // case 4
        if (ipSplit[0] == 0 || ipSplit[3] == 0) return false; // case 16,17  papa add 2004.12.07

        if (ipSplit[3] == 255 || ipSplit[3] == 0) {
            if (ipSplit[0] >= 192 && ipSplit[0] <= 255) return false; //case 7, 10

            if (ipSplit[2] == 255 || ipSplit[2] == 0) {
                if (ipSplit[0] >= 128 && ipSplit[0] <= 191) return false; //case 6, 9

                if (ipSplit[1] == 255 || ipSplit[1] == 0)
                    if (ipSplit[0] >= 64 && ipSplit[0] <= 127) return false; //case 5, 8
            }
        }
        return true;
    }

    /************************************ FONCTION POUR VERIFIER LE MASQUE DE SOUS RESEAU ************************************/

    ////////////////////////////////////////////////////////////////////
    // Masques de sous-réseau invalides
    //
    //case 1:   (>255).(>255).(>255).(>255)
    //case 2:   255.255.255.255
    //case 3:   255.254.255.255
    //case 4:   255.255.255.130
    //case 5:   0.0.0.0
    //case 6:   10.10.10.10
    ////////////////////////////////////////////////////////////////////
    function CheckMask(textValue) {
        re = /\W/;

        ipSplit = textValue.split(re);

        if (ipSplit.length != 4) return false;

        re = /[A-Za-z]/;

        for (var i = 0; i < ipSplit.length; i++) {
            if (textValue.search(re) != -1) {
                return false;
            }
        }


        for (i = 0; i < ipSplit.length; i++) {
            if (ipSplit[i] > 255) return false; // case 1
        }
        if (ipSplit[0] == 255 && ipSplit[1] == 255 && ipSplit[2] == 255 && ipSplit[3] == 255) return false; // case 2
        if (ipSplit[0] == 255 && ipSplit[1] == 254 && ipSplit[2] == 255 && ipSplit[3] == 255) return false; // case 3
        if (ipSplit[0] == 255 && ipSplit[1] == 255 && ipSplit[2] == 255 && ipSplit[3] == 130) return false; // case 4
        if (ipSplit[0] == 0 && ipSplit[1] == 0 && ipSplit[2] == 0 && ipSplit[3] == 0) return false; // case 5
        if (ipSplit[0] == 10 && ipSplit[1] == 10 && ipSplit[2] == 10 && ipSplit[3] == 10) return false; // case 6

        return true;
    }

    /************************************ FONCTION POUR VERIFIER LA PASSERELLE PAR DEFAUT ************************************/

    ////////////////////////////////////////////////////////////////////
    // Passerelles par défaut invalides
    //
    //case 1:       (>255).(>255).(>255).(>255)
    //case 2:   255.255.255.255
    //case 3:   (224-239).(0-255).(0-255).(0-255)
    //case 4:   (240-254).(0-255).(0-255).(0-255)
    //case 5:   127.0.0.1
    //case 6:   0.0.0.0
    ////////////////////////////////////////////////////////////////////
    function CheckGateway(textValue) {
        re = /\W/;

        ipSplit = textValue.split(re);

        if (ipSplit.length != 4) return false;

        re = /[A-Za-z]/;

        for (var i = 0; i < ipSplit.length; i++) {
            if (textValue.search(re) != -1) {
                return false;
            }
        }

        for (i = 0; i < ipSplit.length; i++) {
            if (ipSplit[i] > 255) return false; // case 1
        }
        if (ipSplit[0] == 255 && ipSplit[1] == 255 && ipSplit[2] == 255 && ipSplit[3] == 255) return false; // case 2
        if (ipSplit[0] >= 224 && ipSplit[0] <= 239) return false; // case 3
        if (ipSplit[0] >= 240 && ipSplit[0] <= 254) return false; // case 4
        if (ipSplit[0] == 127 && ipSplit[1] == 0 && ipSplit[2] == 0 && ipSplit[3] == 1) return false; // case 5
        if (ipSplit[0] == 0 && ipSplit[1] == 0 && ipSplit[2] == 0 && ipSplit[3] == 0) return false; // case 6

        return true;
    }

});