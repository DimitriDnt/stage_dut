$(document).ready(function() {

    // Initialisation des variables
    //var series = new Array(6);
    var temp1 = new Array(2);
    var temp2 = new Array(2);
    var temp3 = new Array(2);
    var socket = new WebSocket("ws://192.168.1.90:81/ws");
    jsonV = null;
    //var socket = new WebSocket("ws://192.168.43.12:81/ws");


    // CONNEXION AU SERVEUR WEB SOCKET
    socket.onopen = function() {
        console.log("Connexion !");
    };

    socket.onmessage = function(message) {
        //console.log("Receiving : " + message.data);

        var temp1 = Highcharts.charts[0].series.slice();
        var temp2 = Highcharts.charts[1].series.slice();
        var temp3 = Highcharts.charts[2].series.slice();
        var data = message.data;
        //console.log("Donnees : " + data);
        var jsonV = JSON.parse(data);

        var x = (new Date()).getTime();

        // VALEURS DES CAPTEURS DE TEMPERATURE
        var t1 = parseFloat(jsonV.DHT11_1.Temperature);
        console.log(t1);
        var t2 = parseFloat(jsonV.DHT11_2.Temperature);
        console.log(t2);
        var t3 = parseFloat(jsonV.DHT11_3.Temperature);
        console.log(t3);

        // AJOUT DES VALEURS SUR LES GRAPHIQUES
        temp1[0].addPoint([x, t1], true, true);
        temp2[0].addPoint([x, t2], true, true);
        temp3[0].addPoint([x, t3], true, true);

    }

    socket.onclose = function() {
        console.log("disconnected");
    };

    /*************************************************** GRAPHIQUE TEMPERATURE 1 ***************************************************/

    window.chart1 = Highcharts.chart('temperature1', {
        chart: {
            type: 'spline',
            animation: Highcharts.svg,
            borderColor: "black",
            borderWidth: 2,
            marginRight: 10,

            events: {

            }
        },
        time: {
            useUTC: false
        },

        credits: {
            enabled: false
        },

        title: {
            text: '<b>Température</b><br/><p style="color: blue;"> (Capteur 1)</p>'
        },

        plotOptions: {

            series: {
                color: 'blue'
            }

        },

        // AXE DU TEMPS
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },

        // AXE DES VALEURS
        yAxis: {
            title: {
                text: 'Degrés'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Cpt1 Température',
            data: (function() {

                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -60; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: 0
                    });
                }
                return data;
            }())
        }]
    });

    /*************************************************** GRAPHIQUE TEMPERATURE 2 ***************************************************/

    window.chart2 = Highcharts.chart('temperature2', {
        chart: {
            type: 'spline',
            animation: Highcharts.svg,
            borderColor: "black",
            borderWidth: 2,
            marginRight: 10,

            events: {

            }
        },
        time: {
            useUTC: false
        },

        credits: {
            enabled: false
        },

        title: {
            text: '<b>Température</b><br/><p style="color: green;"> (Capteur 2)</p>'
        },

        plotOptions: {

            series: {
                color: 'green'
            }

        },

        // AXE DU TEMPS
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },

        // AXE DES VALEURS
        yAxis: {
            title: {
                text: 'Degrés'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Cpt2 Température',
            data: (function() {

                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -60; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: 0
                    });
                }
                return data;
            }())
        }]
    });


    /*************************************************** GRAPHIQUE TEMPERATURE 3 ***************************************************/

    window.chart3 = Highcharts.chart('temperature3', {
        chart: {
            type: 'spline',
            animation: Highcharts.svg,
            borderColor: "black",
            borderWidth: 2,
            marginRight: 10,

            events: {

            }
        },
        time: {
            useUTC: false
        },

        credits: {
            enabled: false
        },

        title: {
            text: '<b>Température</b><br/><p style="color: red;"> (Capteur 3)</p>'
        },

        plotOptions: {

            series: {
                color: 'red'
            }

        },

        // AXE DU TEMPS
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },

        // AXE DES VALEURS
        yAxis: {
            title: {
                text: 'Degrés'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: 'red'
            }]
        },

        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.x:%Y-%m-%d %H:%M:%S}<br/>{point.y:.2f}'
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Cpt3 Température',
            data: (function() {

                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -60; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: 0
                    });
                }
                return data;
            }())
        }]
    });
});