<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <title>Ajout d'une station</title>
    <script src="../js/jquery.js"></script>
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>

    <?php

        // Importation du fichier de connexion à la BDD
        include('../bdd/connect.php');

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idConfiguration, mode FROM `Configuration_communication`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $configuration = "";

        // Tant que la requête est exécutée
        while($row = $reponse->fetch())
        {
            $configuration = $configuration."<option>$row[0] - $row[1]</option>";
        }

        $reponse->closeCursor();

    ?>
</head>

<body>

    <header>
        <div id="titre">
            <div id="logo">
                <img src="../images/ovpf.png" alt="Logo OVPF" />
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <div id="menu1"></div>

    <br/><br/>

    <center>
        <!-- Formulaire pour ajouter une station dans la BDD -->
        <form action="ajouter_station_traitement.php" id="ajout" method="POST">
            <h1>NOUVELLE STATION</h1>
            <br/><br/>
            <input type="hidden" name="idGet">

            <p>Nom de la station</p>
            <input type="text" name="nom" required>
            <br><br>

            <p>Lieu d'affectation</p>
            <input type="text" name="lieux_affectation" required>
            <br><br>

            <p>Date de destruction</p>
            <input type="date" name="date_de_destruction" maxlength="10">
            <br><br>

            <p>Configuration réseau</p>
             <select name="Configuration_communication_idConfiguration" style="width:40%;" required>
                <?php echo $configuration;?>
            </select>
            <br>
            <br>
            <br>

            <input type="submit" value="Ajouter">
            <br>
            <input type="button" value="Retour" onclick="history.back()">
            <br />
        </form>

    </center>

</html>