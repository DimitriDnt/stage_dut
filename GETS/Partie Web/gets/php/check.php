<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Traitement</title>
	</head>
	<body>
		<center>
			<?php

				// Importation du fichier de connexion à la BDD
        		include('../bdd/connect.php');
        		
				// On prépare les requêtes SQL pour récupérer les identifiants et les mot de passes
				$sql1 = $bdd->query("SELECT login_admin, pwd_admin FROM Admins");
				$sql2 = $bdd->query("SELECT login_user, pwd_user FROM Users");

				$PWD  = $_POST['mdp'];
				$PASS = hash('MD5', $PWD);
				// On exécute les requêtes SQL
				while ($donnees1 = $sql1->fetch() AND $donnees2 = $sql2->fetch()) {
				    // Si le nom et le mot de passe de l'administrateur correspondent aux valeurs contenues dans la base de données
				    if ($_POST['nom'] == $donnees1['login_admin'] AND $PASS == $donnees1['pwd_admin']) {
				        header('Location: admin_page.php');
				    }
				    
				    // Si le nom et le mot de passe de l'utilisateur correspondent aux valeurs contenues dans la base de données
				    elseif ($_POST['nom'] == $donnees2['login_user'] AND $PASS == $donnees2['pwd_user']) {
				        header('Location: user_page.php');
				    }
				    // Si les identifiants saisis sont incorrects, il y aura une redirection sur la page de connexion avec un message d'erreur
				    else {
				        echo '<br>';
				        header('Location: login.php?auth=err');
				    }
				}
			?>
		</center>
	</body> 
</html>
