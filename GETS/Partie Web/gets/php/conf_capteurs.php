<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Capteurs</title>
    <!-- Bootstrap -->
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <!-- Importation de jQuery -->
    <script src="../js/jquery.js"></script>
    <!-- Importation des plugins et scripts -->
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/capteurs.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>

    <!-- Script PHP pour la liste des stations GETS présentes dans la BDD -->
    <?php

        // Importation du fichier de connexion à la BDD
        include('../bdd/connect.php');

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idGet, lieux_affectation FROM `Stations_GETS`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $options = "";

        // Tant que la requête est exécutée
        while ($row = $reponse->fetch()) {
            $options = $options . "<option>GETS $row[0]</option>";
        }

        $reponse->closeCursor();

    ?>

        <!-- Script PHP pour la liste des capteurs présents dans la BDD -->
        <?php

        // On sélectionne uniquement les capteurs de type électrochimiques et étant non utilisés
        $sql = ("SELECT `idCapteur`, `nom` FROM Capteurs WHERE  type_de_capteur_idtype_de_capteur = '1' AND etat='Libre'");

        // On execute la requête
        $reponse = $bdd->query($sql);

        $capteurs = "";

        // Tant que la requête est exécutée
        while ($row = $reponse->fetch()) {
            $capteurs = $capteurs . "<option>$row[1]</option>";
        }

        $reponse->closeCursor();

    ?>
</head>

<body>
    <header>
        <div id="titre">
            <div id="logo">
                <a href="../php/admin_page.php"><img src="../images/ovpf.png" alt="Logo OVPF" /></a>
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <div id="menu1"></div>

    <!-- Popup qui apparait lors de la sélection d'une station GETS -->
    <center>
        <div id="confirm">
            <div class="message"></div>
            <br/>
            <button class="bdd">Lecture BDD</button>
            <button class="ws">Lecture WS</button>
        </div>
    </center>
    <br/>

    <!-- Liste des stations GETS de la BDD -->
    <div id="selection">
        <center>
            <b><p>Sélectionnez la station GETS à configurer :</p></b>
            <select id="input" style="width: 30%;">
                <option value=""></option>
                <?php echo $options;?>
            </select>
            <button type="button" id="add" onclick="window.location.href='../php/ajouter_station.php'">+</button>
        </center>
    </div>
    <div id="box">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div id="gauche">
                        <center>
                            <h3>CONVERTISSEUR 1</h3>
                            <div class="column" style="background-color:#aaa;">
                                <b><p>Capteur 1</p></b>
                                <select id="name1" class="capteur" style="width: 100%; padding: 3px;">
                                    <option value=""></option>
                                    <?php echo $capteurs; ?>
                                </select>
                                <br/>
                                <br/>
                                <b><p>Gain</p></b>
                                <input type="text" id="gain1" disabled="disabled" />
                                <br/>
                                <br/>
                                <b><p>A0</p></b>
                                <input type="text" id="A01" disabled="disabled" />
                                <br/>
                                <br/>
                                <b><p>A1</p></b>
                                <input type="text" id="A11" maxlength="10" disabled="disabled" />
                                <br/>
                                <br/>
                                <b><p>Date d'installation</p></b>
                                <input type="text" id="installation1" maxlength="10" disabled="disabled" />
                                <br/>
                                <br/>
                                <b><p>Date d'etalonnage</p></b>
                                <input type="text" id="etalonnage1" maxlength="10" disabled="disabled" />
                                <br/>
                                <hr>
                                <input type="checkbox" class="checkbox" id="checkbox1" value="False" />Activer
                                <br/>
                                </form>
                            </div>
                            <div class="column" style="background-color:#bbb;">
                                <form>
                                    <b><p>Capteur 2</p></b>
                                    <select id="name2" class="capteur" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                    <br/>
                                    <br/>
                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain2" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A0</p></b>
                                    <input type="text" id="A02" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A1</p></b>
                                    <input type="text" id="A12" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation2" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage2" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>
                                    <input type="checkbox" class="checkbox" id="checkbox2" name="enable" value="False">Activer
                                    <br/>
                                </form>
                            </div>
                            <div class="column" style="background-color:#ccc;">
                                <form>
                                    <b><p>Capteur 3</p></b>
                                    <select id="name3" class="capteur" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                    <br/>
                                    <br/>
                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain3" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A0</p></b>
                                    <input type="text" id="A03" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A1</p></b>
                                    <input type="text" id="A13" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation3" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage3" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>
                                    <input type="checkbox" class="checkbox" id="checkbox3" name="enable" value="False">Activer
                                    <br/>
                                </form>
                            </div>
                            <div class="column" style="background-color:#ddd;">
                                <form>
                                    <b><p>Capteur 4</p></b>
                                    <select id="name4" class="capteur" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                    <br/>
                                    <br/>
                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain4" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A0</p></b>
                                    <input type="text" id="A04" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A1</p></b>
                                    <input type="text" id="A14" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation4" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage4" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>
                                    <input type="checkbox" class="checkbox" id="checkbox4" value="False">Activer
                                    <br/>
                                </form>
                            </div>
                            <input type="checkbox" id="all1" name="tout">
                            <br/>Activer tout
                            <br/>
                        </center>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div id="droite">
                        <center>
                            <h3>CONVERTISSEUR 2</h3>
                            <div class="column" style="background-color:#aaa;">
                                <form>
                                    <b><p>Capteur 1</p></b>
                                    <select id="name5" class="capteur" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                    <br/>
                                    <br/>
                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain5" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A0</p></b>
                                    <input type="text" id="A05" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A1</p></b>
                                    <input type="text" id="A15" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation5" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage5" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>
                                    <input type="checkbox" class="checkboxes" id="checkbox5" value="False">
                                    <br/>Activer
                                </form>
                            </div>
                            <div class="column" style="background-color:#bbb;">
                                <form>
                                    <b><p>Capteur 2</p></b>
                                    <select id="name6" class="capteur" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                    <br/>
                                    <br/>
                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain6" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A0</p></b>
                                    <input type="text" id="A06" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A1</p></b>
                                    <input type="text" id="A16" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation6" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage6" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>
                                    <input type="checkbox" class="checkboxes" id="checkbox6" name="enable" value="False">
                                    <br/>Activer
                                </form>
                            </div>
                            <div class="column" style="background-color:#ccc;">
                                <form>
                                    <b><p>Capteur 3</p></b>
                                    <select id="name7" class="capteur" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                    <br/>
                                    <br/>
                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain7" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A0</p></b>
                                    <input type="text" id="A07" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A1</p></b>
                                    <input type="text" id="A17" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation7" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage7" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>
                                    <input type="checkbox" class="checkboxes" id="checkbox7" name="enable" value="False">
                                    <br/>Activer
                                    <br/>
                                </form>
                            </div>
                            <div class="column" style="background-color:#ddd;">
                                <form>
                                    <b><p>Capteur 4</p></b>
                                    <select id="name8" class="capteur" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                    <br/>
                                    <br/>
                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain8" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A0</p></b>
                                    <input type="text" id="A08" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>A1</p></b>
                                    <input type="text" id="A18" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation8" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>
                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage8" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>
                                    <input type="checkbox" class="checkboxes" id="checkbox8" value="False">
                                    <br/>Activer
                                    <br/>
                                </form>
                            </div>
                            <input type="checkbox" id="all2" name="tout">
                            <br/>Activer tout
                        </center>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <center>
                        <h3>CAPTEURS DIVERS</h3>
                        <div id="capteurs" style="background-color: #bbb; width:100%;">
                            <br/>
                            <label>
                                <input type="checkbox" class="gps" name="gps" id="gps" value="False" / style="background-color: red;"> GPS
                                <hr>
                                <input type="checkbox" class="co2a" name="co2a" id="CO2_1" value="False" /> CO2_1
                                <input type="checkbox" class="co2b" name="co2b" id="CO2_2" value="False" style="margin-left: 30px;" /> CO2_2
                                <hr>
                                <input type="checkbox" class="energie" name="energie" id="MesureUI" value="False" /> Mesure de l'énergie
                            </label>
                            <hr>
                            <label>
                                <input type="checkbox" class="air1" name="air" id="Qair_1" value="False" /> Qualité de l'air 1
                                <input type="checkbox" class="air2" name="air" id="Qair_2" value="False" style="margin-left: 30px;" /> Qualité de l'air 2
                            </label>
                            <hr>
                            <label>
                                <input type="checkbox" class="temp" name="temp" id="Temp" value="False" /> Température
                                <input type="checkbox" class="baro" name="baro" id="Baro" value="False" style="margin-left: 30px;" /> Baromètre
                            </label>
                            <hr>
                            <label>
                                <input type="checkbox" class="particules1" name="part1" id="C_Particules_1" value="False" /> Mesure de particules 1
                                <input type="checkbox" class="particules2" name="part2" id="C_Particules_2" value="False" style="margin-left: 30px;" /> Mesure de particules 2
                            </label>
                            <br/>
                        </div>
                    </center>
                </div>
            </div>
            <br/>
            <center>
                <button type="button" id="add1" onclick="window.location.href='../php/ajouter_capteur.php'">Ajouter un capteur</button>
                <button type="button" id="add1" onclick="window.location.href='../php/ajouter_fiche_etalonnage.php'">Fiche d'étalonnage</button>
                <button type="button" id="add1" onclick="window.location.href='../php/intervention.php'">Table d'interventions</button>
                <button type="button" id="add1" onclick="window.location.href='../php/etalonner_capteur.php'">Etalonner un capteur</button>
            </center>
            <br/>
            <br/>
            <center>
                <button type=button id="apply" class="button">SAUVEGARDER</button>
            </center>
        </div>
    </div>
    </div>
    </div>
    </center>
    </div>
    <footer style="top: 50px;">
        <p>OVPF | Geochemical Easily Transported System (GETS)</p>
    </footer>
</body>

</html>