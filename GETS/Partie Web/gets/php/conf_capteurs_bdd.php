<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Capteurs</title>
    <!-- Bootstrap -->
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <!-- Importation de jQuery -->
    <script src="../js/jquery.js"></script>
    <!-- Importation des plugins et scripts -->
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/capteurs.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>

    <!-- Script PHP pour la liste des stations GETS présentes dans la BDD -->
    <?php

        // Importation du fichier de connexion à la BDD
        include('../bdd/connect.php');

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idGet, lieux_affectation FROM `Stations_GETS`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $options = "";

        // Tant que la requête est exécutée
        while ($row = $reponse->fetch()) {
            $options = $options . "<option value=$row[0]>GETS $row[0]</option>";
        }

        $reponse->closeCursor();

    ?>

    <!-- Script PHP pour la liste des capteurs présents dans la BDD -->
    <?php
            // On sélectionne uniquement les capteurs de type électrochimiques et étant non utilisés
            $sql = ("SELECT `idCapteur`, `nom` FROM Capteurs WHERE type_de_capteur_idtype_de_capteur = '1' AND etat = 'Libre'");

            // On execute la requête
            $reponse = $bdd->query($sql);

            $capteurs = "";

            // Tant que la requête est exécutée, on stocke les valeurs dans des variables
            while ($row = $reponse->fetch()) {

                $capteurs = $capteurs . "<option value=$row[0]>$row[0] - $row[1]</option>";
            }

            // On termine la requête
            $reponse->closeCursor();

    ?>

    <?php

        // SI ON SELECTIONNE UN CAPTEUR
        if (isset($_POST['liste1'])) {
            
            $id = $_POST['liste1'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                             FROM Stations_GETS, Fiche_etalonnages 
                             WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                             AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                             FROM Capteurs, Stations_GETS 
                             WHERE idCapteur = '$id'
                             AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage = $row["date"];
                $a0         = $row["A0"];
                $a1         = $row["A1"];
                $gain       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom  = $row["nom"];
                $date = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            //echo $nom;
        }

        // CAPTEUR 2
        if (isset($_POST['liste2'])) {
            
            $id = $_POST['liste2'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                                FROM Stations_GETS, Fiche_etalonnages 
                                WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                                AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                                FROM Capteurs, Stations_GETS 
                                WHERE idCapteur = '$id'
                                AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage1 = $row["date"];
                $a01         = $row["A0"];
                $a11         = $row["A1"];
                $gain1       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom1  = $row["nom"];
                $date1 = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
        }

        if (isset($_POST['liste3'])) {
            
            $id = $_POST['liste3'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                                FROM Stations_GETS, Fiche_etalonnages 
                                WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                                AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                                FROM Capteurs, Stations_GETS 
                                WHERE idCapteur = '$id'
                                AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage2 = $row["date"];
                $a02         = $row["A0"];
                $a12         = $row["A1"];
                $gain2       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom2  = $row["nom"];
                $date2 = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
        }
        // CAPTEUR 4
        if (isset($_POST['liste4'])) {
            
            $id = $_POST['liste4'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                             FROM Stations_GETS, Fiche_etalonnages 
                             WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                             AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                             FROM Capteurs, Stations_GETS 
                             WHERE idCapteur = '$id'
                             AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage3 = $row["date"];
                $a03         = $row["A0"];
                $a13         = $row["A1"];
                $gain3       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom3  = $row["nom"];
                $date3 = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
        }

        // CAPTEUR 5
        if (isset($_POST['liste5'])) {
            
            $id = $_POST['liste5'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                             FROM Stations_GETS, Fiche_etalonnages 
                             WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                             AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                             FROM Capteurs, Stations_GETS 
                             WHERE idCapteur = '$id'
                             AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage4 = $row["date"];
                $a04         = $row["A0"];
                $a14         = $row["A1"];
                $gain4       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom4  = $row["nom"];
                $date4 = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
        }


        // CAPTEUR 6
        if (isset($_POST['liste6'])) {
            
            $id = $_POST['liste6'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                             FROM Stations_GETS, Fiche_etalonnages 
                             WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                             AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                             FROM Capteurs, Stations_GETS 
                             WHERE idCapteur = '$id'
                             AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage5 = $row["date"];
                $a05         = $row["A0"];
                $a15         = $row["A1"];
                $gain5       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom5  = $row["nom"];
                $date5 = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
        }

        // CAPTEUR 7
        if (isset($_POST['liste7'])) {
            
            $id = $_POST['liste7'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                             FROM Stations_GETS, Fiche_etalonnages 
                             WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                             AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                             FROM Capteurs, Stations_GETS 
                             WHERE idCapteur = '$id'
                             AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage6 = $row["date"];
                $a06         = $row["A0"];
                $a16         = $row["A1"];
                $gain6       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom6  = $row["nom"];
                $date6 = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
        }

        // CAPTEUR 8
        if (isset($_POST['liste8'])) {
            
            $id = $_POST['liste8'];
            
            
            // Requête pour sélectionner les coefficients la date et les coefficients d'étalonnage du capteur 1 de la station choisie
            $sql1 = "SELECT idGet, Stations_GETS.Capteurs_idCapteur, date, gain, A0, A1 
                             FROM Stations_GETS, Fiche_etalonnages 
                             WHERE Stations_GETS.Capteurs_idCapteur = '$id'
                             AND Fiche_etalonnages.Capteurs_idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            // Requête pour sélectionner le nom et la date d'installation du capteur de la station choisie
            $sql2 = "SELECT idGet, idCapteur, Capteurs.nom, Capteurs.date_de_mise_en_service 
                             FROM Capteurs, Stations_GETS 
                             WHERE idCapteur = '$id'
                             AND idCapteur = Stations_GETS.Capteurs_idCapteur";
            
            
            // On exécute la première requête
            $reponse = $bdd->query($sql1);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $etalonnage7 = $row["date"];
                $a07         = $row["A0"];
                $a17         = $row["A1"];
                $gain7       = $row["gain"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
            
            
            // On exécute la deuxième requête
            $reponse = $bdd->query($sql2);
            
            // On stocke les valeurs de la base dans des variables
            while ($row = $reponse->fetch()) {
                
                $nom7  = $row["nom"];
                $date7 = $row["date_de_mise_en_service"];
            }
            
            // On termine la requête
            $reponse->closeCursor();
        }

    ?>

    <?php

        if (isset($_POST['stations'])) {

            $idGet = $_POST['stations'];

            $sql  = "SELECT idGet, idConfiguration, ssid, pwd, portWS, portTelnet, mode, ip, masque, gw, dns1 
                     FROM Stations_GETS, Configuration_communication 
                     WHERE Stations_GETS.idGet = '$idGet'
                     AND idConfiguration = Stations_GETS.Configuration_communication_idConfiguration";


            $reponse = $bdd->query($sql);

            // On affiche les données contenues dans la table materiel

            while ($donnees = $reponse->fetch())

            {
                $get = 'GETS'." ".$donnees["idGet"];
            }

            $reponse->closeCursor(); // Permet de terminer la requête en cours

        }
    ?>

</head>

<script>

    $(document).ready(function() {


        function printSensors() {

                    // CAPTEUR 1
                    var nom = "<?php echo $nom; ?>";
                    var gain = "<?php echo $gain; ?>";
                    var etalonnage = "<?php echo $etalonnage; ?>";
                    var a0 = "<?php echo $a0; ?>";
                    var a1 = "<?php echo $a1; ?>";                    
                    var installation = "<?php echo $date; ?>";

                    // CAPTEUR 2
                    var nom1 = "<?php echo $nom1; ?>";
                    var gain1 = "<?php echo $gain1; ?>";
                    var etalonnage1 = "<?php echo $etalonnage1; ?>";
                    var a01 = "<?php echo $a01; ?>";
                    var a11 = "<?php echo $a11; ?>";                    
                    var installation1 = "<?php echo $date1; ?>";

                    // CAPTEUR 3
                    var nom2 = "<?php echo $nom2; ?>";
                    var gain2 = "<?php echo $gain2; ?>";
                    var etalonnage2 = "<?php echo $etalonnage2; ?>";
                    var a02 = "<?php echo $a02; ?>";
                    var a12 = "<?php echo $a12; ?>";                    
                    var installation2 = "<?php echo $date2; ?>";

                    // CAPTEUR 4
                    var nom3 = "<?php echo $nom3; ?>";
                    var gain3 = "<?php echo $gain3; ?>";
                    var etalonnage3 = "<?php echo $etalonnage3; ?>";
                    var a03 = "<?php echo $a03; ?>";
                    var a13 = "<?php echo $a13; ?>";                    
                    var installation3 = "<?php echo $date3; ?>";

                    // CAPTEUR 5
                    var nom4 = "<?php echo $nom4; ?>";
                    var gain4 = "<?php echo $gain4; ?>";
                    var etalonnage4 = "<?php echo $etalonnage4; ?>";
                    var a04 = "<?php echo $a04; ?>";
                    var a14 = "<?php echo $a14; ?>";                    
                    var installation4 = "<?php echo $date4; ?>";

                    // CAPTEUR 6
                    var nom5 = "<?php echo $nom5; ?>";
                    var gain5 = "<?php echo $gain5; ?>";
                    var etalonnage5 = "<?php echo $etalonnage5; ?>";
                    var a05 = "<?php echo $a05; ?>";
                    var a15 = "<?php echo $a15; ?>";                    
                    var installation5 = "<?php echo $date5; ?>";

                    // CAPTEUR 7
                    var nom6 = "<?php echo $nom6; ?>";
                    var gain6 = "<?php echo $gain6; ?>";
                    var etalonnage6 = "<?php echo $etalonnage6; ?>";
                    var a06 = "<?php echo $a06; ?>";
                    var a16 = "<?php echo $a16; ?>";                    
                    var installation6 = "<?php echo $date6; ?>";

                    // CAPTEUR 8
                    var nom7 = "<?php echo $nom7; ?>";
                    var gain7 = "<?php echo $gain7; ?>";
                    var etalonnage7 = "<?php echo $etalonnage7; ?>";
                    var a07 = "<?php echo $a07; ?>";
                    var a17 = "<?php echo $a17; ?>";                    
                    var installation7 = "<?php echo $date7; ?>";

                    // NOM DU CAPTEUR
                    $("#name1").val(nom);
                    $("#name2").val(nom1);
                    $("#name3").val(nom2);
                    $("#name4").val(nom3);
                    $("#name5").val(nom4);
                    $("#name6").val(nom5);
                    $("#name7").val(nom6);
                    $("#name8").val(nom7);

                    // GAIN DU CAPTEUR
                    $("#gain1").val(gain);
                    $("#gain2").val(gain1);
                    $("#gain3").val(gain2);
                    $("#gain4").val(gain3);
                    $("#gain5").val(gain4);
                    $("#gain6").val(gain5);
                    $("#gain7").val(gain6);
                    $("#gain8").val(gain7);

                    // DATE D'ETALONNAGE DU CAPTEUR
                    $("#etalonnage1").val(etalonnage);
                    $("#etalonnage2").val(etalonnage1);
                    $("#etalonnage3").val(etalonnage2);
                    $("#etalonnage4").val(etalonnage3);
                    $("#etalonnage5").val(etalonnage4);
                    $("#etalonnage6").val(etalonnage5);
                    $("#etalonnage7").val(etalonnage6);
                    $("#etalonnage8").val(etalonnage7);

                    // COEFFICIENT A0 DU CAPTEUR
                    $("#A01").val(a0);
                    $("#A02").val(a01);
                    $("#A03").val(a02);
                    $("#A04").val(a03);
                    $("#A05").val(a04);
                    $("#A06").val(a05);
                    $("#A07").val(a06);
                    $("#A08").val(a07);

                    // COEFFICIENT A1 DU CAPTEUR
                    $("#A11").val(a1);
                    $("#A12").val(a11);
                    $("#A13").val(a12);
                    $("#A14").val(a13);
                    $("#A15").val(a14);
                    $("#A16").val(a15);
                    $("#A17").val(a16);
                    $("#A18").val(a17);

                    // DATE D'INSTALLATION DU CAPTEUR
                    $("#installation1").val(installation);
                    $("#installation2").val(installation1);
                    $("#installation3").val(installation2);
                    $("#installation4").val(installation3);
                    $("#installation5").val(installation4);
                    $("#installation6").val(installation5);
                    $("#installation7").val(installation6);
                    $("#installation8").val(installation7);

                };

            printSensors();

    });
</script>

<body>

    <!-- BANNIERE EN HAUT DU MENU -->
    <header>
        <div id="titre">
            <div id="logo">
                <a href="../php/admin_page.php"><img src="../images/ovpf.png" alt="Logo OVPF" /></a>
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <!-- IMPORTATION DU MENU -->
    <div id="menu1"></div>


    <!-- POPUP LECTURE BDD OU LECTURE WEBSOCKET -->
    <center>
        <div id="confirm">
            <div class="message"></div>
            <br/>
            <button class="bdd">Lecture BDD</button>
            <button class="ws">Lecture WS</button>
        </div>
    </center>
    <br/>

    <!-- LISTE POUR SELECTIONNER LES STATIONS GETS -->
    <div id="selection">
        <center>
            <b><p>Sélectionnez la station GETS à configurer :</p></b>
            <form action="" method="POST">
            <select name="stations" id="stations" style="width: 30%;" onchange="this.form.submit();">
                <option value=""></option>
                <?php echo $options;?>
            </select>
            <button type="button" id="add" onclick="window.location.href='../php/ajouter_station.php'">+</button>
            <br><br>
            <b><p>Station choisie : </p></b>
            <input type="text" value="<?php echo $get; ?>" style="width: 15%;"/>
        </form>
        </center>
    </div>

    <!-- TOUS LES BLOCS PRESENTS SUR LA PAGE -->
    <div id="box">
        <div class="container-fluid">
            <div class="row">

            	<!-- BLOC DU CONVERTISSEUR 1 -->
                <div class="col-md-4 col-sm-4">
                        <center>
                            <h3>CONVERTISSEUR 1</h3>
                            <form action="" method="POST">
                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#aaa;">

                                <b><p>Capteur 1</p></b>
                                    <select id="liste1" name="liste1" style="width: 100%; padding: 3px;">
                                        <option value=""></option>
                                        <?php echo $capteurs;?>
                                    </select>
                                <br/>
                                <br/>

                                <b><p>Nom</p></b>
                                <input type="text" id="name1" disabled="disabled" />
                                <br/>
                                <br/>

                                <b><p>Gain</p></b>
                                <input type="text" id="gain1" disabled="disabled" />
                                <br/>
                                <br/>

                                <b><p>A0</p></b>
                                <input type="text" id="A01" disabled="disabled" />
                                <br/>
                                <br/>

                                <b><p>A1</p></b>
                                <input type="text" id="A11" maxlength="10" disabled="disabled" />
                                <br/>
                                <br/>

                                <b><p>Date d'installation</p></b>
                                <input type="text" id="installation1" maxlength="10" disabled="disabled" />
                                <br/>
                                <br/>

                                <b><p>Date d'etalonnage</p></b>
                                <input type="text" id="etalonnage1" maxlength="10" disabled="disabled" />
                                <br/>
                                <hr>

                                <input type="checkbox" class="checkbox" id="checkbox1" value="False" />Activer
                            </div>

                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#bbb;">

                                    <b><p>Capteur 2</p></b>
                                        <select id="liste2" name="liste2" style="width: 100%; padding: 3px;">
                                            <option value=""></option>
                                            <?php echo $capteurs;?>
                                        </select>
                                    <br/>
                                    <br/>
                                    
                                    <b><p>Nom</p></b>
                                    <input type="text" id="name2" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain2" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A0</p></b>
                                    <input type="text" id="A02" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A1</p></b>
                                    <input type="text" id="A12" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation2" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage2" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>

                                    <input type="checkbox" class="checkbox" id="checkbox2" name="enable" value="False" />Activer
                                    <br/>
                            </div>

                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#ccc;">

                                    <b><p>Capteur 3</p></b>
                                        <select id="liste3" name="liste3" style="width: 100%; padding: 3px;">
                                            <option value=""></option>
                                            <?php echo $capteurs;?>
                                        </select>
                                    <br/>
                                    <br/>

                                    <b><p>Nom</p></b>
                                    <input type="text" id="name3" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain3" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A0</p></b>
                                    <input type="text" id="A03" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A1</p></b>
                                    <input type="text" id="A13" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation3" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage3" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>

                                    <input type="checkbox" class="checkbox" id="checkbox3" name="enable" value="False" />Activer
                                    <br/>
                            </div>

                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#ddd;">

                                    <b><p>Capteur 4</p></b>
                                        <select id="liste4" name="liste4" style="width: 100%; padding: 3px;">
                                            <option value=""></option>
                                            <?php echo $capteurs;?>
                                        </select>
                                    <br/>
                                    <br/>
                                    
                                    <b><p>Nom</p></b>
                                    <input type="text" id="name4" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain4" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A0</p></b>
                                    <input type="text" id="A04" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A1</p></b>
                                    <input type="text" id="A14" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation4" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage4" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>

                                    <input type="checkbox" class="checkbox" id="checkbox4" value="False" />Activer
                                    <br/>
                            </div>
                            <input type="checkbox" id="all1" name="tout">
                            <br/>Activer tout
                            <br/>
                        </center>
                </div>

                <!-- BLOC DU CONVERTISSEUR 2 -->
                <div class="col-md-4 col-sm-4">
                        <center>
                            <h3>CONVERTISSEUR 2</h3>
                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#aaa;">

                                    <b><p>Capteur 1</p></b>
                                        <select id="liste5" name="liste5" style="width: 100%; padding: 3px;">
                                            <option value=""></option>
                                            <?php echo $capteurs;?>
                                        </select>
                                    <br/>
                                    <br/>
                                    
                                    <b><p>Nom</p></b>
                                    <input type="text" id="name5" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain5" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A0</p></b>
                                    <input type="text" id="A05" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A1</p></b>
                                    <input type="text" id="A15" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation5" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage5" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>

                                    <input type="checkbox" class="checkboxes" id="checkbox5" value="False" />
                                    <br/>Activer
                            </div>

                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#bbb;">

                                    <b><p>Capteur 2</p></b>
                                        <select id="liste6" name="liste6" style="width: 100%; padding: 3px;">
                                            <option value=""></option>
                                            <?php echo $capteurs;?>
                                        </select>
                                    <br/>
                                    <br/>
                                    
                                    <b><p>Nom</p></b>
                                    <input type="text" id="name6" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain6" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A0</p></b>
                                    <input type="text" id="A06" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A1</p></b>
                                    <input type="text" id="A16" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation6" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage6" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>

                                    <input type="checkbox" class="checkboxes" id="checkbox6" name="enable" value="False" />
                                    <br/>Activer
                            </div>

                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#ccc;">

                                    <b><p>Capteur 3</p></b>
                                        <select id="liste7" name="liste7" style="width: 100%; padding: 3px;">
                                            <option value=""></option>
                                            <?php echo $capteurs;?>
                                        </select>
                                    <br/>
                                    <br/>
                                    
                                    <b><p>Nom</p></b>
                                    <input type="text" id="name7" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain7" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A0</p></b>
                                    <input type="text" id="A07" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A1</p></b>
                                    <input type="text" id="A17" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation7" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage7" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>

                                    <input type="checkbox" class="checkboxes" id="checkbox7" name="enable" value="False" />
                                    <br/>Activer
                                    <br/>
                            </div>

                            <!-- BLOC CAPTEUR -->
                            <div class="column" style="background-color:#ddd;">

                                    <b><p>Capteur 4</p></b>
                                        <select id="liste8" name="liste8" style="width: 100%; padding: 3px;">
                                            <option value=""></option>
                                            <?php echo $capteurs;?>
                                        </select>
                                    <br/>
                                    <br/>
                                    
                                    <b><p>Nom</p></b>
                                    <input type="text" id="name8" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Gain</p></b>
                                    <input type="text" id="gain8" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A0</p></b>
                                    <input type="text" id="A08" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>A1</p></b>
                                    <input type="text" id="A18" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'installation</p></b>
                                    <input type="text" id="installation8" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <br/>

                                    <b><p>Date d'etalonnage</p></b>
                                    <input type="text" id="etalonnage8" maxlength="10" disabled="disabled" />
                                    <br/>
                                    <hr>

                                    <input type="checkbox" class="checkboxes" id="checkbox8" value="False" />
                                    <br/>Activer
                                    <br/>
                            </div>
                            <input type="checkbox" id="all2" name="tout">
                            <br/>Activer tout
                        </center>
                        <br>
                    <input type="submit" name="submit" id="envoi" value="CHARGER CONFIGURATION CAPTEURS">

                </div>
            </form>

                <!-- BLOC CAPTEURS DIVERS -->
                <div class="col-md-4 col-sm-4">
                    <center>

                        <h3>CAPTEURS DIVERS</h3>
                        <div id="capteurs" style="background-color: #bbb; width:100%;">
                            <br/>

                            <label>
                            	<!-- CASE A COCHER -->
                                <input type="checkbox" class="gps" name="gps" id="gps" value="False" / style="background-color: red;"> GPS
                                <hr>
                                <!-- CASE A COCHER -->
                                <input type="checkbox" class="co2a" name="co2a" id="CO2_1" value="False" /> CO2_1
                                <!-- CASE A COCHER -->
                                <input type="checkbox" class="co2b" name="co2b" id="CO2_2" value="False" style="margin-left: 30px;" /> CO2_2
                                <hr>
                                <!-- CASE A COCHER -->
                                <input type="checkbox" class="energie" name="energie" id="MesureUI" value="False" /> Mesure de l'énergie
                            </label>
                            <hr>
                            <label>
                            	<!-- CASE A COCHER -->
                                <input type="checkbox" class="air1" name="air" id="Qair_1" value="False" /> Qualité de l'air 1
                                <!-- CASE A COCHER -->
                                <input type="checkbox" class="air2" name="air" id="Qair_2" value="False" style="margin-left: 30px;" /> Qualité de l'air 2
                            </label>
                            <hr>
                            <label>
                            	<!-- CASE A COCHER -->
                                <input type="checkbox" class="temp" name="temp" id="Temp" value="False" /> Température
                                <!-- CASE A COCHER -->
                                <input type="checkbox" class="baro" name="baro" id="Baro" value="False" style="margin-left: 30px;" /> Baromètre
                            </label>
                            <hr>
                            <label>
                            	<!-- CASE A COCHER -->
                                <input type="checkbox" class="particules1" name="part1" id="C_Particules_1" value="False" /> Mesure de particules 1
                                <!-- CASE A COCHER -->
                                <input type="checkbox" class="particules2" name="part2" id="C_Particules_2" value="False" style="margin-left: 30px;" /> Mesure de particules 2
                            </label>
                            
                            <br/>
                        
                        </div>
                    
                    </center>
                </div>
            </div>
            <br/>

             <!-- BOUTONS / LIENS POUR LANCER DES FORMULAIRES -->
            <center>
                <button type="button" id="add1" onclick="window.location.href='../php/ajouter_capteur.php'">Ajouter un capteur</button>
                <button type="button" id="add1" onclick="window.location.href='../php/ajouter_fiche_etalonnage.php'">Fiche d'étalonnage</button>
                <button type="button" id="add1" onclick="window.location.href='../php/intervention.php'">Table d'interventions</button>
                <button type="button" id="add1" onclick="window.location.href='../php/etalonner_capteur.php'">Etalonner un capteur</button>
            </center>

            <br/>
            <br/>

            <!-- BOUTON POUR ENVOYER LE JSON -->
            <center>
                <button type=button id="apply" class="button">SAUVEGARDER</button>
            </center>
        </div>
    </div>
    </div>
    </div>
    </center>
    </div>

    <body>

</html>