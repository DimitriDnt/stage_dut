<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link rel="icon" href="../images/ovpf.ico" />
	    <link rel="stylesheet" type="text/css" href="../css/style.css" />
	    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
	    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
	    <title>Fiche d'étalonnage</title>
	    <script src="../js/jquery.js"></script>
	    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
	    <script type="text/javascript" src="../js/menus.js"></script>

		<?php

			// Importation du fichier de connexion à la BDD
			include('../bdd/connect.php');

			// Requete SQL pour sélectionner les champs dans la base
			$sql = "SELECT idCapteur, Nom FROM `Capteurs`";

			// On execute la requête
			$reponse = $bdd->query($sql);

			$options = "";

			// Stockage des données dans une liste
			while($row = $reponse->fetch())
			{
			    $options = $options."<option>$row[0] - $row[1]</option>";
			}


			$reponse->closeCursor();

		?>


	</head>

	<body>

    <header>
        <div id="titre">
            <div id="logo">
                <img src="../images/ovpf.png" alt="Logo OVPF"/>
                <h1>GETS</h1>
            </div>
        </div>
    </header>
    
    <div id="menu1"></div>

		<center>
			<!-- Formulaire pour ajouter une fiche d'étalonnage dans la BDD -->
			<form action="ajouter_fiche_traitement.php" id="ajout" method="POST">
				<h1>NOUVEL ETALONNAGE</h1>
				<input type="hidden" name="idfiche_etalonnage">
				<p>Méthode</p>
				<input type="text" name="methode">
				<br>
				<p>Gain</p>
				<input type="text" name="gain">
        		<br>
				<p>Coefficient de linéarisation A0</p> 
				<input type="text" name="A0">
				<br>
				<p>Coefficient de linéarisation A1</p> 
				<input type="text" name="A1">
				<br>
				<p>Coefficient de linéarisation A2</p> 
				<input type="text" name="A2">
				<br>
				<p>Coefficient de linéarisation A3</p> 
				<input type="text" name="A3">
				<br>
				<p>Coefficient de linéarisation A4</p> 
				<input type="text" name="A4">
				<br>
				<p>Opérateur</p> 
				<input type="text" name="operateur">
				<br>
				<p>Capteur à étalonner</p> 
				<select name="Capteurs_idCapteur" style="width:40%;">
		            <?php echo $options;?>
        		</select>
        		<br><br><br>
				<input type="submit" value="AJOUTER LA FICHE D'ETALONNAGE" style="width: 70%;">
				<br/>
			</form>
			<br/><br/>
		</center>
</html>