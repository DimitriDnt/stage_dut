<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <title>Accueil</title>
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <script src="../js/jquery.js"></script>
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
</head>

<body>
    <header>
    <div id="titre">
        <div id="logo">
            <img src="../images/ovpf.png" alt="Logo OVPF"/>
            <h1>GETS</h1>
        </div>
    </div>

    <?php include("../menus/menu_user.html"); ?>

    <br/>
    <center>
        <h1>Bienvenue Utilisateur !</h1>
    </center>
    <footer>
        <p>OVPF | Geochimical Easily Transported System (GETS)</p>
    </footer>
</body>

</html>