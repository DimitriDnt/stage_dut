<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <title>Accueil</title>
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <script src="../js/jquery.js"></script>
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>
</head>

<body>
    <header>
        <div id="titre">
            <div id="logo">
                <a href="../php/admin_page.php"><img src="../images/ovpf.png" alt="Logo OVPF" /></a>
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <div id="menu1"></div>

    <br/>
    <center>
        <h1>Bienvenue Administrateur !</h1>
    </center>
    <footer class="page-footer font-small blue">
        <p>OVPF | Geochimical Easily Transported System (GETS)</p>
    </footer>
</body>

</html>