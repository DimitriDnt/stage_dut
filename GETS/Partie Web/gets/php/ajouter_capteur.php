<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <title>Ajout d'un capteur</title>
    <script src="../js/jquery.js"></script>
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>
    <?php

        // Importation du fichier de connexion à la Base De Données
        include('../bdd/connect.php');

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idtype_de_capteur, type, période FROM `Type_de_capteur`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $options = "";

        // Stockage des données récupérées dans une liste
        while($row = $reponse->fetch())
        {
            $options = $options."<option>$row[0] - $row[1]</option>";
        }

        $reponse->closeCursor();

    ?>
</head>

<body>

    <header>
        <div id="titre">
            <div id="logo">
                <img src="../images/ovpf.png" alt="Logo OVPF" />
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <div id="menu1"></div>
    
    <br/>

    <center>
        <!-- Formulaire pour ajouter un capteur dans la BDD -->
        <form action="ajouter_capteur_traitement.php" id="ajout" method="POST">
            <h1>NOUVEAU CAPTEUR</h1>
            <br />
            <input type="hidden" name="idCapteur">

            <p>Nom</p>
            <input type="text" name="Nom" required>
            <br>
            <br>

            <p>Fabriquant</p>
            <input type="text" name="fabriquant">
            <br>
            <br>

            <p>Date de fin de service</p>
            <input type="date" name="date_de_fin_de_service">
            <br>
            <br>

            <p>Référence constructeur</p>
            <input type="text" name="reference_constructeur">
            <br>
            <br>

            <p>Type de capteur</p>
            <select name="type_de_capteur_idtype_de_capteur" style="width:40%;" required>
                <?php echo $options;?>
            </select>
            <br>
            <br>
            <br>

            <input type="hidden" name="etat" value="Libre">
            <input type="submit" value="AJOUTER LE CAPTEUR" style="width: 80%;">
            <br>
        </form>

    </center>

</html>