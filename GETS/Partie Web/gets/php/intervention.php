<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <title>Table d'interventions</title>
    <script src="../js/jquery.js"></script>
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>
    <?php include('../bdd/connect.php'); ?>

        <?php

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idGet, lieux_affectation FROM `Stations_GETS`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $options = "";

        while($row = $reponse->fetch())
        {
            $options = $options."<option>$row[0]</option>";
        }

        $reponse->closeCursor();

     ?>

</head>

<body>

    <header>
        <div id="titre">
            <div id="logo">
                <img src="../images/ovpf.png" alt="Logo OVPF" />
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <div id="menu1"></div>

    <center>

    <table>
        
                    <?php

                            echo 
                            '<tr>
                            <td><b>ID</b></td>
                            <td><b>Description</b></td>
                            <td><b>Intervenant</b></td>
                            <td><b>Numéro de la station</b></td>
                            </tr>';

                            $sql = ("SELECT `idintervention`, `description`, `intervenant`, `Stations_GETS_idGet` FROM Interventions");

                            $reponse = $bdd->query($sql);

                            // On affiche les données contenues dans la table materiel

                            while ($donnees = $reponse->fetch())
                                
                                {
                                    $i = $donnees["idintervention"];
                                    $d = $donnees["description"];
                                    $o = $donnees["intervenant"];
                                    $s = $donnees["Stations_GETS_idGet"];

                                    echo
                                    "<tr><td>".$donnees["idintervention"].
                                    "</td><td>".$donnees["description"].
                                    "</td><td>".$donnees["intervenant"].
                                    "</td><td> GETS ".$donnees["Stations_GETS_idGet"].
                                    "</td></tr>".
                                    '<br>';

                                }

                            $reponse->closeCursor(); // Permet de terminer la requête en cours
                        ?>

    </table>

</center>
    <br/>

    <center>

        <form action="intervention_traitement.php" id="ajout" method="POST">
            <h1>NOUVELLE INTERVENTION</h1>
            <br/>
            <input type="hidden" name="idintervention">
            <p>Description de l'intervention</p>
            <textarea rows="4" cols="30" name="description" maxlength="255"></textarea>
            <br>
            <br>
            <p>Intervenant</p>
            <input type="text" name="intervenant">
            <br>
            <br>
            <p>Configuration JSON</p>
            <textarea rows="5" cols="30" name="configuration_JSON"></textarea>
            <br>
            <br>
            <p>Numéro de la station</p>
            <select name="Stations_GETS_idGet" style="width:40%;">
                <?php echo $options;?>
            </select>
            <br>
            <br>
            <input type="submit" value="Ajouter">
            <br>
            <input type = "button" value = "Retour"  onclick = "history.back()">
            <br/>
        </form>

        <br>
    </center>

</html>