<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <title>Nouvel étalonnage</title>
    <script src="../js/jquery.js"></script>
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>
    <?php include('../bdd/connect.php'); ?>

        <?php

            /********* REQUETE POUR LA FICHE D'ETALONNAGE *********/

            $sql = "SELECT idfiche_etalonnage FROM `Fiche_etalonnages`";

            // On execute la requête
            $reponse = $bdd->query($sql);

            $fiche = "";

            while($row = $reponse->fetch())
            {
                $fiche = $fiche."<option>$row[0]</option>";
            }

            $reponse->closeCursor();

            /********* REQUETE POUR LE NUMERO DE LA STATION *********/

            // Requete SQL pour sélectionner les champs dans la base
            $sql1 = "SELECT idGet FROM `Stations_GETS`";

            // On execute la requête
            $reponse = $bdd->query($sql1);

            $station = "";

            // Tant que la requête est exécutée
            while($row = $reponse->fetch())
            {
                $station = $station."<option>$row[0]</option>";
            }

            $reponse->closeCursor();

            /********* REQUETE POUR LE NUMERO DU CAPTEUR *********/

            $sql2 = "SELECT idCapteur, nom FROM `Capteurs`";

            // On execute la requête
            $reponse = $bdd->query($sql2);

            $capteur = "";

            // Tant que la requête est exécutée
            while($row = $reponse->fetch())
            {
                $capteur = $capteur."<option>$row[0] - $row[1]</option>";
            }

            $reponse->closeCursor();

        ?>

</head>

<body>

    <header>
        <div id="titre">
            <div id="logo">
                <img src="../images/ovpf.png" alt="Logo OVPF" />
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <div id="menu1"></div>
    
    <br/>

    <center>

        <form action="etalonner_capteur_traitement.php" id="ajout" method="POST">
            <h1>NOUVEL ETALONNAGE</h1>
            <br />
            <input type="hidden" name="idCapteur_étalonné">
            <br>
            <br>

            <p>Fiche d'étalonnage</p>
            <select name="fiche_etalonnages_idfiche_etalonnage" style="width:40%;" required>
                <?php echo $fiche;?>
            </select>
            <br>
            <br>
            <br>

            <p>Capteur à étalonner</p>
            <select name="Capteurs_idCapteur" style="width:40%;" required>
                <?php echo $capteur;?>
            </select>
            <br>
            <br>
            <br>

            <p>Station d'affectation</p>
            <select name="Stations_GETS_idGet" style="width:40%;" required>
                <?php echo $station;?>
            </select>
            <br>
            <br>
            <br>

            <input type="submit" value="ETALONNER LE CAPTEUR" style="width: 80%;">
            <br>
        </form>

    </center>

</html>