<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/login.css" />
        <title>Connexion</title>
        <?php include("../bdd/connect.php");?>
    </head>

    <body>
        <div id="container">
            <form action="check.php" method="POST">
                <h1><b>Connexion<b></h1>
                <hr>
                <br>
                <b>Nom d'utilisateur</b>
                <input type="text" placeholder="Entrez votre nom d'utilisateur..." name="nom" required>
                <br><br>
                <b>Mot de passe</b>
                <input type="password" placeholder="Entrez votre mot de passe..." name="mdp" required>
                <br><br>
                <center><b><input type="submit" value="Se connecter"></b></center>
            </form>
        </div>
        <center>
        <div id="error">
            <?php
                if ( isset($_GET['auth']) == ('err'))
                {
                     echo "<b><p>Nom d'utilisateur ou mot de passe incorrect !</p></b>";
                } 
            ?>
        </div>
        </center>
    </body>

</html>