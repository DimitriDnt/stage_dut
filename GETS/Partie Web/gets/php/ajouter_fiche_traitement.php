<!DOCTYPE html>
<html>
	<head>
		<title>Traitement</title>
	</head>

	<body>
		<center>
			<?php

				// Importation du fichier de connexion à la BDD
				include('../bdd/connect.php');

				// On execute la requête pour ajouter un matériel et on ajoute les champs saisies dans la base de données

				$requete = $bdd->prepare('INSERT INTO Fiche_etalonnages (idfiche_etalonnage, methode, gain, A0, A1, A2, A3, A4, operateur, Capteurs_idCapteur) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
				$requete->execute(array($_POST['idfiche_etalonnage'], $_POST['methode'], $_POST['gain'], $_POST['A0'], $_POST['A1'], $_POST['A2'], $_POST['A3'], $_POST['A4'], $_POST['operateur'], $_POST['Capteurs_idCapteur']));
 
				// Test si la requête a bien fonctionné
				if ($requete)
					{
						header('Location: ../php/conf_capteurs.php');

					}

				// On envoie un message d'erreur dans le cas contraire
					
				else
				{
					echo("Echec de l'ajout !");
				}
			?>

			<br>
			<a href="../php/capteurs.php">Retour à la page de configuration</a>

		</center>
	</body>

</html>