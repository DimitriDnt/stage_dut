<!DOCTYPE html>
<html>
	<head>
		<title>Traitement</title>
	</head>

	<body>
		<center>

			<?php
			
				// Importation du fichier de connexion à la Base De Données
				include('../bdd/connect.php');

				// On execute la requête pour ajouter un matériel et on ajoute les champs saisies dans la base de données

				$requete = $bdd->prepare('INSERT INTO Capteurs (idCapteur, Nom, fabriquant, date_de_fin_de_service, reference_constructeur, type_de_capteur_idtype_de_capteur, etat) VALUES (?, ?, ?, ?, ?, ?, ?)');

				$requete->execute(array($_POST['idCapteur'], $_POST['Nom'], $_POST['fabriquant'], $_POST['date_de_fin_de_service'], $_POST['reference_constructeur'], $_POST['type_de_capteur_idtype_de_capteur'], $_POST['etat']));
 
				// Test si la requête a bien fonctionné

				if ($requete)
					{
						header('Location: ../php/conf_capteurs.php');

					}

				// On envoie un message d'erreur dans le cas contraire
					
				else
				{
					echo("Echec de l'ajout !");
				}
			?>

			<br>
			<a href="../php/conf_capteurs.php">Retour à la page de configuration</a>

		</center>
	</body>

</html>