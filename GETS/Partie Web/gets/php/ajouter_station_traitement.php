<!DOCTYPE html>
<html>
	<head>
		<title>Traitement</title>
	</head>

	<body>
		<center>

			<?php

				// Importation du fichier de connexion à la BDD
        		include('../bdd/connect.php');
        		
				// On execute la requête pour ajouter un matériel et on ajoute les champs saisies dans la base de données

				$requete = $bdd->prepare('INSERT INTO Stations_GETS (idGet, nom, lieux_affectation, date_de_destruction, Configuration_communication_idConfiguration) VALUES 
					(?, ?, ?, ?, ?)');
				$requete->execute(array($_POST['idGet'], $_POST['nom'], $_POST['lieux_affectation'], $_POST['date_de_destruction'], $_POST['Configuration_communication_idConfiguration']));
 
				// Test si la requête a bien fonctionné

				if ($requete)
					{
						header('Location: ../php/conf_capteurs.php');

					}

				// On envoie un message d'erreur dans le cas contraire
					
				else
				{
					echo("Echec de l'ajout !");
				}
			?>

			<br>
			<a href="../php/capteurs.php">Retour à la page de configuration</a>

		</center>
	</body>

</html>