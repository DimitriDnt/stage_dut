<!DOCTYPE html>
<html>
	<head>
		<title>Traitement</title>
		<?php include('../bdd/connect.php');?>
	</head>

	<body>
		<center>

			<?php
				// On execute la requête pour ajouter un matériel et on ajoute les champs saisies dans la base de données

				$requete = $bdd->prepare('INSERT INTO Interventions (idintervention, description, intervenant, configuration_JSON, Stations_GETS_idGet) VALUES (?, ?, ?, ?, ?)');
				$requete->execute(array($_POST['idintervention'], $_POST['description'], $_POST['intervenant'], $_POST['configuration_JSON'], $_POST['Stations_GETS_idGet']));
 
				// Test si la requête a bien fonctionné

				if ($requete)
					{
						header('Location: ../php/table_interventions.php');

					}

				// On envoie un message d'erreur dans le cas contraire
					
				else
				{
					echo("Echec de l'ajout !");
				}
			?>

			<br>
			<a href="../php/capteurs.php">Retour à la page de configuration</a>

		</center>
	</body>

</html>