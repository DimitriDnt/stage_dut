<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Réseau</title>
    <!-- Bootstrap -->
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <!-- Importation de jQuery -->
    <script src="../js/jquery.js"></script>
    <!-- Importation des plugins et scripts -->
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/reseau.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>
    <?php include('../bdd/connect.php');?>

        <?php

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idGet, lieux_affectation FROM `Stations_GETS`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $options = "";

        // Tant que la requête est exécutée
        while($row = $reponse->fetch())
        {
            $options = $options."<option>GETS $row[0]</option>";
        }

        $reponse->closeCursor();

     ?>

</head>

       <body>

        <header>
        <div id="titre">
            <div id="logo">
                <a href="../php/admin_page.php"><img src="../images/ovpf.png" alt="Logo OVPF" /></a>
                <h1>GETS</h1>
            </div>
        </div>
    </header>

    <div id="menu1"></div>

    <center>
    <div id="confirm">
         <div class="message"></div>
         <br/>
         <button class="bdd">Lecture BDD</button>
         <button class="ws">Lecture WS</button>
      </div>
</center>
    <br/>
    <div id="selection">
        <center>
            <b><p>Sélectionnez la station GETS à configurer :</p></b>
            <select id="input" style="width: 30%;">
                <option value=""></option>
                <?php echo $options;?>
            </select>
            <button type="button" id="add" onclick="window.location.href='../php/ajouter_station.php'">+</button>
        </center>
        </div>
        
<div id="box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div id="config">
                        <h3>CONFIGURATION LORA</h3>
                        <form>
                            <center>
                                    <p>BAND</p>
                                    <input type="text" id="band" maxlength="50" disabled />
                                    <br/>
                                    <br/>
                                    <p>LOCAL ADDRESS</p>
                                    <input type="text" id="local" disabled />
                                    <br/>
                                    <br/>
                                    <p>INTERVAL</p>
                                    <input type="text" id="interval" maxlength="15" disabled />
                                    <br/>
                                    <br/>
                            </center>
                    </form>

            </div>
        </div>

            <div class="col-md-4 col-sm-4">
                <div id="config">
                        <h3>CONFIGURATION WI-FI</h3>
                        <form>
                            <center>
                                    <p>SSID</p>
                                    <input type="text" id="ssid" maxlength="50" placeholder="Nom du réseau" disabled />
                                    <br/>
                                    <br/>
                                    <p>MOT DE PASSE</p>
                                    <input type="password" id="pwd" placeholder="Mot de passe" disabled />
                                    <br/>
                                    <br/>
                                    <p>PORT WS</p>
                                    <input type="text" id="ws" maxlength="15" placeholder="Web Socket" disabled />
                                    <br/>
                                    <br/>
                                    <p>PORT TELNET</p>
                                    <input type="text" id="telnet" maxlength="15" placeholder="Telnet" disabled />
                                    <br/>
                                    <br/>
                            </center>
                            </form>
                        </div>
                        
            </div>

            <div class="col-md-4 col-sm-4">
                <div id="config">
                        <h3>CONFIGURATION IP</h3>
                        <form>
                            <center>
                                    <p>MODE</p>
                                    <input type="text" id="mode" maxlength="50" placeholder="DHCP / Static" disabled />
                                    <br/>
                                    <br/>
                                    <p>ADRESSE IP</p>
                                    <input type="text" id="adresse" placeholder="Adresse IPv4" disabled />
                                    <br/>
                                    <br/>
                                    <p>MASQUE</p>
                                    <input type="text" id="masque" maxlength="15" placeholder="Masque de sous-réseau" disabled />
                                    <br/>
                                    <br/>
                                    <p>PASSERELLE</p>
                                    <input type="text" id="passerelle" maxlength="15" placeholder="Passerelle par défaut" disabled />
                                    <br/>
                                    <br/>
                                    <p>DNS</p>
                                    <input type="text" id="dns" maxlength="15" placeholder="Adresse du DNS" disabled />
                                    <br/>
                                    <br/>
                                    <p>NTP</p>
                                    <input type="text" id="ntp" maxlength="15" placeholder="Adresse de NTP" disabled />
                                    <br/>
                                    <br/>
                            </center>
                            </form>
                        </div>
                    </div>
                        
    </div>

<br><br>
<center>
    <button type="button" id="save" class="button">SAUVEGARDER</button>
</center>

</div>
</div>
</body>
</html>