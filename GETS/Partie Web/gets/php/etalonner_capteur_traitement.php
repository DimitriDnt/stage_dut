<!DOCTYPE html>
<html>
	<head>
		<title>Traitement</title>
		<?php include('../bdd/connect.php');?>
	</head>

	<body>
		<center>

			<?php
				// On execute la requête pour ajouter un matériel et on ajoute les champs saisies dans la base de données

				$requete = $bdd->prepare('INSERT INTO Capteur_étalonné (idCapteur_étalonné, fiche_etalonnages_idfiche_etalonnage, Capteurs_idCapteur, Stations_GETS_idGet) VALUES (?, ?, ?, ?)');

				$requete->execute(array($_POST['idCapteur_étalonné'], $_POST['fiche_etalonnages_idfiche_etalonnage'], $_POST['Capteurs_idCapteur'], $_POST['Stations_GETS_idGet']));
 
				// Test si la requête a bien fonctionné

				if ($requete)
					{
						header('Location: ../php/conf_capteurs.php');

					}

				// On envoie un message d'erreur dans le cas contraire
					
				else
				{
					echo("Echec de l'ajout !");
				}
			?>

			<br>
			<a href="../php/conf_capteurs.php">Retour à la page de configuration</a>

		</center>
	</body>

</html>