<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Capteurs</title>
    <!-- Bootstrap -->
    <link rel="icon" href="../images/ovpf.ico" />
    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <!-- Importation de jQuery -->
    <script src="../js/jquery.js"></script>
    <!-- Importation des plugins et scripts -->
    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/capteurs.js"></script>
    <script type="text/javascript" src="../js/menus.js"></script>
    <?php include('../bdd/connect.php');?>

     <?php

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idGet, lieux_affectation FROM `Stations_GETS`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $options = "";

        // Tant que la requête est exécutée
        while($row = $reponse->fetch())
        {
            $options = $options."<option>GETS $row[0]</option>";
        }

        $reponse->closeCursor();

     ?>

     <?php 
     
        // On sélectionne uniquement les capteurs de type électrochimiques et étant non utilisés
        $sql = ("SELECT `idCapteur`, `nom` FROM Capteurs WHERE  type_de_capteur_idtype_de_capteur = '1' AND etat='Libre'");

        // On execute la requête
        $reponse = $bdd->query($sql);

        $capteurs = "";

        // Tant que la requête est exécutée
        while($row = $reponse->fetch())
        {
            $capteurs = $capteurs."<option>$row[1]</option>";
        }

        $reponse->closeCursor();
        
     ?>
</head>

<body>
    <header>
        <div id="titre">
            <div id="logo">
                <a href="../php/admin_page.php"><img src="../images/ovpf.png" alt="Logo OVPF" /></a>
                <h1>GETS</h1>
            </div>
        </div>
    </header>
      
    <div id="menu1"></div>
    
    <center>
            <button type="button" id="reset" class="button">RESET</button>
        </center>
    </div>
    </div>
    </center>
    </div>
    <footer style="top: 50px;">
        <p>OVPF | Geochemical Easily Transported System (GETS)</p>
    </footer>
</body>

</html>
