<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link rel="icon" href="../images/ovpf.ico" />
	    <link rel="stylesheet" type="text/css" href="../css/style.css" />
	    <link rel="stylesheet" href="../librairies/bootstrap/bootstrap.min.css">
	    <link rel="stylesheet" href="../librairies/bootstrap/custom.css">
	    <title>Modifier intervention</title>
	    <script src="../js/jquery.js"></script>
	    <script src="../librairies/bootstrap/bootstrap.min.js"></script>
	    <script type="text/javascript" src="../js/menus.js"></script>
		<?php include('../bdd/connect.php');?>

		<?php

        // Requete SQL pour sélectionner les champs dans la base
        $sql = "SELECT idGet, lieux_affectation FROM `Stations_GETS`";

        // On execute la requête
        $reponse = $bdd->query($sql);

        $options = "";

        while($row = $reponse->fetch())
        {
            $options = $options."<option>GETS $row[0]</option>";
        }

        $reponse->closeCursor();

        $requete = "SELECT idintervention FROM `Interventions`";

        $test = $bdd->query($requete);

        $id = "";

        while($row = $test->fetch())
        {
            $id = $id."<option>$row[0]</option>";
        }

        $test->closeCursor();
     ?>

	</head>

<body>

		<center>

			<form action="modifier_traitement.php" id="ajout" method="POST">
            <h1>MODIFICATION DE L'INTERVENTION</h1>
            <br/>
            <p>ID intervention</p>
            <select type="hidden" name="idintervention" style="width:40%;">
                <?php echo $id;?>
            </select>
            <br/>
            <br/>
            <p>Description de l'intervention</p>
            <textarea rows="4" cols="30" name="description" maxlength="255"></textarea>
            <br>
            <br>
            <input type="submit" value="Modifier">
            <br>
            <input type = "button" value = "Retour"  onclick = "history.back()">
            <br/>
        </form>

		</center>
</html>