################################### -*- coding: utf-8 -*- #############################

#!/usr/bin/python

import MySQLdb
global cursor
global db

# Fonction pour se connecter à la base de données
def connexionBDD():
     global cursor
     global db

     # Connexion à la base de données
     try:
         db = MySQLdb.connect("localhost", "root", "admin", "ovpf")

     except IOError:
                 print("Erreur de connexion.")

     cursor = db.cursor()

     cursor.execute("SELECT VERSION()")

     data = cursor.fetchone()

     print ("Version de la base : ", data)
     return

# Fonction pour créer les tables dans la base de données
def creationTable():
     global cursor
     global db

     # Efface la table si elle existe déjà
     cursor.execute("DROP TABLE IF EXISTS admins")

     # Creation de la table admins
     sql1 = """CREATE TABLE IF NOT EXISTS admins (
              id_admin INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
              login_admin CHAR(20) NOT NULL UNIQUE KEY,
	      pwd_admin TINYTEXT NOT NULL
              )"""

     # Creation de la table users
     sql2 = """CREATE TABLE IF NOT EXISTS users (
              id_user INT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
              login_user CHAR(20) NOT NULL UNIQUE KEY,
              pwd_user TINYTEXT  NOT NULL
              )"""

     # Affichage des requêtes SQL
     print(sql1)
     print(sql2)

     try:
             # Execution des requetes SQL
             cursor.execute(sql1)
	     cursor.execute(sql2)

             # Appliquer les modifications dans la base de données
             db.commit()

     except:
             # En cas d'erreur, réinitialiser la base comme elle était
             db.rollback()
     return

# Programme principal - Appel des fonctions
connexionBDD()

creationTable()

