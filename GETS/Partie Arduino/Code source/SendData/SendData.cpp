#include <ArduinoJson.h>
#include "./SendData.h"
/*
   %d = signed integer               %f = floating point number
   %s = string                     %.1f = float to 1 decimal place
   %c = character                  %.3f = float to 3 decimal places
   %e = scientific notation          %g = shortest representation of %e or %f
   %u = unsigned integer             %o = unsigned octal
   %x = unsigned hex (lowercase)     %X = unsigned hex (uppercase)
   %hd = short int                  %ld = long int
   %lld = long long int
   =============================================================================  */
Data::savetoSD(conf parametre)
{ char NaN[3] = "NaN";
  String ligne = "";
  String info_capteur;
  if (parametre.config.GPS.enable)
  {
    sprintf(info_capteur, "%c \t%c \t%c \t%c  \t%c \t%c \t%c \t%c \t%c \t%c "  , Data.GPS.date, Data.GPS.time, Data.GPS.status, Data.GPS.latitud, Data.GPS.latitudHemisphere, Data.GPS.longitud, Data.GPS.longitudMeridiano, Data.GPS.speedKnots , Data.GPS.trackAngle, Data.GPS.magneticVariation, Data.GPS.magneticVariationOrientation);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c "  , Data.RTC.date, Data.RTC.time);
  }
  ligne = +info_capteur;


  if (parametre.config.ADS1.enable)
  {
    sprintf(info_capteur, "%d \t%d \t%d \t%d "  , Data.ADS1.capteur1.count, Data.ADS1.capteur2.count, Data.ADS1.capteur3.count, Data.ADS1.capteur4.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c \t%c \t%c "  , NaN, NaN, NaN, NaN);
  }
  ligne = +info_capteur;


  if (parametre.config.ADS2.enable)
  {
    sprintf(info_capteur, "%d \t%d \t%d \t%d "  , Data.ADS2.capteur1.count, Data.ADS2.capteur2.count, Data.ADS2.capteur3.count, Data.ADS2.capteur4.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c \t%c \t%c "  , NaN, NaN, NaN, NaN);
  }
  ligne = +info_capteur;


  if (parametre.config.MesureUI.enable)
  {
    sprintf(info_capteur, "%d \t%d \t%d "  , Data.MesureUI.capteur1.count, Data.MesureUI.capteur2.count, Data.MesureUI.capteur3.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c \t%c "  , NaN, NaN, NaN);
  }
  ligne = +info_capteur;


  if (parametre.config.Qair.enable)
  {
    sprintf(info_capteur, "%d \t%d "  , Data.Qair.capteur1.count, Data.Qair.capteur2.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c "  , NaN, NaN);
  }
  ligne = +info_capteur;


  if (parametre.config.CO2_1.enable)
  {
    sprintf(info_capteur, "%d \t%d \t%d "  , Data.CO2_1.capteur1.count, Data.CO2_1.capteur2.count, Data.CO2_1.capteur3.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c \t%c "  , NaN, NaN, NaN);
  }
  ligne = +info_capteur;


  if (parametre.config.CO2_2.enable)
  {
    sprintf(info_capteur, "%d \t%d \t%d "  , Data.CO2_2.capteur1.count, Data.CO2_2.capteur2.count, Data.CO2_2.capteur3.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c \t%c "  , NaN, NaN, NaN);
  }
  ligne = +info_capteur;

  if (parametre.config.Temp.enable)
  {
    sprintf(info_capteur, "%d \t "  , Data.Temp.capteur1.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t"  , NaN;
  }
  ligne = +info_capteur;


  if (parametre.config.BARO.enable)
  {
    sprintf(info_capteur, "%d \t%d \t%d \t%d "  , Data.BARO.capteur1.count, Data.BARO.capteur2.count, Data.BARO.capteur3.count, Data.BARO.capteur4.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c \t%c \t%c "  , NaN, NaN, NaN, NaN);
  }
  ligne = +info_capteur;


  if (parametre.config.CPart_1.enable)
  {
    sprintf(info_capteur, "%d \t%d "  , Data.CPart_1.capteur1.count, Data.CPart_1.capteur2.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c "  , NaN, NaN);
  }
  ligne = +info_capteur;


  if (parametre.config.CPart_2.enable)
  {
    sprintf(info_capteur, "%d \t%d \t%d \t%d "  , Data.CPart_2.capteur1.count, Data.CPart_2.capteur2.count, Data.CPart_2.capteur3.count, Data.CPart_2.capteur4.count);
  }
  else
  {
    sprintf(info_capteur, "%c \t%c \t%c \t%c "  , NaN, NaN, NaN, NaN);
  }
  ligne = +info_capteur;

         )


};

Data::sendToLora(conf parametre)
{
};
Data::sendToWS(conf parametre)
{
  DynamicJsonDocument jsonBufferOut(600);
  if (parametre.config.GPS.enable)
  {
    jsonBufferOut["GPS"][date] == Data.GPS.date;
    jsonBufferOut["GPS"][time] == Data.GPS.time;
    jsonBufferOut["GPS"][status] == Data.GPS.status;
    jsonBufferOut["GPS"][latitud] == Data.GPS.latitud;
    jsonBufferOut["GPS"][latitudHemisphere] == Data.GPS.latitudHemisphere;
    jsonBufferOut["GPS"][longitud] == Data.GPS.longitud;
    jsonBufferOut["GPS"][longitudMeridiano] == Data.GPS.longitudMeridiano;
    jsonBufferOut["GPS"][speedKnots] == Data.GPS.speedKnots;
    jsonBufferOut["GPS"][trackAngle] == Data.GPS.trackAngle;
    jsonBufferOut["GPS"][magneticVariation] == Data.GPS.magneticVariation;
    jsonBufferOut["GPS"][magneticVariationOrientation] == Data.GPS.magneticVariationOrientation;
  }
  else
  {
    jsonBufferOut["GPS"][date] == NaN;
    jsonBufferOut["GPS"][time] == NaN;
    jsonBufferOut["GPS"][status] == NaN;
    jsonBufferOut["GPS"][latitud] == NaN;
    jsonBufferOut["GPS"][latitudHemisphere] == NaN;
    jsonBufferOut["GPS"][longitud] == NaN;
    jsonBufferOut["GPS"][longitudMeridiano] == NaN;
    jsonBufferOut["GPS"][speedKnots] == NaN;
    jsonBufferOut["GPS"][trackAngle] == NaN;
    jsonBufferOut["GPS"][magneticVariation] == NaN;
    jsonBufferOut["GPS"][magneticVariationOrientation] == NaN;

  }

  if (parametre.config.ADS1.enable)
  {
    jsonBufferOut["ADS1"][capteur1] == Data.ADS1.capteur1.count;
    jsonBufferOut["ADS1"][capteur2] == Data.ADS1.capteur2.count;
    jsonBufferOut["ADS1"][capteur3] == Data.ADS1.capteur3.count;
    jsonBufferOut["ADS1"][capteur4] == Data.ADS1.capteur4.count;
  }
  else
  {
    jsonBufferOut["ADS1"][capteur1] == NaN;
    jsonBufferOut["ADS1"][capteur2] == NaN;
    jsonBufferOut["ADS1"][capteur3] == NaN;
    jsonBufferOut["ADS1"][capteur4] == NaN;

  }

  if (parametre.config.ADS2.enable)
  {
    jsonBufferOut["ADS2"][capteur1] == Data.ADS2.capteur1.count;
    jsonBufferOut["ADS2"][capteur2] == Data.ADS2.capteur2.count;
    jsonBufferOut["ADS2"][capteur3] == Data.ADS2.capteur3.count;
    jsonBufferOut["ADS2"][capteur4] == Data.ADS2.capteur4.count;
  }
  else
  {
    jsonBufferOut["ADS2"][capteur1] == NaN;
    jsonBufferOut["ADS2"][capteur2] == NaN;
    jsonBufferOut["ADS2"][capteur3] == NaN;
    jsonBufferOut["ADS2"][capteur4] == NaN;

  }

if (parametre.config.MesureUI.enable)
  {
    jsonBufferOut["MesureUI"][capteur1] == Data.MesureUI.capteur1.count;
    jsonBufferOut["MesureUI"][capteur2] == Data.MesureUI.capteur2.count;
    jsonBufferOut["MesureUI"][capteur3] == Data.MesureUI.capteur3.count;
  }
  else
  {
    jsonBufferOut["MesureUI"][capteur1] == NaN;
    jsonBufferOut["MesureUI"][capteur2] == NaN;
    jsonBufferOut["MesureUI"][capteur3] == NaN;

  }

if (parametre.config.Qair.enable)
  {
    jsonBufferOut["Qair"][capteur1] == Data.Qair.capteur1.count;
    jsonBufferOut["Qair"][capteur2] == Data.Qair.capteur2.count;
  }
  else
  {
    jsonBufferOut["Qair"][capteur1] == NaN;
    jsonBufferOut["Qair"][capteur2] == NaN;

  }

if (parametre.config.CO2_1.enable)
  {
    jsonBufferOut["CO2_1"][capteur1] == Data.CO2_1.capteur1.count;
    jsonBufferOut["CO2_1"][capteur2] == Data.CO2_1.capteur2.count;
    jsonBufferOut["CO2_1"][capteur3] == Data.CO2_1.capteur3.count;
  }
  else
  {
    jsonBufferOut["CO2_1"][capteur1] == NaN;
    jsonBufferOut["CO2_1"][capteur2] == NaN;
    jsonBufferOut["CO2_1"][capteur3] == NaN;

  }

  if (parametre.config.CO2_2.enable)
  {
    jsonBufferOut["CO2_2"][capteur1] == Data.CO2_2.capteur1.count;
    jsonBufferOut["CO2_2"][capteur2] == Data.CO2_2.capteur2.count;
  }
  else
  {
    jsonBufferOut["CO2_2"][capteur1] == NaN;
    jsonBufferOut["CO2_2"][capteur2] == NaN;

  }

if (parametre.config.Temp.enable)
  {
    jsonBufferOut["Temp"][capteur1] == Data.Temp.capteur1.count;
  }
  else
  {
    jsonBufferOut["Temp"][capteur1] == NaN;

  }
  
if (parametre.config.BARO.enable)
  {
    jsonBufferOut["BARO"][capteur1] == Data.BARO.capteur1.count;
    jsonBufferOut["BARO"][capteur2] == Data.BARO.capteur2.count;
    jsonBufferOut["BARO"][capteur3] == Data.BARO.capteur3.count;
    jsonBufferOut["BARO"][capteur4] == Data.BARO.capteur4.count;
  }
  else
  {
    jsonBufferOut["BARO"][capteur1] == NaN;
    jsonBufferOut["BARO"][capteur2] == NaN;
    jsonBufferOut["BARO"][capteur3] == NaN;
    jsonBufferOut["BARO"][capteur4] == NaN;

  }

if (parametre.config.CPart_1.enable)
  {
    jsonBufferOut["CPart_1"][capteur1] == Data.CPart_1.capteur1.count;
    jsonBufferOut["CPart_1"][capteur2] == Data.CPart_1.capteur2.count;
  }
  else
  {
    jsonBufferOut["CPart_1"][capteur1] == NaN;
    jsonBufferOut["CPart_1"][capteur2] == NaN;

  }

if (parametre.config.CPart_2.enable)
  {
    jsonBufferOut["CPart_2"][capteur1] == Data.CPart_2.capteur1.count;
    jsonBufferOut["CPart_2"][capteur2] == Data.CPart_2.capteur2.count;
    jsonBufferOut["CPart_2"][capteur3] == Data.CPart_2.capteur3.count;
    jsonBufferOut["CPart_2"][capteur4] == Data.CPart_2.capteur4.count;
  }
  else
  {
    jsonBufferOut["CPart_2"][capteur1] == NaN;
    jsonBufferOut["CPart_2"][capteur2] == NaN;
    jsonBufferOut["CPart_2"][capteur3] == NaN;
    jsonBufferOut["CPart_2"][capteur3] == NaN;

  }
  
  for (int cpt = 0; cpt < 4; cpt++)
  {
    if (MesureEnable[cpt])
    {
      jsonBufferOut[NomCapteur[cpt]]["Temperature"] = String((float)temperature[cpt], 0);
      jsonBufferOut[NomCapteur[cpt]]["Hydrometrie"] = String((float)humidity[cpt], 0);

    }
  }

};
