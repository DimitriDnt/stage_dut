#include "./conf.h"
struct D_capteur
{
    
    float Value;     
    int count;
    
   
};
struct D_ADS
{ 
  D_capteur capteur1;
  D_capteur capteur2;
  D_capteur capteur3;
  D_capteur capteur4;
};
struct D_param
{
  boolean enable;
};
struct D_GPS
{
char date[10];
char time[10];
char status[3];
char latitud[11];
char latitudHemisphere[3];
char longitud[11];
char longitudMeridiano[3];
char speedKnots[10];
char trackAngle[8];
char magneticVariation[10];
char magneticVariationOrientation[3];


};
struct D_RTC
{
char time[10];
char date[10];

};
struct D_MesureUI
{
  D_capteur capteur1;
  D_capteur capteur2;
  D_capteur capteur3;
}
struct D_Qair
{
  D_capteur capteur1;
  D_capteur capteur2;
};

struct D_CO2_1
{
  D_capteur capteur1;
  D_capteur capteur2;
  D_capteur capteur3;
};

struct D_CO2_2
{
  D_capteur capteur1;
  D_capteur capteur2;
 };
 struct D_Temp
{
  D_capteur capteur1;
};
struct D_BARO
{
  D_capteur capteur1;
  D_capteur capteur2;
  D_capteur capteur3;
  D_capteur capteur4;
};
struct D_CPart_1
{
  D_capteur capteur1;
  D_capteur capteur2;
}; 
struct D_CPart_2
{
  D_capteur capteur1;
  D_capteur capteur2;
  D_capteur capteur3;
  D_capteur capteur4;
};
struct Data
{ D_GPS GPS;
  D_ADS ADS1;
  D_ADS ADS2;
  D_MesureUI MesureUI;
  D_Qair CCS;
  D_Qair CJMCU;
  D_Qair E2V;
  D_CO2_1 CO2_1;
  D_CO2_2 CO2_2;
  D_Temp TEMP;
  D_BARO BARO;
  D_CPart_1 CPart_1;
  D_CPart_2 CPart_2;
  
};

                         
class Data
{
public:
void savetoSD();
void sendToLora();
void sendToWS();
void saveGPS(String nmea);
void saveADS1(uint8_t ads, uint8_t voie,int count) ;
void saveMesureUI(int countU, int countI);
void saveCCS(int mesure1,int mesure2);
void saveCJMCU(int mesure1,int mesure2);
void saveE2V(int mesure1,int mesure2);
void saveCO2_1(int mesure1,int mesure2,int mesure3);
void saveCO2_2(int mesure1,int mesure2);
void saveTEMP(int mesure1);
void saveBARO(int mesure1,int mesure2,int mesure3);
void saveCPart_1(int mesure1,int mesure2);
void saveCPart_2(int mesure1,int mesure2,int mesure3);
  

};
 
