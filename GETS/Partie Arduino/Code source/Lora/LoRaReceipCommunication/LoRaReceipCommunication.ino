#include "heltec.h"
#include <ArduinoJson.h>
#define BAND    433E6  //you can set band here directly,e.g. 868E6,915E6
#include <Crc16.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>
#include "heltec.h"
#include "WiFi.h"
WebSocketsServer webSocket = WebSocketsServer(81);

bool plug = false;
String outgoing;              // outgoing message
byte localAddress = 0xFD;     // address of this device
byte destination = 0xBB;      // destination to send to
byte msgCount = 0;            // count of outgoing messages
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends
String file;
Crc16 crcb;

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {

  switch (type) {
    case WStype_DISCONNECTED:
      plug = false;
      Serial.printf("[%u] Disconnected!\n", num);
      break;

    case WStype_CONNECTED: {
        plug = true;
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        // send message to client
        //webSocket.sendTXT(num, "Connected");

      }
      break;

    case WStype_TEXT:
      Serial.printf("[%u] get Text: %s\n", num, payload);
      if (payload[0] == '#') {
      }
      break;
  }
}
void WIFISetUp(void)
{
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin("IRT", "pitonguanyin");
  delay(100);

  byte count = 0;
  while (WiFi.status() != WL_CONNECTED && count < 10)
  {
    count ++;
    delay(500);
    Serial.println("Connecting...");

  }

  //Heltec.display -> clear();
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("Connecting...OK");

    //delay(500);
  }
  else
  {
    Serial.println("Connecting...Failed");
  }

  return;
}
void setup()
{
  //WIFI Kit series V1 not support Vext control
  //Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.begin(115200);
  //Serial2.begin (9600, SERIAL_8N1, RXD2, TXD2);
  Serial.setDebugOutput(true);
  WIFISetUp();
  Serial.println("Heltec.LoRa Duplex");
  IPAddress myIP = WiFi.localIP();
  Serial.print("Adresse IP du serveur: ");
  Serial.println(myIP);
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

void loop() {
  //webSocket.loop();
  /* if (millis() - lastSendTime > interval){
     //String message = "ESP32";   // send a message
     //sendMessage(message);
     //Serial.println("reception de g et d");
     Heltec.display -> clear();
     Heltec.display -> drawString(0, 0, "Je recois");
     Heltec.display -> display();
     lastSendTime = millis();            // timestamp the message
     interval = random(2000) + 1000;    // 2-3 seconds
     delay(1000);
    }*/

  // parse for a packet, and call onReceive with the result:
  //Serial.println("attente recp");
  /*String texte = "attente recp";
  sendMessage(texte);*/
  onReceive(LoRa.parsePacket());
  webSocket.loop();
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return
  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length
  String incoming;

  while (LoRa.available())
  {
    incoming += (char)LoRa.read();
  }
  /*char charBuf[200];
    incoming.toCharArray(charBuf, 200);*/
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incomingMsgId));
  Serial.println("Message length: " + String(incomingLength));
  Serial.println(incoming);
  if (plug) {
    Serial.println("lancement d'un message");
    /*coor = (String)"{\"x\":\"" + gps.location.lat() + "\",\"y\":\"" + gps.location.lng() + "\"}";
      Serial.println(coor);*/
    //lancement d'un message
    webSocket.sendTXT(0, incoming);
    //delay(500);
  }

  //Serial.println();
  //Serial.println("RSSI: " + String(LoRa.packetRssi()));
  //Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println();
  Heltec.display -> clear();
  Heltec.display -> drawString(1, 1, "Packet de " + String(sender, HEX) + " id " + String(incomingMsgId));
  Heltec.display -> display();

}
void sendMessage(String outgoing) {
  Serial.print("envoi: ");
  Serial.println(outgoing);
  LoRa.beginPacket();                   // début du packet

  LoRa.write(destination);              // ajout de l'adresse de destination
  LoRa.write(localAddress);             // ajout de l'addresse de l'envoyeur
  LoRa.write(msgCount);                 // ajout ID du message
  LoRa.write(outgoing.length());        // ajout de la longueur du payload
  LoRa.print(outgoing);                 // ajout du payload
  LoRa.endPacket();                     // fin du packet et envoie
  Serial.println("test");
  msgCount++;                         // increment message ID
  Serial.println("envoie vers: 0x" + String(destination, HEX));
  Serial.println("depuis: 0x" + String(localAddress, HEX));
  Serial.println("Message ID: " + String(msgCount));
  Serial.println("Message length: " + String(outgoing.length()));
  Serial.println(outgoing);
  Serial.println();
}
