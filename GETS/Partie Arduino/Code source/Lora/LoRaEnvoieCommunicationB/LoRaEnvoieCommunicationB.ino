#include "heltec.h"
#include <Crc16.h>
#define BAND    433E6  //you can set band here directly,e.g. 868E6,915E6
Crc16 crc;


String outgoing;              // outgoing message

byte localAddress = 0xBB;     // address of this device
byte destination = 0xFD;      // destination to send to

byte msgCount = 0;            // count of outgoing messages
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends
byte data[13] = "123456789";
boolean receving = false;
String ack = "dmd ack";
int compteur = 0;
void setup()
{
  //WIFI Kit series V1 not support Vext control
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.println("Heltec.LoRa Duplex");
}

void loop() {
  /*nsigned short value = crc.getCrc(); //création crc depuis byte array
    crc.clearCrc();
    value = crc.XModemCrc(data, 0, sizeof(data) - 4);

    char crcH[4];
    sprintf(crcH, "%04X", value);

    for (int i = 0; i <= 3; i++) {
    data[sizeof(data) - i - 1] = crcH[3 - i];
    }
    String datas = ""; //création varaiable data recu

    //byte array en string
    for (int i = 0; i <= sizeof(data) - 1; i++) {
    //Serial.print(char(data[i]));
    //Serial.print(i);
    datas += char(data[i]);
    }
  */
  if (millis() - lastSendTime > interval)  {

    String message = "ESP32";   // send a message
    /*if (msgCount == 0) {
      sendMessage(message); //premier message envoie normal
      onReceive(LoRa.parsePacket());
      Serial.println("Sending " + message + " id " + msgCount);
      //msgCount++;
      } else if (msgCount != 0) {*/
    //autre message attendre message
    sendMessage(message); //premier message envoie normal
    //delay(500);
    onReceive(LoRa.parsePacket());
    boolean burp = true;
    for (int i = 0; i < 500; i++) {
      delay(1);
      onReceive(LoRa.parsePacket());
      //while (burp = true) {
      //sendMessage(message); //premier message envoie normal
      /*for (int i = 0; i < 500; i++) {
        onReceive(LoRa.parsePacket());
        }*/
      //Serial.println("try");
      if (!receving) {
        //Serial.println("no recp");
        //sendMessage(ack);
        //Serial.println("envoie dmd ack");
        onReceive(LoRa.parsePacket());
        //burp = true;
      }
      if (receving) {
        Serial.println("reception");
        sendMessage(message);
        Serial.println("Sending " + message + " id " + msgCount);
        msgCount++;
        i = 501;
        //burp = false;
      }
    }

    Serial.println("compteur: " + String(compteur));
    compteur++;
    //}
    //Serial.println("Sending " + message + " id " + msgCount);
    //compte= compte +1;
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "J'envoie");
    Heltec.display -> display();
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
    delay(1000);
  }

  // parse for a packet, and call onReceive with the result:
  //onReceive(LoRa.parsePacket());
}

void sendMessage(String outgoing) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  //msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return
  receving = true;
  Serial.println("true");
  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length
  String incoming;

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }
  /*
    if (incomingLength != sizeof(incoming))
    { // check length for error
      Serial.println("error: message length does not match length");
      Serial.println("false");
      receving = false;
      return;                             // skip rest of function
    }

    // if the recipient isn't this device or broadcast,
    if (recipient != localAddress && recipient != 0xFF) {
      Serial.println("This message is not for me.");
      Serial.println("false");
      receving = false;
      return;                             // skip rest of function
    }*/
  //Serial.println("income " + incoming);
  //Serial.println(incoming);
  //msgCount = incoming.toInt() + 1;

  Serial.println("msgCount: " + String(msgCount));
  msgCount++;
  /*
    // if message is for this device, or broadcast, print details:
    Serial.println("Received from: 0x" + String(sender, HEX));
    Serial.println("Sent to: 0x" + String(recipient, HEX));
    Serial.println("Message ID: " + String(incomingMsgId));
    Serial.println("Message length: " + String(incomingLength));*/
  char charBuf[20];
  incoming.toCharArray(charBuf, 20);
  Serial.println("Message: " + incoming);
  /*Serial.println("RSSI: " + String(LoRa.packetRssi()));
    Serial.println("Snr: " + String(LoRa.packetSnr()));
    Serial.println();*/

  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "Packet de " + String(sender, HEX) + " id: " + String(incomingMsgId) + " recu");
  Heltec.display -> display();
  Serial.println("false");
  receving = false;
}
