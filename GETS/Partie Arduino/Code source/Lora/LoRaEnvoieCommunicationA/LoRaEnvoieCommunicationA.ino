#include <heltec.h>
#define BAND    433E6  //you can set band here directly,e.g. 868E6,915E6
#include <Crc16.h>

String outgoing;              // outgoing message
byte localAddress = 0xBB;     // address of this device
byte destination = 0xFD;      // destination to send to
byte msgCount = 0;            // count of outgoing messages
int msgRecv = 0;
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends
String dataTest = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
String info = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
//Crc16 crc;
#define RXD2 23 // 16 est utilisé pour OLED_RST! 
#define TXD2 17
#include <TinyGPS++.h>
TinyGPSPlus gps;
String coor;
String lati;
String lon;
//String trameAenvoyer = fullTrame; //modifier la variable à envoyer
//int limiteCount;

void setup() {
  //WIFI Kit series V1 not support Vext control
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  //Serial.begin(115200);
  Serial.println("Heltec.LoRa Duplex");
  Serial2.begin (9600, SERIAL_8N1, RXD2, TXD2);
}

void loop() {
  sendMessage("test");
  onReceive(LoRa.parsePacket());
  Serial.println(Serial2.available());
  if (millis() - lastSendTime > interval)  {
    //sendMessage("patate");
    while (Serial2.available() > 0) {
      if (gps.encode(Serial2.read()))
        displayInfo();
      //sendMessage(coor);
      //delay(2000);
    }
    if (millis() > 5000 && gps.charsProcessed() < 10) {
      Serial.println(F("No GPS detected: check wiring."));
      while (true);
    }
    //sendMessage(coor);
    //Serial.println("envoie par lora");
    //sendMessage(message);
    //Serial.println("Sending " + message +" id "+ msgCount);
    //compte= compte +1;
    //Serial.println("Sending : ");
    //Serial.println(message);
    /*Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "envoie du message");
      Heltec.display -> display();*/
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
    delay(2000);
  }

  // parse for a packet, and call onReceive with the result:
  //onReceive(LoRa.parsePacket());
}
void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return
  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length
  String incoming;

  while (LoRa.available())
  {
    incoming += (char)LoRa.read();
  }
  /*char charBuf[200];
  incoming.toCharArray(charBuf, 200);*/
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incomingMsgId));
  Serial.println("Message length: " + String(incomingLength));
  Serial.println(incoming);
}
void sendMessage(String outgoing) {
  /*if (msgCount > limiteCount) { // si le numero de message est supérieur à la limite
    msgCount = 0; //on réinitialise le numero de message
    }*/
  LoRa.beginPacket();                   // début du packet
  LoRa.write(destination);              // ajout de l'adresse de destination
  LoRa.write(localAddress);             // ajout de l'addresse de l'envoyeur
  LoRa.write(msgCount);                 // ajout ID du message
  //Serial.print("Envoie de mess par lora : ");
  /*Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "envoie du message ");
    Heltec.display -> display();*/
  //Serial.println(outgoing);
  LoRa.write(outgoing.length());        // ajout de la longueur du payload
  LoRa.print(outgoing);                 // ajout du payload
  LoRa.endPacket();                     // fin du packet et envoie
  msgCount++;                           // increment message ID
  Serial.println("envoie vers: 0x" + String(destination, HEX));
  Serial.println("depuis: 0x" + String(localAddress, HEX));
  Serial.println("Message ID: " + String(msgCount));
  Serial.println("Message length: " + String(outgoing.length()));
  Serial.println(outgoing);
  Serial.println();
}
void displayInfo() {
  Serial.print(F("Location: "));
  if (gps.location.isValid()) {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }  else {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid()) {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  } else {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid()) {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  } else {
    Serial.print(F("INVALID"));
  }
  Serial.println();
  /*
    if (lati != gps.location.lat()) {
      if (lon != gps.location.lng()) {
        lati = (String)gps.location.lat();
        lon = (String)gps.location.lng();
      }
    }*/
  lati = (String)gps.location.lat();
  lon = (String)gps.location.lng();

  if (gps.location.lat() == 0) {
    lati = -21;
  }
  if (gps.location.lng() == 0) {
    lon = 55;
  }
  //Serial.println("lancement d'un message");
  coor = (String)"{\"x\":\"" + lati + "\",\"y\":\"" + lon + "\"}";
  Serial.println(coor);
  //lancement d'un message
  sendMessage(coor);
  //webSocket.sendTXT(0, coor);
  //delay(500);
}
