#include "Arduino.h"
#include "heltec.h"
#include "WiFi.h"
#define BAND    433E6  //you can set band here directly,e.g. 868E6,915E6

//les address doivent être modifié selon les besoins
//les serveur principal à pour adresse 0xFF
String outgoing;              // outgoing message
byte localAddress = 0xFD;     // address of this device
byte destination = 0xBB;      // destination to send to
byte msgCount = 0;            // count of outgoing messages
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends

void setup() {
  Heltec.begin(true /*DisplayEnable Enable*/, true /*LoRa Enable*/, true /*Serial Enable*/, true /*LoRa use PABOOST*/, 868E6 /*LoRa RF working band*/);
  Serial.println("Heltec.LoRa.Wifi Duplex");
  /*Heltec.display -> clear();
    Heltec.display() -> drawString(0, 50,"Heltec.LoRa.Wifi");
    Heltec.display -> display();*/
  delay(1000);
  Heltec.display -> clear();
  WIFISetUp();
}

void WIFISetUp(void)
{
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin("IRT", "pitonguanyin");
  delay(100);

  byte count = 0;
  while (WiFi.status() != WL_CONNECTED && count < 10)
  {
    count ++;
    delay(500);
    Serial.println("Connecting...");
    Heltec.display -> drawString(0, 0, "Connecting...");
    Heltec.display -> display();
  }

  Heltec.display -> clear();
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("Connecting...OK");
    Heltec.display -> drawString(0, 0, "Connecting...OK.");
    Heltec.display -> display();
    //delay(500);
  }
  else
  {
    Heltec.display -> clear();
    Serial.println("Connecting...Failed");
    Heltec.display -> drawString(0, 0, "Connecting...Failed");
    Heltec.display -> display();
    //while(1);
  }

  Heltec.display -> clear();
  return;
}

void loop() {
  //verification que la connection wifi est active
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("Connection OK");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Connection OK");
    Heltec.display -> display();
    delay(500);
  }
  else
  {
    Serial.println("Connection NOK");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Connection NOK");
    Heltec.display -> display();
    WIFISetUp();
    delay(500);
  }
  /*if (millis() - lastSendTime > interval)
  {
    String message = "Hello,I'm coming!";   // send a message
    sendMessage(message);
    Serial.println("Sending " + message);
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
  }*/
  //envoie de donner vers les serveur lora
  Serial.println("test reception");
  onReceive(LoRa.parsePacket());

}

//fonction envoie de donnée
/*void sendMessage(String outgoing)
{
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount);                 // add message ID
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}*/

//fonction reception de donnée
void onReceive(int packetSize)
{
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length

  String incoming = "";

  while (LoRa.available())
  {
    incoming += (char)LoRa.read();
  }

  if (incomingLength != incoming.length())
  { // check length for error
    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
  }

  // if the recipient isn't this device or broadcast,
  if (recipient != localAddress && recipient != 0xFF) {
    Serial.println("This message is not for me.");
    return;                             // skip rest of function
  }

  // if message is for this device, or broadcast, print details:
  Serial.println();
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incomingMsgId));
  Serial.println("Message length: " + String(incomingLength));
  Serial.println("Message: " + incoming);
  Serial.println("RSSI: " + String(LoRa.packetRssi()));
  Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println();

  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "Packet de " + String(sender, HEX) + " id: " + String(incomingMsgId) + " recu");
  Heltec.display -> display();

}
