#include "heltec.h"
#define BAND    433E6  //you can set band here directly,e.g. 868E6,915E6
#include <Crc16.h>

String outgoing;              // outgoing message
byte localAddress = 0xBB;     // address of this device
byte destination = 0xFD;      // destination to send to
byte msgCount = 0;            // count of outgoing messages
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends
byte data[] = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
String info = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
Crc16 crc;

void setup() {
  //WIFI Kit series V1 not support Vext control
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.println("Heltec.LoRa Duplex");
  //Serial.println(info);
  for (int i = 0; i < sizeof(data); i++)
  {
    Serial.print(data[i]);
  }
  Serial.println("data");
  Serial.println(info);
}

void loop() {
  if (millis() - lastSendTime > interval)  {
    //"{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000, \"c1\":{\"n\":\"SO2_001\"}" ;//, \"A\": \"true\"},\"c2\": { \"n\": \"HCL_001\", \"A\": \"true\"}, \"c3\": { \"n\": \"CH4_001\" , \"A\": \"true\"}, \"c4\": {\"n\":\"CH4_001\" , \"A\": \"true\"}},\"AD_2\":{\"p\":10000, \"c1\":{\"n\":\"SO2_001\" , \"A\": \"true\"},\"c2\": { \"n\": \"HCL_001\", \"A\": \"true\"}, \"c3\": { \"n\": \"CH4_001\" , \"A\": \"true\"}, \"c4\": {\"n\":\"CH4_001\" , \"A\": \"true\"}} }";
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Envoie debut ...");
    Heltec.display -> display();
    char* info1 = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,";
    sendMessage(info1);
    Serial.println("Envoie debut ...");
    Serial.println(info1);
    //delay(200);
    char* cap11 = "\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},";
    sendMessage(cap11);
    //delay(200);
    char* cap12 = "\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},";
    sendMessage(cap12);
    //delay(200);
    char* cap13 = "\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},";
    sendMessage(cap13);
    //delay(200);
    char* cap14 = "\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}";
    sendMessage(cap14);
    //delay(200);
    char* info2 = ",\"AD_2\":{\"p\":10000,";
    sendMessage(info2);
    //delay(200);
    char* cap21 = "\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},";
    sendMessage(cap21);
    //delay(200);
    char* cap22 = "\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},";
    sendMessage(cap22);
    //delay(200);
    char* cap23 = "\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},";
    sendMessage(cap23);
    //delay(200);
    char* cap24 = "\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
    sendMessage(cap24);
    Serial.println("Envoie dernière trame.");
    Serial.println(cap24);

    Serial.println("Calculating crc incrementally");
    unsigned short value = crc.getCrc();
    value = crc.XModemCrc(data, 0, sizeof(data));
    Serial.print("crc = 0x");
    Serial.println(value);//, HEX);
    String CRCE;
    CRCE = value;//, HEX;
    Serial.println("Envoie crc");
    sendMessage(CRCE);
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Envoie fin");
    Heltec.display -> display();
    //delay(1000);

    //sendMessage(message);
    //Serial.println("Sending " + message +" id "+ msgCount);
    //compte= compte +1;
    //Serial.println("Sending : ");
    //Serial.println(message);
    /*Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "envoie du message");
      Heltec.display -> display();*/
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
    delay(2000);
  }

  // parse for a packet, and call onReceive with the result:
  //onReceive(LoRa.parsePacket());
}

void sendMessage(String outgoing)
{
  if (msgCount > 10) {
    msgCount = 0;
  }
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  //if(msgCount)
  LoRa.write(msgCount);                 // add message ID
  Serial.print("Envoie de mess : ");
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "envoie du message ");
  Heltec.display -> display();
  /*Heltec.display -> clear();
    Heltec.display -> drawString(1, 0, String(msgCount));
    Heltec.display -> display();*/
  Serial.println(msgCount);
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}
