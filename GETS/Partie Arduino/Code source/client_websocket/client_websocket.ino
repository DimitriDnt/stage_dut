#include <WiFi.h>
#include <WebSocketClient.h>
 
const char* ssid     = "IRT";
const char* password = "pitonguanyin";
 
char path[] = "envoie de données test";
char host[] = "192.168.1.26";
 
WebSocketClient webSocketClient;
WiFiClient client;

String tramedebut = "{\"AdresseStation\":\"01\",\"CodeStation\":\"Gets5\",\"ADS1224_1\":{\"periode\":10000, \"capteur1\":{\"name\":\"SO2_001\" , \"Activer\": \"true\"},\"capteur2\": { \"name\": \"HCL_001\", \"Activer\": \"true\"}, \"capteur3\": { \"name\": \"CH4_001\" , \"Activer\": \"true\"}, \"capteur4\": {\"name\":\"CH4_001\" , \"Activer\": \"true\"}},\"ADS1224_2\":{\"période\":10000, \"capteur1\":{\"name\":\"SO2_001\" , \"Activer\": \"true\"},\"capteur2\": { \"name\": \"HCL_001\", \"Activer\": \"true\"}, \"capteur3\": { \"name\": \"CH4_001\" , \"Activer\": \"true\"}, \"capteur4\": {\"name\":\"CH4_001\" , \"Activer\": \"true\"}}}\"";

void setup() {
  Serial.begin(115200);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
 
  delay(5000);
 
  if (client.connect(host, 81)) {
    Serial.println("Connected");
  } else {
    Serial.println("Connection failed.");
  }
 
  webSocketClient.path = path;
  webSocketClient.host = host;
  if (webSocketClient.handshake(client)) {
    Serial.println("Handshake successful");
  } else {
    Serial.println("Handshake failed.");
  }
 
}
 
void loop() {
  String data;
 
  if (client.connected()) {
 
    webSocketClient.sendData(tramedebut);
 
    webSocketClient.getData(data);
    if (data.length() > 0) {
      Serial.print("Received data: ");
      Serial.println(data);
    }
 
  } else {
    Serial.println("Client disconnected.");
  }
 
  delay(3000);
 
}
