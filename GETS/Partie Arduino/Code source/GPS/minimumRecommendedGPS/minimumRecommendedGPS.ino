#include <SoftwareSerial.h>
#include <Gpsneo.h>
//Gpsneo gps;
Gpsneo gps(33, 32);

char latitud[11];
char latitudHemisphere[3];
char longitud[11];
char longitudMeridiano[3];


void  setup()
{
  Serial.begin(115200);
}
void loop()
{
  //Just simple do getDataGPRMC, and the method solve everything.
  gps.getDataGPRMC(latitud,
                   latitudHemisphere,
                   longitud,
                   longitudMeridiano);
                   
  Serial.print("latitude");
  Serial.println(latitud);
  Serial.print("latitudHEMi");
  Serial.println(latitudHemisphere);
  Serial.print("longitude");
  Serial.println(longitud);
  Serial.print("longitudeMER");
  Serial.println(longitudMeridiano);
  //Serial.println(gps.satellites.value());

}
