#define RXD2 23 // 16 est utilisé pour OLED_RST! 
#define TXD2 17
#include <TinyGPS++.h>
#include <WebSocketsServer.h>
#include "WiFi.h"
#include "heltec.h"
#include <Arduino.h>
TinyGPSPlus gps;
WebSocketsServer webSocket = WebSocketsServer(81);
bool plug = false;

void setup() {
  Serial.begin (115200);
  Serial2.begin (9600, SERIAL_8N1, RXD2, TXD2);
  WIFISetUp(); //appel de la fonction wifisetup
  IPAddress myIP = WiFi.localIP(); //definition de l'adresse ip
  Serial.print("Adresse IP du serveur: ");
  Serial.println(myIP);
  //fonction création Websocket
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}


void loop() {
  webSocket.loop(); //maintient de l'état ouvert et ecoute
  while (Serial2.available() > 0) {
    if (gps.encode(Serial2.read()))
      displayInfo();
  }
  if (millis() > 5000 && gps.charsProcessed() < 10) {
    Serial.println(F("No GPS detected: check wiring."));
    while (true);
  }
}

void displayInfo() {
  Serial.print(F("Location: "));

  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }
  String ggppss = (String) "Lat : " + (gps.location.lat(), 6) + ", Long: " + (gps.location.lng(), 6);
  webSocket.sendTXT(0, ggppss);

  Serial.println();
}
//création de la connexion wifi
void WIFISetUp(void) {
  // Configuration du WiFi en mode station et déconnection si deja connecté à un point d'accès
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  //WiFi.setAutoConnect(true);
  WiFi.begin("OVPFIUT", "pitonguanyin");
  delay(100);
  byte count = 0;
  // tant que la connection au wifi n'est pas établie on essaye
  while (WiFi.status() != WL_CONNECTED && count < 10) {
    count ++;
    delay(500);
    Serial.println("Connecting...");
  }
  //si la connection au wifi est établie
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Connecting...OK");
  }
  else {
    Serial.println("Connecting...Failed");
  }
  return;
}

//création websocket
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch (type) {
    //dans le cas ou le wifi n'est pas connecter
    case WStype_DISCONNECTED:
      plug = false;
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    //dans le cas ou le wifi est conecter
    case WStype_CONNECTED: {
        IPAddress ip = webSocket.remoteIP(num);
        /*boolean pareil = false;
        //Serial.print(placement);
        if (placement == 0) {
          Serial.println("= 0");
          for (int i = 0; i < 4; i++) {
            adrr[placement][i] = ip[i];
            /*Serial.print(adrr[placement][i]);
              Serial.print(".");*/
          /*}
        } else if (placement > 0 ) {
          Serial.println(" > 0");
          for (int i = 0; i < 4; i++) {
            if (adrr[placement][i] == ip[i]) {
              Serial.print((String)adrr[placement][i]);
              Serial.println(ip[i]);
              pareil = true;
              Serial.println((String)"pareil " + pareil);
            }
          }
        }

        if (pareil == true) {
          Serial.println("addr identique");
          break;
        }
        placement = num;
        Serial.println();
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        //placement = num ;
        Serial.printf("position %d \n", placement);
        Serial.print("adresse ");
        for (int i = 0; i < 4; i++) {
          //adrr[placement][i] = ip[i];
          Serial.print(adrr[placement][i]);
          Serial.print(".");
        }
        Serial.println();
      }*/
      break;
    }
    case WStype_TEXT:
      Serial.println(sizeof(payload));
      Serial.printf("[%u] get Text: %s\n", num, payload);
      /*if (nb trame qui arrive) {

        }
        if (trame) {

        }
        if (crc) {

        }
      */
      if (payload[0] == '#') {
      }
      break;
  }
}
