#include <Arduino.h>
//#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>
#include "heltec.h"
#include "WiFi.h"
//#include <SoftwareSerial.h>
//#include <Hash.h>
#define RXD2 23 // 16 est utilisé pour OLED_RST! 
#define TXD2 17
#include <TinyGPS++.h>
TinyGPSPlus gps;
#include <ArduinoJson.h>
WebSocketsServer webSocket = WebSocketsServer(81);

/*const char *ssid = "IRT";  //changer le nom ssid pour un autre nom unique
  const char *password = "pitonguanyin";*/

bool plug = false;
String don = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000, \"c1\":{\"n\":\"SO2_001\", \"A\": \"true\"},\"c2\": { \"n\": \"HCL_001\", \"A\": \"true\"}, \"c3\": { \"n\": \"CH4_001\" , \"A\": \"true\"}, \"c4\": {\"n\":\"CH4_001\" , \"A\": \"true\"}},\"AD_2\":{\"p\":10000, \"c1\":{\"n\":\"SO2_001\" , \"A\": \"true\"},\"c2\": { \"n\": \"HCL_001\", \"A\": \"true\"}, \"c3\": { \"n\": \"CH4_001\" , \"A\": \"true\"}, \"c4\": {\"n\":\"CH4_001\" , \"A\": \"true\"}} }";
String donne = "{\"AS\":\"01\",\"CS\":\"Gets6\"}";
//String fakegps = "{\"x\":\"45.41245542\",\"y\":\"45.65845413\"}";
String coordo;
String coor;
//String trame;
String traming;
char * json[200];
int posx, posy = 0;
StaticJsonDocument<1000> doc;
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

  switch (type) {
    case WStype_DISCONNECTED: {
        plug = false;
        Serial.printf("[%u] Disconnected!\n", num);
        break;
      }
    case WStype_CONNECTED: {
        plug = true;
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        // send message to client
        //webSocket.sendTXT(num, "Connected");
        break;
      }
    case WStype_TEXT: {
        String trame = (String) "{\"gps\":\"True\",\"Ads1\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"Ads2\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"CO2_1\":\"true\",\"CO2_2\":\"true\",\"MesureUI\":\"true\",\"Qair_1\":\"true\",\"CO2_1\":\"true\",\"Qair_2\":\"true\",\"CO2_1\":\"true\",\"Temp\":\"true\",\"Baro\":\"true\",\"Qair_1\":\"true\",\"C_Particules_1\":\"true\",\"C_Particules_2\":\"true\"}";
        //if (payload != #) {
        Serial.printf("[%u] get Text: %s \n", num, payload );
        char test[12];
        sprintf(test, "%s", payload);
        String res = test;
        Serial.println(test);
        Serial.println(res);
        if (res == "GETS1") {
          Serial.printf("Envoie JSON: GETS %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS2") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "{\"GETS\":\"3\"}") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS4") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS5") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS6") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS7") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS8") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS9") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS10") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res.length() > sizeof("GETS10")) {
          
          
          Serial.printf(" %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);

          
        } else Serial.printf("%s =/= {\"GCFG\":\"GETS3\"} \n", res); 
        /*
          char test[4];
          sprintf(test, "%s", payload);
          String res = test;
          Serial.println(test);
          Serial.println(res);
          if (res == "demande de configuration") {
          Serial.printf("Envoie JSON: %s \n", payload);
          webSocket.sendTXT(num, trame);
          } else if (res == "1") {
          Serial.printf("Envoie JSON: %s \n", payload);
          webSocket.sendTXT(num, trame);
          } else Serial.printf("message non reconnu: %s \n", res);
        */

        //Serial.println((String) "[" + num + "] a envoyer un message");
        /* for (int i = 0; i < sizeof(json); i++) {
           json[i] = (char *)payload[i];
           Serial.println((String) "json[" + i + "] = " + json[i]);
          }
          for (int i = 0; i < sizeof(payload); i++) {
           //json[i] = (char *)payload[i];
           Serial.println((String) payload[i]);
          }*/
        /*if (payload[0] == '#') {
          }
          break;*/
        /*Serial.println((String) "json length: " + sizeof(json));
          Serial.println((String) "payload length: " + sizeof(payload));*/

        /*for (int i = 0; i < 100; i++) {
          Serial.println((String) "json[" + i + "] = " + json[i]);
          }*/
        /*if (sizeof(json) == 0) {
          json = (char *)payload;
          Serial.println("0");
          } else if (sizeof(json) > 0) {
          Serial.println(">0");
          for (int i = 0; i < sizeof(json); i++) {
            if (json[i] != payload[i]) {
              Serial.println((String) "json[" + i + "] = " + json[i]);
              Serial.println((String) "payload[" + i + "] = " + payload[i]);
            }
          }
          }*/
        /* recup champ json
          json = (char *)payload;
          // Deserialize the JSON document
          DeserializationError error = deserializeJson(doc, json);

          // Test if parsing succeeds.
          if (error) {
          Serial.print(F("deserializeJson() failed: "));
          Serial.println(error.c_str());
          return;
          }
          char Date = doc["Date"];
          char NumeroSerie = doc["NumeroSerie"];
          uint8_t Periode = doc["Periode"];
          uint8_t Gain = doc["Gain"];
          boolean Enable = doc["Enable"];

          // Print values.
          Serial.print("Date : ");
          Serial.println(Date);
          Serial.print("NumeroSerie : ");
          Serial.println(NumeroSerie);
          Serial.print("Periode : ");
          Serial.println(Periode);
          Serial.print("Gain : ");
          Serial.println(Gain);
          Serial.print("Enable : ");
          Serial.println(Enable);
        */
        /*if (payload[0] == '#') {
          }*/
        break;
      }
  }
}
void WIFISetUp(void) {
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin("IRT", "pitonguanyin");
  delay(100);

  byte count = 0;
  while (WiFi.status() != WL_CONNECTED && count < 10)
  {
    count ++;
    delay(500);
    Serial.println("Connecting...");

  }

  //Heltec.display -> clear();
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("Connecting...OK");

    //delay(500);
  }
  else
  {
    Serial.println("Connecting...Failed");
  }

  return;
}

void setup() {
  Serial.begin(115200);
  Serial2.begin (9600, SERIAL_8N1, RXD2, TXD2);
  Serial.setDebugOutput(true);
  WIFISetUp();
  //WiFi.softAP(ssid, password);

  IPAddress myIP = WiFi.localIP();
  Serial.print("Adresse IP du serveur: ");
  Serial.println(myIP);
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}
void loop() {
  webSocket.loop();

  while (Serial2.available() > 0) {
    if (gps.encode(Serial2.read()))
      displayInfo();
  }
  /*if (millis() > 5000 && gps.charsProcessed() < 10) {
    Serial.println(F("No GPS detected: check wiring."));
    while (true);
    }*/
  if (plug) {
    //displayInfo();
    //Serial.println("lancement d'un message");
    //coordo = (String)"{\"x\":\"" + gps.location.lng() + "\",\"y\":\"" + gps.location.lat() + "\"}";

    /*posx++;// random(0, 2);
      posy++;//random(0, 2);
      Serial.println(posx);
      Serial.println(posy);*/

    coor = (String)"{\"GPS\":{\"x\":\"55.57192" + posx + "\",\"y\":\"-21.208" + posy + "\"}}";
    String trame = (String) "{\"gps\":\"True\",\"Ads1\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"Ads2\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"CO2_1\":\"true\",\"CO2_2\":\"true\",\"MesureUI\":\"true\",\"Qair_1\":\"true\",\"CO2_1\":\"true\",\"Qair_2\":\"true\",\"CO2_1\":\"true\",\"Temp\":\"true\",\"Baro\":\"true\",\"Qair_1\":\"true\",\"C_Particules_1\":\"true\",\"C_Particules_2\":\"true\"}";
    traming = (String) "{\"codeMateriaux\":\"BOI_100\",\"DHT11_1\":{\"Temperature\":\"" + random(17, 20) + "\",\"Hydrometrie\":\" " + random(45, 65) + "\"},\"DHT11_2\":{\"Temperature\":\" " + random(17, 20) + "\",\"Hydrometrie\":\" " + random(45, 65) + "\"},\"DHT11_3\":{\"Temperature\":\" " + random(17, 20) + "\",\"Hydrometrie\":\" " + random(45, 65) + "\"}}";

    //Serial.println(coor);
    //lancement d'un message
    /*for (int i = 0; i < 4; i++) {
      //webSocket.sendTXT(i, coordo);
      //delay(500);
      webSocket.sendTXT(i, coor);
      //Serial.println(coor);
      delay(500);
      webSocket.sendTXT(i, traming);
      //Serial.println(trame);
      //delay(500);
      webSocket.sendTXT(i, trame);
      //Serial.println(trame);
      delay(500);
      }*/
    //Serial.println(".");
  } else if (!plug) {
    posx = 0;
    posy = 0;
  }
}

void displayInfo() {
  Serial.print(F("Location: "));
  if (gps.location.isValid()) {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }  else {
    Serial.print(F("INVALID"));
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid()) {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  } else {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid()) {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  } else {
    Serial.print(F("INVALID"));
  }
}
/*Serial.println();
  if (plug) {
  Serial.println("lancement d'un message");
  //coor = (String)"{\"x\":\"" + gps.location.lat() + "\",\"y\":\"" + gps.location.lng() + "\"}";
  coor = (String)"{\"x\":\"-21.209" + random(0000, 9999) + "\",\"y\":\"55.571" + random(0000, 9999) + "\"}";
  //Serial.println(coor);
  //lancement d'un message
  webSocket.sendTXT(0, coor);
  Serial.println(coor);
  delay(250);
  webSocket.sendTXT(0, trame);
  Serial.println(trame);
  delay(250);
  }

  }*/
