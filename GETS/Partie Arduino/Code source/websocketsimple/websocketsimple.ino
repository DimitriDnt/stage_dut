/*#define BAND 433E6  //you can set band here directly,e.g. 868E6,915E6
  #define PORT 81   //81 port du WEBSOCKET
*/
#include <Arduino.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>
#include "heltec.h"
#include "WiFi.h"

WebSocketsServer webSocket = WebSocketsServer(81);
bool plug = false;
int posx, posy = 0;
String trame;

//SETUP
void setup() {
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, 433E6 /*long BAND*/);

  //setup WIFI
  WIFISetUp();
  IPAddress myIP = WiFi.localIP();
  Serial.print("Adresse IP du serveur: ");
  Serial.println(myIP);

  //setup WS
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

//Initialisation WIFI
void WIFISetUp(void) {
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin("IRT", "pitonguanyin");
  delay(100);
  byte count = 0;
  while (WiFi.status() != WL_CONNECTED && count < 10) {
    count ++;
    delay(500);
    Serial.println("Connecting...");
  }
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Connecting...OK");
  } else {
    Serial.println("Connecting...Failed");
  }
  return;
}

//LOOP
void loop() {
  webSocket.loop();
  if (plug) { //si client connecté les variables sont initialisé et peuvent être envoyé
    String coor = (String)"{\"GPS\":{\"x\":\"55.57192" + posx + "\",\"y\":\"-21.208" + posy + "\"}}";
    trame = (String) "{\"gps\":\"True\",\"Ads1\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"Ads2\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"CO2_1\":\"true\",\"CO2_2\":\"true\",\"MesureUI\":\"true\",\"Qair_1\":\"true\",\"CO2_1\":\"true\",\"Qair_2\":\"true\",\"CO2_1\":\"true\",\"Temp\":\"true\",\"Baro\":\"true\",\"Qair_1\":\"true\",\"C_Particules_1\":\"true\",\"C_Particules_2\":\"true\"}";
    String traming = (String) "{\"codeMateriaux\":\"BOI_100\",\"DHT11_1\":{\"Temperature\":\"" + random(17, 20) + "\",\"Hydrometrie\":\" " + random(45, 65) + "\"},\"DHT11_2\":{\"Temperature\":\" " + random(17, 20) + "\",\"Hydrometrie\":\" " + random(45, 65) + "\"},\"DHT11_3\":{\"Temperature\":\" " + random(17, 20) + "\",\"Hydrometrie\":\" " + random(45, 65) + "\"}}";
  } else if (!plug) { //Mise à zero variable position
    posx = 0;
    posy = 0;
  }
}

//fonction ecoute evenement WS
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

  switch (type) {
    case WStype_DISCONNECTED: {
        plug = false;
        Serial.printf("[%u] Disconnected!\n", num);
        break;
      }
    case WStype_CONNECTED: {
        plug = true;
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        // send message to client
        //webSocket.sendTXT(num, "Connected");
        break;
      }
    case WStype_TEXT: {
        String trame = (String) "{\"gps\":\"True\",\"Ads1\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"Ads2\":{\"capteur1\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur2\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur3\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"},\"capteur4\":{\"name\":\"CH4\",\"gain\":\"" + random(17, 20) + "." + random(17, 20) + "\",\"dateMiseEnFonction\":\"jj/mm/aaaa\",\"NumeroSerie\":\"" + random(17, 20) + "WX" + random(17, 20) + "KS0" + random(17, 20) + "\",\"periode\":\"" + random(00000, 10000) + "\",\"enable\":\"True\"}},\"CO2_1\":\"true\",\"CO2_2\":\"true\",\"MesureUI\":\"true\",\"Qair_1\":\"true\",\"CO2_1\":\"true\",\"Qair_2\":\"true\",\"CO2_1\":\"true\",\"Temp\":\"true\",\"Baro\":\"true\",\"Qair_1\":\"true\",\"C_Particules_1\":\"true\",\"C_Particules_2\":\"true\"}";
        //if (payload != #) {
        //Serial.printf("[%u] get Text: %s \n", num, payload );
        char test[12];
        sprintf(test, "%s", payload);
        String res = test;
        /*Serial.println(test);
        Serial.println(res);*/
        if (res == "GETS1") {
          Serial.printf("Envoie JSON: %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS2") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS3") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS4") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS5") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS6") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS7") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS8") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS9") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else if (res == "GETS10") {
          Serial.printf("Envoie JSON:  %s a [%u] \n", payload, num);
          webSocket.sendTXT(num, trame);
        } else Serial.printf("[%u] get Text: %s \n", num, payload );
        break;
      }
  }
}
