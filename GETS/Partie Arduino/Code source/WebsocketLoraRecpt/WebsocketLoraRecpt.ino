//début librairie
#include "heltec.h"
#include <ArduinoJson.h>
#define BAND    433E6  //you can set band here directly,e.g. 868E6,915E6
#include <Arduino.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>
#include "heltec.h"
#include "WiFi.h"
#include <Crc16.h>
//fin librairie

//début variable
WebSocketsServer webSocket = WebSocketsServer(81);
bool plug = false;
String outgoing;              // outgoing message
byte localAddress = 0xFD;     // address of this device
byte destination = 0xBB;      // destination to send to
byte msgCount = 0;            // count of outgoing messages
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends
String file;
//byte data[] = file;
Crc16 crc;
//fin variable

void setup() {
  //Initialisation
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.println("Heltec.WebSocket.LoraRecpetion");
  Serial.setDebugOutput(true);
  //fonction connexion wifi
  WIFISetUp();
  IPAddress myIP = WiFi.localIP();
  Serial.print("Adresse IP du serveur: ");
  Serial.println(myIP);
  //fonction création WS
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

void loop() {
  // parse for a packet, and call onReceive with the result:
  if (plug) {
    onReceive(LoRa.parsePacket());
    //Serial.println("connecter");
  }
  webSocket.loop();
}

//début déclaration fonction
//création de la connexion wifi
void WIFISetUp(void) {
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin("IRT", "pitonguanyin");
  delay(100);
  byte count = 0;

  while (WiFi.status() != WL_CONNECTED && count < 10) {
    count ++;
    delay(500);
    Serial.println("Connecting...");
  }
  //Heltec.display -> clear();
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Connecting...OK");
    //delay(500);
  }
  else {
    Serial.println("Connecting...Failed");
  }
  return;
}

//création websocket
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch (type) {
    case WStype_DISCONNECTED:
      plug = false;
      Serial.printf("[%u] Disconnected!\n", num);
      break;

    case WStype_CONNECTED: {
        plug = true;
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        // send message to client
        //webSocket.sendTXT(num, "Connected");
      }
      break;

    case WStype_TEXT:
      Serial.printf("[%u] get Text: %s\n", num, payload);
      if (payload[0] == '#') {
      }
      break;
  }
}

//fonction de réception de donnée
void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return
  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length
  String incoming;

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }
  /*if (incomingLength != incoming.length())
    { // check length for error
    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
    }*/

  // if the recipient isn't this device or broadcast,
  if (recipient != localAddress && recipient != 0xFF) {
    Serial.println("This message is not for me.");
    return;                             // skip rest of function
  }

  char charBuf[200];
  incoming.toCharArray(charBuf, 200);
  //afficher quand réception de message
  /*if (millis() - lastSendTime > interval) {
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Je recois");
    Heltec.display -> display();
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
    delay(1000);
    Heltec.display -> clear();
    }*/
  // if message is for this device, or broadcast, print details:
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incomingMsgId));
  Serial.println("Message length: " + String(incomingLength));
  Serial.println(incoming);
  //Serial.println("RSSI: " + String(LoRa.packetRssi()));
  //Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println();
  Heltec.display -> clear();
  Heltec.display -> drawString(1, 1, "Packet de " + String(sender, HEX) + " id " + String(incomingMsgId));
  Heltec.display -> display();

  if (incomingMsgId == 0) { //si id = 0
    Serial.println("id = 0");
    if (file.length() == 0) { //si fichier vide
      Serial.print("fichier vide: ");
      Serial.println(file.length());
      file = file + incoming; //stocker dans fichier

    } else if (file.length() != 0 ) { //si fichier pas vide
      Serial.print("fichier pas vide: ");
      Serial.println(file.length());
      Serial.println("erase"); //affiche fichier
      file = ""; //vider le ficher
      file = file + incoming; //stocker dans fichier
    }
  } /*else  if (incomingMsgId == 9) { //si id diff 0
    Serial.println("id = 9");
    if (file.length() != 0 ) { //si fichier pas vide
      Serial.print("fichier pas vide: ");
      file = file + incoming; //stocker dans fichier
      Serial.println(file.length());
      Serial.println("file: " + file); //affiche fichier
      Serial.println("envoie file vers Serveur ");
      if (plug = true) {// envoie de la trame
        Serial.println("lancement d'un message");
        //lancement d'un message
        webSocket.sendTXT(0, file);
        //delay(1000);
      }
      file = ""; //vider le ficher
      Heltec.display -> clear();
      Heltec.display -> drawString(0, 0, "J'ai reçu une trame entière");
      Heltec.display -> display();
    }
  }*/ else  if (incomingMsgId == 10) { //si id diff 0
    Serial.println("id = 10");
    if (file.length() != 0 ) { //si fichier pas vide
      Serial.print("fichier pas vide: ");
      //file = file + incoming; //stocker dans fichier
      Serial.println(file.length());

      String CRCR = incoming;//,HEX;
      CRCR = CRCR, HEX;
      Serial.print("crc reçu = 0x");
      Serial.println(CRCR); //affiche fichier

      Serial.println(file);
      //byte* data;//[file.length()];
      char B[file.length()];
      file.toCharArray(B, file.length()+1);
      Serial.println(B);
      byte data[sizeof(B)];
      Serial.println(".");
      for (int i = 0; i < sizeof(B); i++)
      {
        data[i]=B[i];
      }
      Serial.println("..");
      //B.getBytes(data, B.length());
      //char* buff[file.length()];
      //file.toCharArray(buff,file.length());
      //*data = *(byte)*buff;

      byte test[] = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
      String infos = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
      String CRCL;
      Serial.print("data ");
      Serial.print(sizeof(data));
      Serial.print(" file ");
      Serial.print(file.length());
      Serial.println("");
      Serial.print("test ");
      Serial.print(sizeof(test) - 1);
      Serial.print(" infos ");
      Serial.print(infos.length());
      Serial.println("");
      for (int i = 0; i < file.length(); i++)
      {
        Serial.print(data[i]);
      }
      Serial.println("");
      Serial.println(file);
      Serial.println("");
      for (int i = 0; i < file.length(); i++)
      {
        Serial.print(test[i]);
      }
      Serial.println("");
      Serial.println(infos);
      Serial.println("");

      Serial.println("Calculating crc incrementally");
      unsigned short value = crc.getCrc();
      value = crc.XModemCrc(data, 0, sizeof(data));
      Serial.print("crc = 0x");
      Serial.println(value);//, HEX);
      CRCL = value;//, HEX;

      if (CRCL == CRCR) {
        Serial.println("crc valider");
        Serial.println("envoie file vers Serveur ");

        if (plug = true) {// envoie de la trame
          Serial.println("lancement d'un message");
          //lancement d'un message
          webSocket.sendTXT(0, data);
          //delay(1000);
        }
        file = ""; //vider le ficher
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "J'ai reçu une trame entière");
        Heltec.display -> display();
      } else if (CRCL != CRCR) {
        Serial.println("crc pas valide");
        Serial.println("envoie file vers Serveur Annuler");
        file = "";//vider le fichier
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "J'ai reçu une trame non valide");
        Heltec.display -> display();
      }
    }
  } else  if (incomingMsgId != 0) { //si id diff 0
    Serial.println("id !=0");
    if (file.length() != 0 ) { //si fichier pas vide
      Serial.print("fichier pas vide: ");
      Serial.println(file.length());
      file = file + incoming;

    } else if (file.length() == 0 ) { //si fichier vide
      Serial.println("fichier vide: ");
      Serial.println(file.length());
    }
  }
}
