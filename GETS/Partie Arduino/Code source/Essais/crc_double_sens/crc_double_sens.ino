#include <Crc16.h>

//Crc 16 library (XModem)
Crc16 crc;

void setup()
{
  Serial.begin(115200);
  //Serial.println("CRC-16 bit test program");
  //Serial.println("=======================");

}

void loop() {
  byte data[13] = "123456789"; //contenu en envoyer

  unsigned short value = crc.getCrc(); //création crc depuis byte array
  crc.clearCrc();
  value = crc.XModemCrc(data, 0, sizeof(data) - 4);

  //crc dans char
  char crcH[4];
  sprintf(crcH, "%04X", value);

  //ajout du crc à data
  for (int i = 0; i <= 3; i++) {
    data[sizeof(data) - i - 1] = crcH[3 - i];
  }
  String datas = ""; //création varaiable data recu

  //byte array en string
  for (int i = 0; i <= sizeof(data) - 1; i++) {
    //Serial.print(char(data[i]));
    //Serial.print(i);
    datas += char(data[i]);
  }
  Serial.print("datas : ");
  Serial.println(datas);
  /* Transmission
     ||  ||  ||
     ||  ||  ||
     ||  ||  ||
     Reception
  */

  int i = 0;
  byte datar[datas.length() - 4]; //création byte array type du contenu - taille crc
  char crcr[4]; // tableau crc

  while (datas[i] != 0) { //separation entre info et crc dans datar et crcr
    if (i < datas.length() - 4) {
      datar[i] = datas[i];
      /*Serial.print("datar[");
        Serial.print(i);
        Serial.print("]=");
        Serial.println(datas[i]);*/
    } else {
      crcr[i - sizeof(datar)] = datas[i];
      /*Serial.print("crcr[");
        Serial.print(i - sizeof(datar));
        Serial.print("]=");
        Serial.println(crcr[i - sizeof(datar)]);*/
    }
    i++;
  }
  Serial.println();

  //boucle affichage comparaison des contenus
  /*for (int i = 0; i <= sizeof(datar) - 1; i++) {
    Serial.print(char(datar[i]));
    }
    Serial.println();

    for (int i = 0; i <= sizeof(datar) - 1; i++) {
    Serial.print(char(data[i]));
    }
    Serial.println();*/
  unsigned short valuer = crc.getCrc(); // création crc de datar > donnée recu
  crc.clearCrc();
  valuer = crc.XModemCrc(datar, 0, sizeof(datar));

  /*Serial.print("crcr :");
    Serial.println(valuer, HEX);
    Serial.println();*/
  char crcR[4]; //conteneur de crcr
  sprintf(crcR, "%04X", valuer); // crc dans char

  //verif égalité crc
  for (int i = 0; i <= sizeof(crcr) - 1; i++) {
    Serial.print(crcr[i]);
  }
  Serial.println();
  for (int i = 0; i <= sizeof(crcR) - 1; i++) {
    Serial.print(crcR[i]);
  }
  Serial.println();
  boolean verif = true;
  for (int i = 0; i <= sizeof(crcr) - 1; i++) {
    if (crcr[i] == crcR[i]) {
      verif &= true;
      if (verif == false) {
        return;
      }
    }
  }
  if (verif == true) {
    Serial.print("Trame recu");
  }
  while (true); //fin

}
