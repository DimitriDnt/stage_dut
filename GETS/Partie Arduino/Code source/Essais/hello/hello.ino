#include "heltec.h"
#define BAND    433E6  //you can set band here directly,e.g. 868E6,915E6

void setup() {
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
}

void loop() {
  Serial.println("hello");
  Serial.println("hello world");
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "hello");
  Heltec.display -> display();
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "hello world");
  Heltec.display -> display();
  delay(1000);
}
