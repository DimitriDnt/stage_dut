#include <ArduinoJson.h>

void setup() {
  // Initialize serial port
  Serial.begin(115200);
  while (!Serial) continue;

  StaticJsonDocument<2000> doc;
  char json[] =
    "{\"sensor\":\"gps\",\"time\":1351824120,\"data\":[48.756080,2.302038]}";
    //"{\"AdresseStation\":\"01\",\"CodeStation\": \"Gets5,\"ADS1224_1\":{ \"periode\":\"10000\",\"capteur1\":{\"name\":\"SO2_001\",\"Enable\":\"true\"},\"capteur2\":{\"name\":\"HCL_001\",\"Enable\":\"true\"},\"capteur3\":{\"name\":\"CH4_001\",\"Enable\":\"true\"} } }";

  //Exemple du contenu fichier JSON
  /*AdresseStation 01
    CodeStation Gets5
    ADS1224_1
    periode 10000
    capteur1 name SO2_001 Enable true
    capteur2 name HCL_001 Enable true
    capteur3 name CH4_001 Enable true
    ADS1224_2 periode 10000
    capteur1 name OX_001 Enable true
    capteur2 name NO2_001 Enable true
    capteur3 name H2_001 Enable true
  */
  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, json);
  delay(1000);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  // Fetch values.

  const char* sensor = doc["sensor"];

  int AdresseStation = doc["AdresseStation"]; //AdresseStation 01
  const char* CodeStation = doc["CodeStation"];//CodeStation Gets5

  //char Table = doc["ADS1224_1"];//ADS1224_1
  //long time = doc["time"];
  /*
    long periode = doc["ADS1224_1"][0]; //periode 10000
    const char* capteur1 = doc["ADS1224_1"][1,1]; //capteur1 name SO2_001
    boolean enable1 = doc["ADS1224_1"][1,2]; //Enable true
  */
  //doc["ADS1224_1"]; //capteur2 name HCL_001 Enable true
  //doc["ADS1224_1"]; //capteur3 name CH4_001 Enable true
  /*
    double latitude = doc["data"][0];
    double longitude = doc["data"][1];
  */
  // Print values.
  Serial.println(sensor);
  /*Serial.println(AdresseStation);
    Serial.println(CodeStation);
    Serial.println(periode);
    Serial.println(capteur1);
    Serial.println(enable1);*/
  delay(2000);
}

void loop() {
  // not used in this example
}
