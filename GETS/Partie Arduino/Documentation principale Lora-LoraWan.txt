Recherche Lora / LoRaWAN
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Propriétaire : Lora Alliance 
Bande de fréquence : 430MHz Asie, 433MHz et 868MHz Europe, 915MHz Etat Unies
Débit Montant/Descendant : 0,3 - 37,5kbps
Distance : 5 km(urbain), 15 km(rural)
Canaux : 10 en Europe 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Data Rate (DR)	| Modulation	| Spreading Factor (SF)	| Bande Passante	| Débit Physique (bit/s)
0				| LoRa			| SF12					| 125 kHz			| 250
1				| LoRa			| SF11					| 125 kHz			| 440
2				| LoRa			| SF10					| 125 kHz			| 980
3				| LoRa			| SF9					| 125 kHz			| 1 760
4				| LoRa			| SF8					| 125 kHz			| 3 125
5				| LoRa			| SF7					| 125 kHz			| 5 470
6				| LoRa			| SF7					| 250 kHz			| 11 000
7				| FSCK			| 50kbit/s									| 50 000 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
La trame physique LoRa se compose d'un préambule, un en-tête, les données puis un contrôle d'erreurs.
L'en-tête est transmis avec un taux de code de 4/8. Il indique la taille des données, le taux de code pour le reste de la trame et il précise également si un CRC est présent. 
Un CRC est présent dans l'en-tête afin de permettre au récepteur de détecter s'il est altéré.
La taille maximale des données se situe entre 51 et 222 octets selon le facteur d'étalement utilisé (plus le SF est grand plus la taille des données est faible).
Le code rate pour le reste de la trame peut être paramétré de 4/5 à 4/8, le taux 4/5 étant le plus utilisé.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
D'après ces paramètres, il est donc possible de définir le débit utile Rb par la formule mathématique : 
Rb = SF * (BW / 2^SF) * CR <==> RateBit(Bit/s) = SpreadingFactor * ( BandWidth(KHz) / 2^SpreadingFactor) * CodeRate
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Taille des données	| Spreading Factor		| Temps de transmission
20 octets			| 12					| 1,5 sec
20 octets			| 10					| 0,4 sec
40 octets			| 12					| 1,8 sec
40 octets			| 10					| 0,5 sec
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
LoRaWAN implémente plusieurs clefs, propres à chaque équipement terminal, afin d'assurer la sécurité des échanges au niveau réseau et applicatif :
	- clef AES d'une longueur de 128 bits appelée Appkey est utilisée pour générer les clefs NwkSKey et AppSKey
	- clef NwkSKey utilisée par le serveur et l'équipement d'extrémité pour générer le champ d'intégrité MIC présent dans les paquets.  intégrité 
	- clef AppSKey utilisée pour chiffrer les données applicatives présentes dans le paquet.  confidentialité
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
LoRaWAN recommande d'utiliser des méthodes de protections de bout en bout supplémentaires pour les applications qui nécessiteraient un degré de sécurité supérieur
Le protocole définit également les champs DevEUI et AppEUI, permettent d'identifier l'équipement et identifier l'application
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Sources : 
	- https://fr.wikipedia.org/wiki/LoRaWAN
	- http://cedric.cnam.fr/~bouzefra/cours/cours_Lora.pdf