#include "heltec.h"
#define BAND    433E6  //definition de la bande utiliser, 868E6,915E6
#include <Crc16.h>
#include <WiFi.h>
#include <WebSocketClient.h>
//-------------- -WSC----------------
const char* ssid     = "OVPF-iut";
const char* password = "pitonguanyin";
char path[] = "envoie de données test";
char host[] = "192.168.1.99";
WebSocketClient webSocketClient;
WiFiClient client;
//--------------------fin------------
String outgoing;              // message sortant
byte localAddress = 0xBB;     // adresse local
byte destination = 0xFD;      // destination d'envoi
byte msgCount = 0;            // compteur de messages sortant
int msgRecv = 0;
long lastSendTime = 0;        // temps du dernier envoi
int interval = 2000;          // interval entre les envoie
//variable CRC
Crc16 crc;
//Variables trames compresser et complète
String halfTrame = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
String fullTrame = "{\"AdresseStation\":\"01\",\"CodeStation\":\"Gets5\",\"ADS1224_1\":{\"période\":10000,\"capteur1\":{\"name\":\"SO2_001\",\"Activer\":\"true\"},\"capteur2\":{ \"name\":\"HCL_001\",\"Activer\":\"true\"},\"capteur3\":{\"name\":\"CH4_001\",\"Activer\":\"true\"},\"capteur4\":{\"name\":\"CH4_001\",\"Activer\":\"true\"}},\"ADS1224_2\":{\"période\":10000,\"capteur1\":{\"name\":\"SO2_001\",\"Activer\":\"true\"},\"capteur2\":{\"name\":\"HCL_001\",\"Activer\":\"true\"},\"capteur3\":{\"name\":\"CH4_001\",\"Activer\":\"true\"},\"capteur4\":{\"name\":\"CH4_001\",\"Activer\":\"true\"}}}";
String trameAenvoyer = fullTrame; //modifier la variable à envoyer
int limiteCount;              //limite de Message à envoyer
String messagerecu;
int boucle = 0;

void setup() {
  //Declaration des consoles
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.println("Heltec.LoRa Envoie");
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "Heltec.LoRa Envoie");
  Heltec.display -> display();
  wscsetup();
}
void wsfin() {
  WiFi.status() == WL_CONNECTED;
  Serial.println("fermeture WS");
}
void wscsetup() {
  WiFi.begin(ssid, password);
  while (boucle < 10) {
    if (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
      boucle ++;
    } else if (WiFi.status() == WL_CONNECTED) {
      boucle == 0;
      break;
    }
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(2500);
  if (client.connect(host, 81)) {
    Serial.println("Connected");
  } else {
    Serial.println("Connection failed.");
  }
  webSocketClient.path = path;
  webSocketClient.host = host;
  if (webSocketClient.handshake(client)) {
    Serial.println("Handshake successful");
  } else {
    Serial.println("Handshake failed.");
  }
}

void talk(String message) {
 /* if (client.connected()) {
    Serial.println("Send by WS");
    webSocketClient.sendData(message);
    //webSocketClient.getData(messagerecu);
    if (messagerecu.length() > 0) {
      Serial.print("Received data: ");
      Serial.println(messagerecu);
    }
  } else {*/
    Serial.println("No WS send by Lora");
    sendMessage(message); // envoie par Lora
  //}
}
void loop() {
  //Appel de fonction à différente interval
  if (millis() - lastSendTime > interval)  {
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Envoie debut ..."); //affichage sur la carte
    Heltec.display -> display();
    Serial.println();
    while (client.connected()) {
      webSocketClient.sendData(trameAenvoyer);
      Serial.println("Envoi par WebSocket");
      delay(500);
    }
    formate(trameAenvoyer, trameAenvoyer.length(), 0); //Appel de la fonction formatage
    //calcrc(trameAenvoyer, sizeof(trameAenvoyer), trameAenvoyer.length()); //Appel de la fonction calcul CRC

    //Calculating crc incrementally
    Serial.println();
    byte copieData[sizeof(trameAenvoyer)]; //variable de stockage de la trame de type byte

    for (int i = 0; i <= trameAenvoyer.length(); i++) { //boucle de conversion de la trame de type string vers une trame byte
      copieData[i] = trameAenvoyer[i];
    }

    unsigned short value = crc.getCrc(); //variable value prend la valeur du CRC
    crc.clearCrc();
    value = crc.XModemCrc(copieData, 0, sizeof(copieData)); //value obtient la valeur CRC calculer par rapport à la trame en byte

    char crcLocal[4]; // variable de stockage du CRC//sizeof(value)
    sprintf(crcLocal, "%04X", value); // conversion vers un format hexa
    String crcString = ""; //création variable data recu

    //tableau de byte en string
    for (int i = 0; i <= sizeof(crcLocal) - 1; i++) {
      crcString += char(crcLocal[i]);
    }
    /*if (client.connected()) {
      webSocketClient.sendData(message);
      //webSocketClient.getData(messagerecu);
      if (messagerecu.length() > 0) {
        Serial.print("Received data: ");
        Serial.println(messagerecu);
      }
      } else {
      Serial.println("Client disconnected.");
      }*/
    talk(crcString);
    //wsfin();
    //sendMessage(crcString); // envoie du crc
    Serial.println("envoie crc");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Envoie fin");
    Heltec.display -> display();
    //delay(1000);
    lastSendTime = millis();            // horodatage du message
    interval = random(2000) + 1000;    // 2-3 seconds
    delay(2000);
  }
}
//methode de calcul du CRC
void calcrc(String contenu, int taille, int longueur) { //declarations des variables
  //Calculating crc incrementally
  Serial.println();
  byte copieData[taille]; //variable de stockage de la trame de type byte

  for (int i = 0; i <= longueur; i++) { //boucle de conversion de la trame de type string vers une trame byte
    copieData[i] = contenu[i];
  }

  unsigned short value = crc.getCrc(); //variable value prend la valeur du CRC
  crc.clearCrc();
  value = crc.XModemCrc(copieData, 0, sizeof(copieData)); //value obtient la valeur CRC calculer par rapport à la trame en byte

  char crcLocal[4]; // variable de stockage du CRC//sizeof(value)
  sprintf(crcLocal, "%04X", value); // conversion vers un format hexa
  String crcString = ""; //création variable data recu

  //tableau de byte en string
  for (int i = 0; i <= sizeof(crcLocal) - 1; i++) {
    crcString += char(crcLocal[i]);
  }
  sendMessage(crcString); //envoie du crc
  /*Serial.println("envoie crc");
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Envoie fin");
    Heltec.display -> display();*/
}

//methode de formatage de la trame
void formate(String contenu, int longueur, int adresse) {//declarations des variables
  int debut = adresse; //variable de stockage de l'adresse de départ
  int tailleMes = 50; // taille maximal des trames formater
  int nombreTabl = (contenu.length() / tailleMes) + 1; //nombre de tableau pour stocker les trames formater
  limiteCount = nombreTabl + 1; //Nombre maximal de packet à envoyer par lora
  String resu[nombreTabl] = ""; //tableau de string pour stocker les trames formater
  Serial.print("nombre de tableau: ");
  Serial.println(nombreTabl);
  int decompte = 0; //variable pour compter
  int tailleToGo = 50;// taille des messages à atteindres;
  int loopn = 0; //compteur de boucle
  talk(String(nombreTabl));
  //sendMessage(String(nombreTabl)); //envoie du meessage du nombre de tableau totaux
  //boucle de formatage
  while (decompte != longueur) {
    Serial.println((String)"decompte=" + decompte + " et longueur=" + longueur);
    if (decompte >= tailleMes) { //si le compteur est supérieur ou égale à la taille que le message doit avoir
      resu[adresse] = ""; //vide du tableau (dans la cas ou il a déja été utiliser et contiendrais encore son anciens contenu
      if (decompte >= tailleToGo) { //si le compteur est supérieur ou égale à la taille à atteindre
        tailleToGo = tailleToGo + tailleMes; //on augmente la taille à atteindre d'une taille de message
      }

      while (decompte <= tailleToGo) { // tant que le compteur est inférieur ou égale à la taille à atteindre
        if (decompte == longueur) { //si le compteur atteind la taille de la trame initial
          break; // on quitte le formatage
        }
        resu[adresse] = resu[adresse] + contenu[decompte]; //stockage de la trame à l'id du compteur dans le tableau
        decompte++;
        delay(10);
      }
      talk(resu[adresse]);
      //sendMessage(resu[adresse]); // envoie du message du tableau
      adresse++;

    } else if (decompte < tailleMes) { //sinon si le compteur est inférieur à la taille d'un message
      resu[adresse] = ""; //vide du tableau (dans la cas ou il a déja été utiliser et contiendrais encore son anciens contenu
      while (decompte < tailleMes) { //tant que le compteur est inférieur à la d'un message
        if (decompte == longueur) { //si le compteur égale à la taille de la trame
          loopn++;
          break; // onquitte le formatage
        }
        resu[adresse] = resu[adresse] + contenu[decompte]; //stockage de la trame à l'id du compteur dans le tableau
        decompte++;
        delay(10);
      }
      talk(resu[adresse]);
      //sendMessage(resu[adresse]); // envoie du message du tableau
      adresse++;
    }
    loopn++;
  }
  Serial.println("string in array...OK");
  for (debut; debut <= adresse; debut++ ) { //boucle affichage de tout les tableaux
    Serial.println((String)"resu[" + debut + "]: " + resu[debut]);
  }
  //calcrc(trameAenvoyer, sizeof(trameAenvoyer), trameAenvoyer.length());
  Serial.println();
  Serial.println((String)"Formatage fini à: decompte: " + decompte + ", tailleToGo:" + tailleToGo + ", adresse: " + adresse + ", boucle N° " + loopn);
}
//fonction envoie de message
void sendMessage(String outgoing) {
  if (msgCount > limiteCount) { // si le numero de message est supérieur à la limite
    msgCount = 0; //on réinitialise le numero de message
  }
  LoRa.beginPacket();                   // début du packet
  LoRa.write(destination);              // ajout de l'adresse de destination
  LoRa.write(localAddress);             // ajout de l'addresse de l'envoyeur
  LoRa.write(msgCount);                 // ajout ID du message
  Serial.print("Envoie de mess : ");
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "envoie du message ");
  Heltec.display -> display();
  Serial.println(msgCount);
  LoRa.write(outgoing.length());        // ajout de la longueur du payload
  LoRa.print(outgoing);                 // ajout du payload
  LoRa.endPacket();                     // fin du packet et envoie
  msgCount++;                           // incrementation d'identifiant du message
}
//méthode de réception de mesage
void onReceive(int packetSize, int MsgID) {
  if (packetSize == 0) return;          // s'il n'y a pas de paquet alors retour
  // read packet header bytes:
  int recipient = LoRa.read();          // adresse de réception
  byte sender = LoRa.read();            // adresse de l'envoyeur
  byte incomingMsgId = LoRa.read();     // identifiant du message reçu
  byte incomingLength = LoRa.read();    // longueur du message reçu
  String incoming;

  while (LoRa.available()) { // tant que le lora est disponible
    incoming += (char)LoRa.read(); // lire le message reçu
  }
  // si le récepteur n'est pas cette appareil ou un broadcast
  if (recipient != localAddress && recipient != 0xFF) {
    Serial.println("This message is not for me.");
    return;                             // passer le reste de la fonction
  }

  char charBuf[200]; // tableau de caractère
  incoming.toCharArray(charBuf, 200); //conversion du message reçu en tableau
  //afficher quand réception de message
  /*if (millis() - lastSendTime > interval) {
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Je recois");
    Heltec.display -> display();
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
    delay(1000);
    Heltec.display -> clear();
    }*/
  // si le message est pour cette appareil ou un broadcast alors affichage des détails:
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incomingMsgId));
  Serial.println("Message length: " + String(incomingLength));
  Serial.println(incoming);
  //Serial.println("RSSI: " + String(LoRa.packetRssi()));
  //Serial.println("Snr: " + String(LoRa.packetSnr()));
  Serial.println();
  Heltec.display -> clear();
  Heltec.display -> drawString(1, 1, "Packet de " + String(sender, HEX) + " id " + String(incomingMsgId));
  Heltec.display -> display();
  msgRecv = incoming.toInt();
}
