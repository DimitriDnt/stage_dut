//début librairie
#include "heltec.h"
#include <ArduinoJson.h>
#define BAND    433E6  //definition de la bande utiliser, 868E6,915E6
#include <Arduino.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>
//#include "heltec.h"
#include "WiFi.h"
#include <Crc16.h>
//fin librairie

//début variable
WebSocketsServer webSocket = WebSocketsServer(81);
bool plug = false;
String outgoing;              // message sortant
byte localAddress = 0xFD;     // adresse local
byte destination = 0xBB;      // destination d'envoi
byte msgCount = 0;            // compteur de messages sortant
long lastSendTime = 0;        // temps du dernier envoi
int interval = 2000;          // interval entre les envoie
String file;
byte tramb[] = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
//byte data[] = file;
Crc16 crc;
byte limite = 0;
int nbTrameMax = limite;
uint8_t placement = 0;
uint8_t adrr[4][4];
void setup() {
  //Initialisation
  Heltec.begin(true /*DisplayEnable Enable*/, true /*Heltec.LoRa Enable*/, true /*Serial Enable*/, true /*PABOOST Enable*/, BAND /*long BAND*/);
  Serial.println("Heltec.WebSocket.LoraRecpetion");
  Serial.setDebugOutput(true); // pour activer la sortie
  //fonction connexion wifi
  WIFISetUp(); //appel de la fonction wifisetup
  IPAddress myIP = WiFi.localIP(); //definition de l'adresse ip
  Serial.print("Adresse IP du serveur: ");
  Serial.println(myIP);
  //fonction création Websocket
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
}

void loop() {
  //fonction reception
  onReceive(LoRa.parsePacket());
  webSocket.loop(); //maintient de l'état ouvert et ecoute
}

//création de la connexion wifi
void WIFISetUp(void) {
  // Configuration du WiFi en mode station et déconnection si deja connecté à un point d'accès
  WiFi.disconnect(true);
  delay(1000);
  WiFi.mode(WIFI_STA);
  WiFi.setAutoConnect(true);
  WiFi.begin("OVPF-iut", "pitonguanyin");
  delay(100);
  byte count = 0;
  // tant que la connection au wifi n'est pas établie on essaye
  while (WiFi.status() != WL_CONNECTED && count < 10) {
    count ++;
    delay(500);
    Serial.println("Connecting...");
  }
  //si la connection au wifi est établie
  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Connecting...OK");
  }
  else {
    Serial.println("Connecting...Failed");
  }
  return;
}

//création websocket
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch (type) {
    //dans le cas ou le wifi n'est pas connecter
    case WStype_DISCONNECTED:
      plug = false;
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    //dans le cas ou le wifi est conecter
    case WStype_CONNECTED: {
        IPAddress ip = webSocket.remoteIP(num);
        boolean pareil = false;
        Serial.print(placement);
        if (placement == 0) {
          Serial.println("= 0");
          for (int i = 0; i < 4; i++) {
            adrr[placement][i] = ip[i];
            /*Serial.print(adrr[placement][i]);
              Serial.print(".");*/
          }
        } else if (placement > 0 ) {
          Serial.println(" > 0");
          for (int i = 0; i < 4; i++) {
            if (adrr[placement][i] == ip[i]) {
              Serial.print((String)adrr[placement][i]);
              Serial.println(ip[i]);
              pareil = true;
              Serial.println((String)"pareil " + pareil);
            }
          }
        }

        if (pareil == true) {
          Serial.println("addr identique");
          break;
        }
        placement = num;
        Serial.println();
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        //placement = num ;
        Serial.printf("position %d \n", placement);
        Serial.print("adresse ");
        for (int i = 0; i < 4; i++) {
          //adrr[placement][i] = ip[i];
          Serial.print(adrr[placement][i]);
          Serial.print(".");
        }
        Serial.println();
      }
      break;

    case WStype_TEXT:
      Serial.println(sizeof(payload));
            Serial.printf("[%u] get Text: %s\n", num, payload);
      /*if (nb trame qui arrive) {

        }
        if (trame) {

        }
        if (crc) {

        }
      */
      if (payload[0] == '#') {
      }
      break;
  }
}
//Fonction envoie de message
void sendMessage(String outgoing, byte retour) {
  if (msgCount > 10) {
    msgCount = 0;
  }
  retour = retour, HEX;
  LoRa.beginPacket();                   // début du packet
  LoRa.write(destination);              // ajout de l'adresse de destination
  LoRa.write(localAddress);             // ajout de l'addresse de l'envoyeur
  LoRa.write(msgCount);                 // ajout ID du message
  Serial.print("Envoie de mess : ");
  Heltec.display -> clear();
  Heltec.display -> drawString(0, 0, "envoie du message ");
  Heltec.display -> display();
  /*Heltec.display -> clear();
    Heltec.display -> drawString(1, 0, String(msgCount));
    Heltec.display -> display();*/
  Serial.println(msgCount);
  LoRa.write(outgoing.length());        // ajout de la longueur du payload
  LoRa.print(outgoing);                 // ajout du payload
  LoRa.endPacket();                     // fin du packet et envoie
  msgCount++;                           // incrementation d'identifiant du message
}

//fonction de réception de donnée
void onReceive(int packetSize) {
  if (packetSize == 0) return;          // s'il n'y a pas de paquet alors retour
  // read packet header bytes:
  int recipient = LoRa.read();          // adresse de réception
  byte sender = LoRa.read();            // adresse de l'envoyeur
  int incomingMsgId = LoRa.read();      // identifiant du message reçu
  byte incomingLength = LoRa.read();    // longueur du message reçu
  String incoming;
  /*byte limite = 0;
    int nbTrameMax = limite;*/
  while (LoRa.available()) {            // tant que le lora est disponible
    incoming += (char)LoRa.read();      // lire le message reçu
  }
  /*if (incomingLength != incoming.length())
    { // check length for error
    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
    }*/

  // si le récepteur n'est pas cette appareil ou un broadcast
  if (recipient != localAddress && recipient != 0xFF) {
    Serial.println("This message is not for me.");
    return;                             // passer le reste de la fonction
  }

  char charBuf[200]; // tableau de caractère
  incoming.toCharArray(charBuf, 200); //conversion du message reçu en tableau
  //afficher quand réception de message
  /*if (millis() - lastSendTime > interval) {
    Heltec.display -> clear();
    Heltec.display -> drawString(0, 0, "Je recois");
    Heltec.display -> display();
    lastSendTime = millis();            // timestamp the message
    interval = random(2000) + 1000;    // 2-3 seconds
    delay(1000);
    Heltec.display -> clear();
    }*/
  // si le message est pour cette appareil ou un broadcast alors affichage des détails:
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incomingMsgId));
  Serial.println("Message length: " + String(incomingLength));
  Serial.println(incoming);
  //Serial.println("RSSI: " + String(LoRa.packetRssi()));
  //Serial.println("Snr: " + String(LoRa.packetSnr()));
  //Serial.println();
  Heltec.display -> clear();
  Heltec.display -> drawString(1, 1, "Packet de " + String(sender, HEX) + " id " + String(incomingMsgId));
  Heltec.display -> display();

  if (incomingMsgId == 0) { //si id = 0
    limite = incoming.toInt() + 1;
    nbTrameMax = limite;
    //Serial.println(limite);
    //Serial.println(nbTrameMax);
    Serial.println((String)"id = " + incomingMsgId);
    //Serial.println("Envoie ack");
    //sendMessage(String(incomingMsgId),String(sender));
    Serial.println((String)"Nombre de trame à recevoir: " + limite);
    //Serial.println();
  } else  if (incomingMsgId == 1) { //si id diff 0
    Serial.println((String)"id = " + incomingMsgId);
    //Serial.println("Envoie ack");
    //sendMessage(String(incomingMsgId), String(sender));
    if (file.length() == 0) { //si fichier vide
      Serial.print("fichier vide: ");
      Serial.println(file.length());
      file = file + incoming; //stocker dans fichier

    } else if (file.length() != 0 ) { //si fichier pas vide
      Serial.print("fichier pas vide: ");
      Serial.println(file.length());
      Serial.println("erase");
      file = ""; //vider le ficher
      file = file + incoming; //stocker dans fichier
    }
  } else  if (incomingMsgId == nbTrameMax || incomingMsgId == limite) { //si id egal limite
    Serial.println((String)"id: " + incomingMsgId + " = limite: " + limite);
    //Serial.println("Envoie ack");
    //sendMessage(String(incomingMsgId),String(sender));
    if (file.length() != 0 ) { //si fichier pas vide
      Serial.print("fichier pas vide: ");
      //file = file + incoming; //stocker dans fichier
      Serial.println(file.length());
      //int i = 0;
      byte dataRecu[sizeof(file)];
      char crcRecu[incoming.length()];

      //boucle while diviser en 2 boucles for
      for (int i = 0; i <= sizeof(file) - 1; i++) {
        dataRecu[i] = file[i];
      }
      for (int i = 0; i <= sizeof(incoming); i++) {
        crcRecu[i] = incoming[i];
        //Serial.print(crcRecu[i]);
      }
      Serial.println();

      unsigned short valueR = crc.getCrc(); // création crc de datarecu > donnée recu
      crc.clearCrc();
      valueR = crc.XModemCrc(dataRecu, 0, sizeof(dataRecu));

      char crcCalculer[6]; //conteneur de crcr
      sprintf(crcCalculer, "%04X", valueR); // crc dans char

      Serial.print("crc calculé: ");
      for (int i = 0; i <= sizeof(crcCalculer) - 2; i++) {
        Serial.print(char(crcCalculer[i]));
        //Serial.print("oui");
      }
      Serial.println();
      Serial.print("crc recu: ");
      for (int i = 0; i <= sizeof(crcRecu) - 1; i++) {
        Serial.print(crcRecu[i]);
        //Serial.print("oui");
      }
      Serial.println();

      byte test[] = "{\"AS\":\"01\",\"CS\":\"Gets5\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";
      String infos = "{\"AS\":\"01\",\"CS\":\"Gets3\",\"AD_1\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}},\"AD_2\":{\"p\":10000,\"c1\":{\"n\":\"SO2_001\",\"A\":\"true\"},\"c2\":{\"n\":\"HCL_001\",\"A\":\"true\"},\"c3\":{\"n\":\"CH4_001\",\"A\":\"true\"},\"c4\":{\"n\":\"CH4_001\",\"A\":\"true\"}}}";

      boolean verif = true;
      //boucle for verification crc
      for (int i = 0; i <= sizeof(crcRecu) - 1; i++) {
        if (crcRecu[i] == crcCalculer[i]) {
          verif = true;
        } else {
          verif = false;
          i = 4;
        }
      }
      if (verif == true) { // si la verification est bonne
        Serial.println("crc validé");
        Serial.println("envoie file vers Serveur ");

        if (plug = true) {// envoie de la trame si connecter
          //Serial.println("lancement d'un message");
          //lancement d'un message
          webSocket.sendTXT(0, file);
          //delay(1000);
          return;
        }
        file = ""; //vider le ficher
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "J'ai reçu une trame valide");
        Heltec.display -> display();
        return;
      } else if (verif == false) {// si la verification est raté
        Serial.println("crc pas valide");
        Serial.println("envoie file vers Serveur Annuler");
        Serial.println("envoie quand meme pour dimi");
        if (plug) { // si connecter envoie sur websocket
          webSocket.sendTXT(0, file);
        }
        delay(1000);
        if (plug) { //si connecter envoie
          webSocket.sendTXT(0, infos);
        }
        file = "";//vider le fichier
        Heltec.display -> clear();
        Heltec.display -> drawString(0, 0, "J'ai reçu une trame non valide");
        Heltec.display -> display();
        return;
      }
    }
    Serial.println();
  } else  if (incomingMsgId != 0) { //si id diff 0
    Serial.println((String)"id != " + limite);
    //Serial.println("Envoie ack");
    //sendMessage(String(incomingMsgId), String(sender));
    if (file.length() != 0 ) { //si fichier pas vide
      Serial.print("fichier pas vide: ");
      Serial.println(file.length());
      file = file + incoming;

    } else if (file.length() == 0 ) { //si fichier vide
      Serial.println("fichier vide: ");
      Serial.println(file.length());
    }
    Serial.println();
  }
  Serial.println();
}
