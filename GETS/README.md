# Stage à l'Observatoire Volcanologique du Piton de la Fournaise (OVPF)

## Sujet du stage commun 

Projet Geochimical Easily Transported System (GETS)


#### Sujet du stage - DENNEMONT 

```
Création d'un outil web de contrôle et de monitoring pour GETS
```


#### Sujet du stage - HOAREAU 

```
Mise en place des fonctions matérielles et logicielles de télécommunication entre GETS et son utilisateur
```


## Compétences mises en oeuvre 

Développement électronique et informatique sur un système embarqué

## Auteurs 

Dimitri DENNEMONT et François Philippe HOAREAU